﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.Stuff
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using GMap.NET;
using GMap.NET.MapProviders;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;

namespace Demo.WindowsForms
{
  public class Stuff
  {
    private static readonly Random r = new Random();
    public static string sessionId = string.Empty;

    public static IEnumerable<List<GpsLog>> GetRoutesFromMobileLog(string gpsdLogFile, DateTime? start, DateTime? end, double? maxPositionDilutionOfPrecision)
    {
      using (SQLiteConnection sqLiteConnection = new SQLiteConnection())
      {
        ((DbConnection) sqLiteConnection).ConnectionString = string.Format("Data Source=\"{0}\";FailIfMissing=True;", (object) gpsdLogFile);
        ((DbConnection) sqLiteConnection).Open();
        using (DbCommand dbCommand = (DbCommand) sqLiteConnection.CreateCommand())
        {
          dbCommand.CommandText = "SELECT * FROM GPS ";
          int initLenght = dbCommand.CommandText.Length;
          if (start.HasValue)
          {
            dbCommand.CommandText += "WHERE TimeUTC >= @t1 ";
            dbCommand.Parameters.Add((object) new SQLiteParameter("@t1", (object) start));
          }
          if (end.HasValue)
          {
            if (dbCommand.CommandText.Length <= initLenght)
              dbCommand.CommandText += "WHERE ";
            else
              dbCommand.CommandText += "AND ";
            dbCommand.CommandText += "TimeUTC <= @t2 ";
            dbCommand.Parameters.Add((object) new SQLiteParameter("@t2", (object) end));
          }
          if (maxPositionDilutionOfPrecision.HasValue)
          {
            if (dbCommand.CommandText.Length <= initLenght)
              dbCommand.CommandText += "WHERE ";
            else
              dbCommand.CommandText += "AND ";
            dbCommand.CommandText += "PositionDilutionOfPrecision <= @p3 ";
            dbCommand.Parameters.Add((object) new SQLiteParameter("@p3", (object) maxPositionDilutionOfPrecision));
          }
          using (DbDataReader dbDataReader = dbCommand.ExecuteReader())
          {
            List<GpsLog> points = new List<GpsLog>();
            while (dbDataReader.Read())
            {
              GpsLog log = new GpsLog();
              log.TimeUTC = (DateTime) dbDataReader["TimeUTC"];
              log.SessionCounter = (long) dbDataReader["SessionCounter"];
              log.Delta = dbDataReader["Delta"] as double?;
              log.Speed = dbDataReader["Speed"] as double?;
              log.SeaLevelAltitude = dbDataReader["SeaLevelAltitude"] as double?;
              log.EllipsoidAltitude = dbDataReader["EllipsoidAltitude"] as double?;
              // ISSUE: explicit reference operation
              // ISSUE: variable of a reference type
              GpsLog& local1 = @log;
              byte? nullable1 = dbDataReader["SatellitesInView"] as byte?;
              short? nullable2 = nullable1.HasValue ? new short?((short) nullable1.GetValueOrDefault()) : new short?();
              // ISSUE: explicit reference operation
              (^local1).SatellitesInView = nullable2;
              // ISSUE: explicit reference operation
              // ISSUE: variable of a reference type
              GpsLog& local2 = @log;
              byte? nullable3 = dbDataReader["SatelliteCount"] as byte?;
              short? nullable4 = nullable3.HasValue ? new short?((short) nullable3.GetValueOrDefault()) : new short?();
              // ISSUE: explicit reference operation
              (^local2).SatelliteCount = nullable4;
              log.Position = new PointLatLng((double) dbDataReader["Lat"], (double) dbDataReader["Lng"]);
              log.PositionDilutionOfPrecision = dbDataReader["PositionDilutionOfPrecision"] as double?;
              log.HorizontalDilutionOfPrecision = dbDataReader["HorizontalDilutionOfPrecision"] as double?;
              log.VerticalDilutionOfPrecision = dbDataReader["VerticalDilutionOfPrecision"] as double?;
              log.FixQuality = (FixQuality) (byte) dbDataReader["FixQuality"];
              log.FixType = (FixType) (byte) dbDataReader["FixType"];
              log.FixSelection = (FixSelection) (byte) dbDataReader["FixSelection"];
              if (log.SessionCounter == 0L && points.Count > 0)
              {
                List<GpsLog> ret = new List<GpsLog>((IEnumerable<GpsLog>) points);
                points.Clear();
                yield return ret;
              }
              points.Add(log);
            }
            if (points.Count > 0)
            {
              List<GpsLog> ret = new List<GpsLog>((IEnumerable<GpsLog>) points);
              points.Clear();
              yield return ret;
            }
            points.Clear();
            points = (List<GpsLog>) null;
            dbDataReader.Close();
          }
        }
        ((DbConnection) sqLiteConnection).Close();
      }
    }

    public static void GetVilniusTransportData(TransportType type, string line, List<VehicleData> ret)
    {
      ret.Clear();
      string str1 = string.Format((IFormatProvider) CultureInfo.InvariantCulture, "http://www.troleibusai.lt/eismas/get_gps.php?rand={0}&more=1", new object[1]
      {
        (object) Stuff.r.NextDouble()
      });
      if (type == TransportType.Bus)
        str1 += "&bus=1";
      string url = str1 + "&app=GMap.NET.Desktop";
      string contentUsingHttp = EmptyProvider.Instance.GetContentUsingHttp(url);
      char[] chArray1 = new char[1]
      {
        '&'
      };
      foreach (string str2 in contentUsingHttp.Split(chArray1))
      {
        char[] chArray2 = new char[1]
        {
          ';'
        };
        string[] strArray = str2.Split(chArray2);
        if (strArray.Length == 6)
        {
          VehicleData vehicleData = new VehicleData();
          vehicleData.Id = int.Parse(strArray[2]);
          vehicleData.Lat = double.Parse(strArray[0], (IFormatProvider) CultureInfo.InvariantCulture);
          vehicleData.Lng = double.Parse(strArray[1], (IFormatProvider) CultureInfo.InvariantCulture);
          vehicleData.Line = strArray[3];
          if (!string.IsNullOrEmpty(strArray[4]))
            vehicleData.Bearing = new double?(double.Parse(strArray[4], (IFormatProvider) CultureInfo.InvariantCulture));
          if (!string.IsNullOrEmpty(strArray[5]))
          {
            vehicleData.Time = strArray[5];
            DateTime dateTime = DateTime.Parse(vehicleData.Time);
            if (!(DateTime.Now - dateTime > TimeSpan.FromMinutes(5.0)))
              vehicleData.Time = dateTime.ToLongTimeString();
            else
              continue;
          }
          ret.Add(vehicleData);
        }
      }
    }

    public static void GetFlightRadarData(List<FlightRadarData> ret, PointLatLng location, int zoom, bool resetSession)
    {
      ret.Clear();
      if (resetSession || string.IsNullOrEmpty(Stuff.sessionId))
        Stuff.sessionId = Stuff.GetFlightRadarContentUsingHttp("http://www.flightradar24.com/", location, zoom, string.Empty);
      if (string.IsNullOrEmpty(Stuff.sessionId))
        return;
      string contentUsingHttp = Stuff.GetFlightRadarContentUsingHttp("http://www.flightradar24.com/PlaneFeed.json", location, zoom, Stuff.sessionId);
      char[] chArray = new char[1]
      {
        ']'
      };
      foreach (string str1 in contentUsingHttp.Split(chArray))
      {
        if (str1.Length > 11)
        {
          string[] strArray = str1.Substring(2).Replace(":", ",").Replace("\"", string.Empty).Replace("[", string.Empty).Split(',');
          if (strArray.Length == 12)
          {
            string str2 = strArray[0];
            string str3 = strArray[1];
            string s1 = strArray[2];
            string s2 = strArray[3];
            string s3 = strArray[4];
            string str4 = (string) (object) checked ((int) unchecked ((double) int.Parse(strArray[5]) * 0.3048)) + (object) "m";
            string str5 = (string) (object) checked ((int) unchecked ((double) int.Parse(strArray[6]) * 1.852)) + (object) "km/h";
            ret.Add(new FlightRadarData()
            {
              name = str2,
              hex = str3,
              bearing = int.Parse(s3),
              altitude = str4,
              speed = str5,
              point = new PointLatLng(double.Parse(s1, (IFormatProvider) CultureInfo.InvariantCulture), double.Parse(s2, (IFormatProvider) CultureInfo.InvariantCulture)),
              Id = Convert.ToInt32(str3, 16)
            });
          }
          else
            Debugger.Break();
        }
      }
    }

    private static string GetFlightRadarContentUsingHttp(string url, PointLatLng p, int zoom, string sid)
    {
      string str1 = string.Empty;
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(url);
      httpWebRequest.UserAgent = GMapProvider.UserAgent;
      httpWebRequest.Timeout = GMapProvider.TimeoutMs;
      httpWebRequest.ReadWriteTimeout = checked (GMapProvider.TimeoutMs * 6);
      httpWebRequest.Accept = "*/*";
      httpWebRequest.Referer = "http://www.flightradar24.com/";
      httpWebRequest.KeepAlive = true;
      httpWebRequest.Headers.Add("Cookie", string.Format((IFormatProvider) CultureInfo.InvariantCulture, "map_lat={0}; map_lon={1}; map_zoom={2}; " + (!string.IsNullOrEmpty(sid) ? "PHPSESSID=" + sid + ";" : string.Empty) + "__utma=109878426.303091014.1316587318.1316587318.1316587318.1; __utmb=109878426.2.10.1316587318; __utmz=109878426.1316587318.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)", (object) p.Lat, (object) p.Lng, (object) zoom));
      using (HttpWebResponse httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse)
      {
        if (string.IsNullOrEmpty(sid))
        {
          string str2 = httpWebResponse.Headers["Set-Cookie"];
          if (str2.Contains("PHPSESSID"))
            str1 = str2.Split('=')[1].Split(';')[0];
        }
        using (Stream responseStream = httpWebResponse.GetResponseStream())
        {
          using (StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8))
          {
            string str2 = streamReader.ReadToEnd();
            if (!string.IsNullOrEmpty(sid))
              str1 = str2;
          }
        }
        httpWebResponse.Close();
      }
      return str1;
    }
  }
}
