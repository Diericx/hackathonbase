﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.DescendingComparer
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using System.Collections.Generic;

namespace Demo.WindowsForms
{
  internal class DescendingComparer : IComparer<IpStatus>
  {
    public bool SortOnlyCountryName;

    public int Compare(IpStatus x, IpStatus y)
    {
      int num = 0;
      if (!this.SortOnlyCountryName)
        num = y.ConnectionsCount.CompareTo(x.ConnectionsCount);
      if (num == 0)
        return x.CountryName.CompareTo(y.CountryName);
      return num;
    }
  }
}
