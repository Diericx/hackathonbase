﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.FlightRadarData
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using GMap.NET;

namespace Demo.WindowsForms
{
  public struct FlightRadarData
  {
    public string name;
    public string hex;
    public PointLatLng point;
    public int bearing;
    public string altitude;
    public string speed;
    public int Id;
  }
}
