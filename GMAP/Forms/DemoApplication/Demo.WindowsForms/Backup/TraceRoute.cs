﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.TraceRoute
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace Demo.WindowsForms
{
  internal class TraceRoute
  {
    private static readonly string Data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    private static readonly int timeout = 8888;
    private static readonly byte[] DataBuffer = Encoding.ASCII.GetBytes(TraceRoute.Data);

    public static List<PingReply> GetTraceRoute(string hostNameOrAddress)
    {
      return TraceRoute.GetTraceRoute(hostNameOrAddress, 1);
    }

    private static List<PingReply> GetTraceRoute(string hostNameOrAddress, int ttl)
    {
      List<PingReply> list = new List<PingReply>();
      using (Ping ping = new Ping())
      {
        PingOptions options = new PingOptions(ttl, true);
        PingReply pingReply = ping.Send(hostNameOrAddress, TraceRoute.timeout, TraceRoute.DataBuffer, options);
        if (pingReply.Status == IPStatus.Success)
          list.Add(pingReply);
        else if (pingReply.Status == IPStatus.TtlExpired)
        {
          list.Add(pingReply);
          list.AddRange((IEnumerable<PingReply>) TraceRoute.GetTraceRoute(hostNameOrAddress, checked (ttl + 1)));
        }
      }
      return list;
    }
  }
}
