﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.TcpRow
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using System.Net;
using System.Net.NetworkInformation;

namespace Demo.WindowsForms
{
  public struct TcpRow
  {
    private IPEndPoint localEndPoint;
    private IPEndPoint remoteEndPoint;
    private TcpState state;
    private int processId;

    public IPEndPoint LocalEndPoint
    {
      get
      {
        return this.localEndPoint;
      }
    }

    public IPEndPoint RemoteEndPoint
    {
      get
      {
        return this.remoteEndPoint;
      }
    }

    public TcpState State
    {
      get
      {
        return this.state;
      }
    }

    public int ProcessId
    {
      get
      {
        return this.processId;
      }
    }

    public TcpRow(IpHelper.TcpRow tcpRow)
    {
      this.state = tcpRow.state;
      this.processId = tcpRow.owningPid;
      int port1 = checked (((int) tcpRow.localPort1 << 8) + (int) tcpRow.localPort2 + ((int) tcpRow.localPort3 << 24) + (int) tcpRow.localPort4 << 16);
      this.localEndPoint = new IPEndPoint((long) tcpRow.localAddr, port1);
      int port2 = checked (((int) tcpRow.remotePort1 << 8) + (int) tcpRow.remotePort2 + ((int) tcpRow.remotePort3 << 24) + (int) tcpRow.remotePort4 << 16);
      this.remoteEndPoint = new IPEndPoint((long) tcpRow.remoteAddr, port2);
    }
  }
}
