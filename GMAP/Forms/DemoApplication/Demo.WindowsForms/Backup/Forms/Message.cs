﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.Forms.Message
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Demo.WindowsForms.Forms
{
  public class Message : Form
  {
    private IContainer components;
    private Button button1;
    private Button button2;
    internal RichTextBox richTextBox1;

    public Message()
    {
      this.InitializeComponent();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (Message));
      this.button1 = new Button();
      this.button2 = new Button();
      this.richTextBox1 = new RichTextBox();
      this.SuspendLayout();
      this.button1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.button1.DialogResult = DialogResult.Yes;
      this.button1.Font = new Font("Microsoft Sans Serif", 7.8f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.button1.Location = new Point(346, 414);
      this.button1.Name = "button1";
      this.button1.Size = new Size(268, 67);
      this.button1.TabIndex = 1;
      this.button1.Text = "YES, I accept full responsability for using this software";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.button2.DialogResult = DialogResult.No;
      this.button2.Location = new Point(8, 414);
      this.button2.Name = "button2";
      this.button2.Size = new Size(111, 67);
      this.button2.TabIndex = 2;
      this.button2.Text = "NO, thanks";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.richTextBox1.Dock = DockStyle.Top;
      this.richTextBox1.Location = new Point(5, 5);
      this.richTextBox1.Name = "richTextBox1";
      this.richTextBox1.ReadOnly = true;
      this.richTextBox1.Size = new Size(609, 394);
      this.richTextBox1.TabIndex = 3;
      this.richTextBox1.Text = componentResourceManager.GetString("richTextBox1.Text");
      this.AutoScaleDimensions = new SizeF(8f, 16f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = Color.Gray;
      this.ClientSize = new Size(619, 489);
      this.Controls.Add((Control) this.richTextBox1);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.FormBorderStyle = FormBorderStyle.None;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "Message";
      this.Padding = new Padding(5);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "<< WARNING >>";
      this.ResumeLayout(false);
    }
  }
}
