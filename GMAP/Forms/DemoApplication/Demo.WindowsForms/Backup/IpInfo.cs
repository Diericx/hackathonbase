﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.IpInfo
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using System;
using System.Net.NetworkInformation;

namespace Demo.WindowsForms
{
  internal class IpInfo
  {
    public string Ip;
    public int Port;
    public TcpState State;
    public string ProcessName;
    public string CountryName;
    public string RegionName;
    public string City;
    public double Latitude;
    public double Longitude;
    public DateTime CacheTime;
    public DateTime StatusTime;
    public bool TracePoint;
  }
}
