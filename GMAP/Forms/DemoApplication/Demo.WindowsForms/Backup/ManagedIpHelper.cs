﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.ManagedIpHelper
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Demo.WindowsForms
{
  public static class ManagedIpHelper
  {
    public static readonly List<TcpRow> TcpRows = new List<TcpRow>();

    public static void UpdateExtendedTcpTable(bool sorted)
    {
      ManagedIpHelper.TcpRows.Clear();
      IntPtr num1 = IntPtr.Zero;
      int tcpTableLength = 0;
      if ((int) IpHelper.GetExtendedTcpTable(num1, ref tcpTableLength, sorted, 2, IpHelper.TcpTableType.OwnerPidConnections, 0) == 0)
        return;
      try
      {
        num1 = Marshal.AllocHGlobal(tcpTableLength);
        if ((int) IpHelper.GetExtendedTcpTable(num1, ref tcpTableLength, true, 2, IpHelper.TcpTableType.OwnerPidConnections, 0) != 0)
          return;
        IpHelper.TcpTable tcpTable = (IpHelper.TcpTable) Marshal.PtrToStructure(num1, typeof (IpHelper.TcpTable));
        IntPtr ptr = (IntPtr) checked ((long) num1 + (long) Marshal.SizeOf((object) tcpTable.Length));
        int num2 = 0;
        while ((long) num2 < (long) tcpTable.Length)
        {
          ManagedIpHelper.TcpRows.Add(new TcpRow((IpHelper.TcpRow) Marshal.PtrToStructure(ptr, typeof (IpHelper.TcpRow))));
          ptr = (IntPtr) checked ((long) ptr + (long) Marshal.SizeOf(typeof (IpHelper.TcpRow)));
          checked { ++num2; }
        }
      }
      finally
      {
        if (num1 != IntPtr.Zero)
          Marshal.FreeHGlobal(num1);
      }
    }
  }
}
