﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.TcpTable
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using System.Collections;
using System.Collections.Generic;

namespace Demo.WindowsForms
{
  public struct TcpTable : IEnumerable<TcpRow>, IEnumerable
  {
    private IEnumerable<TcpRow> tcpRows;

    public IEnumerable<TcpRow> Rows
    {
      get
      {
        return this.tcpRows;
      }
    }

    public TcpTable(IEnumerable<TcpRow> tcpRows)
    {
      this.tcpRows = tcpRows;
    }

    public IEnumerator<TcpRow> GetEnumerator()
    {
      return this.tcpRows.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.tcpRows.GetEnumerator();
    }
  }
}
