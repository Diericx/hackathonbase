﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.StaticImage
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace Demo.WindowsForms
{
  public class StaticImage : Form
  {
    private BackgroundWorker bg = new BackgroundWorker();
    private readonly List<GPoint> tileArea = new List<GPoint>();
    private readonly List<PointLatLng> GpxRoute = new List<PointLatLng>();
    private RectLatLng AreaGpx = RectLatLng.Empty;
    private IContainer components;
    private Button button1;
    private ProgressBar progressBar1;
    private NumericUpDown numericUpDown1;
    private Button button2;
    private Label label1;
    private CheckBox checkBoxWorldFile;
    private CheckBox checkBoxRoutes;
    private MainForm Main;

    public StaticImage(MainForm main)
    {
      this.InitializeComponent();
      this.Main = main;
      this.numericUpDown1.Maximum = (Decimal) this.Main.MainMap.MaxZoom;
      this.numericUpDown1.Minimum = (Decimal) this.Main.MainMap.MinZoom;
      this.numericUpDown1.Value = new Decimal(this.Main.MainMap.Zoom);
      this.bg.WorkerReportsProgress = true;
      this.bg.WorkerSupportsCancellation = true;
      this.bg.DoWork += new DoWorkEventHandler(this.bg_DoWork);
      this.bg.ProgressChanged += new ProgressChangedEventHandler(this.bg_ProgressChanged);
      this.bg.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.bg_RunWorkerCompleted);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.button1 = new Button();
      this.progressBar1 = new ProgressBar();
      this.numericUpDown1 = new NumericUpDown();
      this.button2 = new Button();
      this.label1 = new Label();
      this.checkBoxWorldFile = new CheckBox();
      this.checkBoxRoutes = new CheckBox();
      this.numericUpDown1.BeginInit();
      this.SuspendLayout();
      this.button1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.button1.Location = new Point(497, 29);
      this.button1.Margin = new Padding(2);
      this.button1.Name = "button1";
      this.button1.Size = new Size(84, 32);
      this.button1.TabIndex = 1;
      this.button1.Text = "Generate";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.progressBar1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.progressBar1.Location = new Point(6, 6);
      this.progressBar1.Margin = new Padding(2);
      this.progressBar1.Name = "progressBar1";
      this.progressBar1.Size = new Size(484, 93);
      this.progressBar1.TabIndex = 2;
      this.numericUpDown1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.numericUpDown1.Location = new Point(535, 6);
      this.numericUpDown1.Margin = new Padding(2);
      this.numericUpDown1.Name = "numericUpDown1";
      this.numericUpDown1.Size = new Size(47, 20);
      this.numericUpDown1.TabIndex = 3;
      this.button2.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.button2.Location = new Point(497, 67);
      this.button2.Margin = new Padding(2);
      this.button2.Name = "button2";
      this.button2.Size = new Size(84, 32);
      this.button2.TabIndex = 4;
      this.button2.Text = "Cancel";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.label1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(494, 8);
      this.label1.Margin = new Padding(2, 0, 2, 0);
      this.label1.Name = "label1";
      this.label1.Size = new Size(37, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "Zoom:";
      this.checkBoxWorldFile.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.checkBoxWorldFile.AutoSize = true;
      this.checkBoxWorldFile.Location = new Point(497, 106);
      this.checkBoxWorldFile.Name = "checkBoxWorldFile";
      this.checkBoxWorldFile.Size = new Size(96, 17);
      this.checkBoxWorldFile.TabIndex = 6;
      this.checkBoxWorldFile.Text = "make Worldfile";
      this.checkBoxWorldFile.UseVisualStyleBackColor = true;
      this.checkBoxRoutes.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.checkBoxRoutes.AutoSize = true;
      this.checkBoxRoutes.Location = new Point(7, 106);
      this.checkBoxRoutes.Name = "checkBoxRoutes";
      this.checkBoxRoutes.Size = new Size(147, 17);
      this.checkBoxRoutes.TabIndex = 7;
      this.checkBoxRoutes.Text = "Use area of routes in map";
      this.checkBoxRoutes.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(588, 130);
      this.Controls.Add((Control) this.checkBoxRoutes);
      this.Controls.Add((Control) this.checkBoxWorldFile);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.numericUpDown1);
      this.Controls.Add((Control) this.progressBar1);
      this.Controls.Add((Control) this.button1);
      this.FormBorderStyle = FormBorderStyle.SizableToolWindow;
      this.Margin = new Padding(2);
      this.MinimumSize = new Size(16, 164);
      this.Name = "StaticImage";
      this.Padding = new Padding(4);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Static Map maker";
      this.numericUpDown1.EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private void bg_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      if (!e.Cancelled)
      {
        if (e.Error != null)
        {
          int num = (int) MessageBox.Show("Error:" + e.Error.ToString(), "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
        else if (e.Result != null)
        {
          try
          {
            Process.Start(e.Result as string);
          }
          catch
          {
          }
        }
      }
      this.Text = "Static Map maker";
      this.progressBar1.Value = 0;
      this.button1.Enabled = true;
      this.numericUpDown1.Enabled = true;
    }

    private void bg_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      this.progressBar1.Value = e.ProgressPercentage;
      GPoint gpoint = (GPoint) e.UserState;
      this.Text = "Static Map maker: Downloading[" + (object) gpoint + "]: " + (string) (object) this.tileArea.IndexOf(gpoint) + " of " + (string) (object) this.tileArea.Count;
    }

    private void bg_DoWork(object sender, DoWorkEventArgs e)
    {
      MapInfo mapInfo = (MapInfo) e.Argument;
      if (mapInfo.Area.IsEmpty)
        return;
      string filename = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + (object) Path.DirectorySeparatorChar + "GMap at zoom " + (string) (object) mapInfo.Zoom + " - " + (string) (object) mapInfo.Type + "-" + (string) (object) DateTime.Now.Ticks + ".png";
      e.Result = (object) filename;
      GPoint gpoint1 = mapInfo.Type.Projection.FromLatLngToPixel(mapInfo.Area.LocationTopLeft, mapInfo.Zoom);
      GPoint gpoint2 = mapInfo.Type.Projection.FromLatLngToPixel(mapInfo.Area.Bottom, mapInfo.Area.Right, mapInfo.Zoom);
      GPoint gpoint3 = new GPoint(checked (gpoint2.X - gpoint1.X), checked (gpoint2.Y - gpoint1.Y));
      mapInfo.Type.Projection.GetTileMatrixMaxXY(mapInfo.Zoom);
      int num1 = mapInfo.MakeWorldFile ? 0 : 22;
      using (Bitmap bitmap = new Bitmap(checked (gpoint3.X + num1 * 2), checked (gpoint3.Y + num1 * 2)))
      {
        using (Graphics g = Graphics.FromImage((Image) bitmap))
        {
          g.InterpolationMode = InterpolationMode.HighQualityBicubic;
          g.SmoothingMode = SmoothingMode.HighQuality;
          int num2 = 0;
          lock (this.tileArea)
          {
            foreach (GPoint item_1 in this.tileArea)
            {
              if (this.bg.CancellationPending)
              {
                e.Cancel = true;
                return;
              }
              this.bg.ReportProgress(checked ((int) unchecked ((double) checked (++num2) / (double) this.tileArea.Count * 100.0)), (object) item_1);
              foreach (GMapProvider item_0 in mapInfo.Type.Overlays)
              {
                Exception local_12;
                WindowsFormsImage local_13 = Singleton<GMaps>.Instance.GetImageFrom(item_0, item_1, mapInfo.Zoom, out local_12) as WindowsFormsImage;
                if (local_13 != null)
                {
                  using (local_13)
                  {
                    int local_14 = checked (item_1.X * mapInfo.Type.Projection.TileSize.Width - gpoint1.X + num1);
                    int local_15 = checked (item_1.Y * mapInfo.Type.Projection.TileSize.Width - gpoint1.Y + num1);
                    g.DrawImage(local_13.Img, local_14, local_15, mapInfo.Type.Projection.TileSize.Width, mapInfo.Type.Projection.TileSize.Height);
                  }
                }
              }
            }
          }
          foreach (GMapRoute gmapRoute in (Collection<GMapRoute>) this.Main.routes.Routes)
          {
            if (gmapRoute.IsVisible)
            {
              using (GraphicsPath path = new GraphicsPath())
              {
                int index = 0;
                while (index < gmapRoute.Points.Count)
                {
                  PointLatLng pointLatLng = gmapRoute.Points[index];
                  GPoint gpoint4 = mapInfo.Type.Projection.FromLatLngToPixel(pointLatLng.Lat, pointLatLng.Lng, mapInfo.Zoom);
                  gpoint4.Offset(num1, num1);
                  gpoint4.Offset(checked (-gpoint1.X), checked (-gpoint1.Y));
                  GPoint gpoint5 = gpoint4;
                  if (index == 0)
                  {
                    path.AddLine(gpoint5.X, gpoint5.Y, gpoint5.X, gpoint5.Y);
                  }
                  else
                  {
                    PointF lastPoint = path.GetLastPoint();
                    path.AddLine(lastPoint.X, lastPoint.Y, (float) gpoint5.X, (float) gpoint5.Y);
                  }
                  checked { ++index; }
                }
                if (path.PointCount > 0)
                  g.DrawPath(gmapRoute.Stroke, path);
              }
            }
          }
          foreach (GMapPolygon gmapPolygon in (Collection<GMapPolygon>) this.Main.polygons.Polygons)
          {
            if (gmapPolygon.IsVisible)
            {
              using (GraphicsPath path = new GraphicsPath())
              {
                int index = 0;
                while (index < gmapPolygon.Points.Count)
                {
                  PointLatLng pointLatLng = gmapPolygon.Points[index];
                  GPoint gpoint4 = mapInfo.Type.Projection.FromLatLngToPixel(pointLatLng.Lat, pointLatLng.Lng, mapInfo.Zoom);
                  gpoint4.Offset(num1, num1);
                  gpoint4.Offset(checked (-gpoint1.X), checked (-gpoint1.Y));
                  GPoint gpoint5 = gpoint4;
                  if (index == 0)
                  {
                    path.AddLine(gpoint5.X, gpoint5.Y, gpoint5.X, gpoint5.Y);
                  }
                  else
                  {
                    PointF lastPoint = path.GetLastPoint();
                    path.AddLine(lastPoint.X, lastPoint.Y, (float) gpoint5.X, (float) gpoint5.Y);
                  }
                  checked { ++index; }
                }
                if (path.PointCount > 0)
                {
                  path.CloseFigure();
                  g.FillPath(gmapPolygon.Fill, path);
                  g.DrawPath(gmapPolygon.Stroke, path);
                }
              }
            }
          }
          foreach (GMapMarker gmapMarker in (Collection<GMapMarker>) this.Main.objects.Markers)
          {
            if (gmapMarker.IsVisible)
            {
              PointLatLng position = gmapMarker.Position;
              GPoint gpoint4 = mapInfo.Type.Projection.FromLatLngToPixel(position.Lat, position.Lng, mapInfo.Zoom);
              gpoint4.Offset(num1, num1);
              gpoint4.Offset(checked (-gpoint1.X), checked (-gpoint1.Y));
              gpoint4.Offset(gmapMarker.Offset.X, gmapMarker.Offset.Y);
              gmapMarker.LocalPosition = new Point(gpoint4.X, gpoint4.Y);
              gmapMarker.OnRender(g);
            }
          }
          foreach (GMapMarker gmapMarker in (Collection<GMapMarker>) this.Main.objects.Markers)
          {
            if (gmapMarker.IsVisible && gmapMarker.ToolTip != null && (gmapMarker.IsVisible && !string.IsNullOrEmpty(gmapMarker.ToolTipText)))
              gmapMarker.ToolTip.Draw(g);
          }
          Rectangle rect = new Rectangle();
          rect.Location = new Point(num1, num1);
          rect.Size = new Size(gpoint3.X, gpoint3.Y);
          using (Font font = new Font(FontFamily.GenericSansSerif, 9f, FontStyle.Bold))
          {
            using (Pen pen = new Pen(Brushes.DimGray, 3f))
            {
              pen.DashStyle = DashStyle.DashDot;
              g.DrawRectangle(pen, rect);
              string str1 = mapInfo.Area.LocationTopLeft.ToString();
              SizeF sizeF1 = g.MeasureString(str1, font);
              g.DrawString(str1, font, pen.Brush, (float) rect.X + sizeF1.Height / 2f, (float) rect.Y + sizeF1.Height / 2f);
              string str2 = new PointLatLng(mapInfo.Area.Bottom, mapInfo.Area.Right).ToString();
              SizeF sizeF2 = g.MeasureString(str2, font);
              g.DrawString(str2, font, pen.Brush, (float) ((double) rect.Right - (double) sizeF2.Width - (double) sizeF2.Height / 2.0), (float) ((double) rect.Bottom - (double) sizeF2.Height - (double) sizeF2.Height / 2.0));
            }
            using (Pen pen = new Pen(Brushes.Blue, 1f))
            {
              double groundResolution = mapInfo.Type.Projection.GetGroundResolution(mapInfo.Zoom, mapInfo.Area.Bottom);
              int width1 = checked ((int) unchecked (100.0 / groundResolution));
              int width2 = checked ((int) unchecked (1000.0 / groundResolution));
              g.DrawRectangle(pen, checked (rect.X + 10), checked (rect.Bottom - 20), width2, 10);
              g.DrawRectangle(pen, checked (rect.X + 10), checked (rect.Bottom - 20), width1, 10);
              string str = "scale: 100m | 1Km";
              SizeF sizeF = g.MeasureString(str, font);
              g.DrawString(str, font, pen.Brush, (float) checked (rect.X + 10), (float) ((double) rect.Bottom - (double) sizeF.Height - 20.0));
            }
          }
        }
        bitmap.Save(filename, ImageFormat.Png);
      }
      if (!mapInfo.MakeWorldFile)
        return;
      using (StreamWriter text = File.CreateText(filename + "w"))
      {
        text.WriteLine("{0:0.000000000000}", (object) (mapInfo.Area.WidthLng / (double) gpoint3.X));
        text.WriteLine("0.0000000");
        text.WriteLine("0.0000000");
        text.WriteLine("{0:0.000000000000}", (object) (-mapInfo.Area.HeightLat / (double) gpoint3.Y));
        text.WriteLine("{0:0.000000000000}", (object) mapInfo.Area.Left);
        text.WriteLine("{0:0.000000000000}", (object) mapInfo.Area.Top);
        text.Close();
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      RectLatLng? nullable = new RectLatLng?();
      if (this.checkBoxRoutes.Checked)
      {
        nullable = this.Main.MainMap.GetRectOfAllRoutes((string) null);
        if (!nullable.HasValue)
        {
          int num = (int) MessageBox.Show("No routes in map", "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
          return;
        }
      }
      else
      {
        nullable = new RectLatLng?(this.Main.MainMap.SelectedArea);
        if (nullable.Value.IsEmpty)
        {
          int num = (int) MessageBox.Show("Select map area holding ALT", "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
          return;
        }
      }
      if (this.bg.IsBusy)
        return;
      lock (this.tileArea)
      {
        this.tileArea.Clear();
        this.tileArea.AddRange((IEnumerable<GPoint>) this.Main.MainMap.MapProvider.Projection.GetAreaTileList(nullable.Value, (int) this.numericUpDown1.Value, 1));
        this.tileArea.TrimExcess();
      }
      this.numericUpDown1.Enabled = false;
      this.progressBar1.Value = 0;
      this.button1.Enabled = false;
      this.bg.RunWorkerAsync((object) new MapInfo(nullable.Value, (int) this.numericUpDown1.Value, this.Main.MainMap.MapProvider, this.checkBoxWorldFile.Checked));
    }

    private void button2_Click(object sender, EventArgs e)
    {
      if (!this.bg.IsBusy)
        return;
      this.bg.CancelAsync();
    }
  }
}
