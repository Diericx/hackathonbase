﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.CustomMarkers.GMapMarkerRect
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System.Drawing;

namespace Demo.WindowsForms.CustomMarkers
{
  public class GMapMarkerRect : GMapMarker
  {
    public Pen Pen;
    public GMapMarkerGoogleGreen InnerMarker;

    public GMapMarkerRect(PointLatLng p)
      : base(p)
    {
      this.Pen = new Pen(Brushes.Blue, 5f);
      this.Size = new Size(111, 111);
      this.Offset = new Point(checked (-this.Size.Width) / 2, checked (-this.Size.Height) / 2);
    }

    public override void OnRender(Graphics g)
    {
      g.DrawRectangle(this.Pen, new Rectangle(this.LocalPosition.X, this.LocalPosition.Y, this.Size.Width, this.Size.Height));
    }
  }
}
