﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.MainForm
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using BSE.Windows.Forms;
using Demo.WindowsForms.CustomMarkers;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.ObjectModel;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace Demo.WindowsForms
{
  public class MainForm : Form
  {
    private readonly Random rnd = new Random();
    private readonly DescendingComparer ComparerIpStatus = new DescendingComparer();
    private string mobileGpsLog = string.Empty;
    private System.Windows.Forms.Timer timerPerf = new System.Windows.Forms.Timer();
    private BackgroundWorker flightWorker = new BackgroundWorker();
    private readonly List<FlightRadarData> flights = new List<FlightRadarData>();
    private readonly Dictionary<int, GMapMarker> flightMarkers = new Dictionary<int, GMapMarker>();
    private bool firstLoadFlight = true;
    private BackgroundWorker transportWorker = new BackgroundWorker();
    private readonly List<VehicleData> trolleybus = new List<VehicleData>();
    private readonly Dictionary<int, GMapMarker> trolleybusMarkers = new Dictionary<int, GMapMarker>();
    private readonly List<VehicleData> bus = new List<VehicleData>();
    private readonly Dictionary<int, GMapMarker> busMarkers = new Dictionary<int, GMapMarker>();
    private bool firstLoadTrasport = true;
    private BackgroundWorker connectionsWorker = new BackgroundWorker();
    private BackgroundWorker ipInfoSearchWorker = new BackgroundWorker();
    private BackgroundWorker iptracerWorker = new BackgroundWorker();
    private readonly Dictionary<string, IpInfo> TcpState = new Dictionary<string, IpInfo>();
    private readonly Dictionary<string, IpInfo> TcpTracePoints = new Dictionary<string, IpInfo>();
    private readonly Dictionary<string, List<PingReply>> TraceRoutes = new Dictionary<string, List<PingReply>>();
    private readonly List<string> TcpStateNeedLocationInfo = new List<string>();
    private readonly System.Collections.Generic.Queue<string> TcpStateNeedtraceInfo = new System.Collections.Generic.Queue<string>();
    private readonly SQLiteIpCache IpCache = new SQLiteIpCache();
    private readonly Dictionary<string, GMapMarker> tcpConnections = new Dictionary<string, GMapMarker>();
    private readonly Dictionary<string, GMapRoute> tcpRoutes = new Dictionary<string, GMapRoute>();
    private readonly List<IpStatus> CountryStatusView = new List<IpStatus>();
    private readonly SortedDictionary<string, int> CountryStatus = new SortedDictionary<string, int>();
    private readonly List<string> SelectedCountries = new List<string>();
    private readonly Dictionary<int, Process> ProcessList = new Dictionary<int, Process>();
    private GMapMarker currentMarker;
    private GMapMarker center;
    private GMapPolygon polygon;
    private GMapOverlay top;
    internal GMapOverlay objects;
    internal GMapOverlay routes;
    internal GMapOverlay polygons;
    private GMapMarkerRect CurentRectMarker;
    private bool isMouseDown;
    private PointLatLng start;
    private PointLatLng end;
    private int tt;
    private GMapMarker currentFlight;
    private GMapMarker currentTransport;
    private volatile bool TryTraceConnection;
    private GMapMarker lastTcpmarker;
    private PointLatLng lastPosition;
    private int lastZoom;
    private bool UserAcceptedLicenseOnce;
    private IContainer components;
    private GroupBox groupBox3;
    private Label label2;
    private Label label1;
    private TextBox textBoxLng;
    private TextBox textBoxLat;
    private Button button8;
    private ComboBox comboBoxMapType;
    private Label label7;
    private TrackBar trackBar1;
    private GroupBox groupBox5;
    private Button button1;
    private Label label6;
    private TextBox textBoxGeo;
    private Button button3;
    private Button button4;
    private Button button5;
    private GroupBox groupBox7;
    private Button button6;
    private CheckBox checkBoxUseGeoCache;
    private CheckBox checkBoxUseRouteCache;
    private GroupBox groupBox8;
    private CheckBox checkBoxCurrentMarker;
    private CheckBox checkBoxCanDrag;
    private Button buttonSetEnd;
    private Button buttonSetStart;
    private CheckBox checkBoxPlacemarkInfo;
    private Button button7;
    private Button button9;
    private Button button10;
    private Button button11;
    private Label label8;
    private ComboBox comboBoxMode;
    private Button button12;
    private CheckBox checkBoxDebug;
    private Button button13;
    private Button button14;
    private DateTimePicker MobileLogFrom;
    private Label label3;
    private Button button15;
    private DateTimePicker MobileLogTo;
    private Button buttonExportToGpx;
    internal Map MainMap;
    private Button button16;
    private BSE.Windows.Forms.Panel panelMenu;
    private XPanderPanelList xPanderPanelList1;
    private XPanderPanel xPanderPanelMain;
    private XPanderPanel xPanderPanelCache;
    private XPanderPanel xPanderPanelLive;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Panel panel4;
    private BSE.Windows.Forms.Splitter splitter1;
    private XPanderPanel xPanderPanelInfo;
    private Label label4;
    private Label label5;
    private TextBox textBoxLngCurrent;
    private TextBox textBoxLatCurrent;
    private Label label9;
    private TextBox textBoxZoomCurrent;
    private Label label10;
    private TextBox textBoxMemory;
    private Label label12;
    private TextBox textBoxrouteCount;
    private Label label11;
    private TextBox textBoxMarkerCount;
    private TableLayoutPanel tableLayoutPanel1;
    private TableLayoutPanel tableLayoutPanel2;
    private TableLayoutPanel tableLayoutPanel3;
    private RadioButton radioButtonNone;
    private CheckBox checkBoxTcpIpSnap;
    private DataGridView GridConnections;
    private DataGridViewTextBoxColumn CountryName;
    private DataGridViewTextBoxColumn ConnectionsCount;
    private RadioButton radioButtonFlight;
    private RadioButton radioButtonPerf;
    private RadioButton radioButtonTcpIp;
    private TableLayoutPanel tableLayoutPanel4;
    private TableLayoutPanel tableLayoutPanel5;
    private CheckBox checkBoxTraceRoute;
    private Button button2;
    private RadioButton radioButtonVehicle;

    public MainForm()
    {
      this.InitializeComponent();
      if (this.DesignMode)
        return;
      try
      {
        Dns.GetHostEntry("www.bing.com");
      }
      catch
      {
        this.MainMap.Manager.Mode = AccessMode.CacheOnly;
        int num = (int) MessageBox.Show("No internet connection avaible, going to CacheOnly mode.", "GMap.NET - Demo.WindowsForms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
      this.MainMap.MapProvider = (GMapProvider) GMapProviders.OpenStreetMap;
      this.MainMap.Position = new PointLatLng(54.6961334816182, 25.2985095977783);
      this.MainMap.MinZoom = 1;
      this.MainMap.MaxZoom = 17;
      this.MainMap.Zoom = 9.0;
      this.MainMap.OnPositionChanged += new PositionChanged(this.MainMap_OnPositionChanged);
      this.MainMap.OnTileLoadStart += new TileLoadStart(this.MainMap_OnTileLoadStart);
      this.MainMap.OnTileLoadComplete += new TileLoadComplete(this.MainMap_OnTileLoadComplete);
      this.MainMap.OnMarkerClick += new MarkerClick(this.MainMap_OnMarkerClick);
      this.MainMap.OnMapZoomChanged += new MapZoomChanged(this.MainMap_OnMapZoomChanged);
      this.MainMap.OnMapTypeChanged += new MapTypeChanged(this.MainMap_OnMapTypeChanged);
      this.MainMap.MouseMove += new MouseEventHandler(this.MainMap_MouseMove);
      this.MainMap.MouseDown += new MouseEventHandler(this.MainMap_MouseDown);
      this.MainMap.MouseUp += new MouseEventHandler(this.MainMap_MouseUp);
      this.MainMap.OnMarkerEnter += new MarkerEnter(this.MainMap_OnMarkerEnter);
      this.MainMap.OnMarkerLeave += new MarkerLeave(this.MainMap_OnMarkerLeave);
      this.comboBoxMapType.ValueMember = "Name";
      this.comboBoxMapType.DataSource = (object) GMapProviders.List;
      this.comboBoxMapType.SelectedItem = (object) this.MainMap.MapProvider;
      this.comboBoxMode.DataSource = (object) System.Enum.GetValues(typeof (AccessMode));
      this.comboBoxMode.SelectedItem = (object) Singleton<GMaps>.Instance.Mode;
      this.textBoxLat.Text = this.MainMap.Position.Lat.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      this.textBoxLng.Text = this.MainMap.Position.Lng.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      this.checkBoxUseRouteCache.Checked = Singleton<GMaps>.Instance.UseRouteCache;
      this.checkBoxUseGeoCache.Checked = Singleton<GMaps>.Instance.UseGeocoderCache;
      this.MobileLogFrom.Value = DateTime.Today;
      this.MobileLogTo.Value = DateTime.Now;
      this.trackBar1.Minimum = this.MainMap.MinZoom;
      this.trackBar1.Maximum = this.MainMap.MaxZoom;
      ToolStripManager.Renderer = (ToolStripRenderer) new Office2007Renderer();
      this.flightWorker.DoWork += new DoWorkEventHandler(this.flight_DoWork);
      this.flightWorker.ProgressChanged += new ProgressChangedEventHandler(this.flight_ProgressChanged);
      this.flightWorker.WorkerSupportsCancellation = true;
      this.flightWorker.WorkerReportsProgress = true;
      this.transportWorker.DoWork += new DoWorkEventHandler(this.transport_DoWork);
      this.transportWorker.ProgressChanged += new ProgressChangedEventHandler(this.transport_ProgressChanged);
      this.transportWorker.WorkerSupportsCancellation = true;
      this.transportWorker.WorkerReportsProgress = true;
      this.connectionsWorker.DoWork += new DoWorkEventHandler(this.connectionsWorker_DoWork);
      this.connectionsWorker.ProgressChanged += new ProgressChangedEventHandler(this.connectionsWorker_ProgressChanged);
      this.connectionsWorker.WorkerSupportsCancellation = true;
      this.connectionsWorker.WorkerReportsProgress = true;
      this.ipInfoSearchWorker.DoWork += new DoWorkEventHandler(this.ipInfoSearchWorker_DoWork);
      this.ipInfoSearchWorker.WorkerSupportsCancellation = true;
      this.iptracerWorker.DoWork += new DoWorkEventHandler(this.iptracerWorker_DoWork);
      this.iptracerWorker.WorkerSupportsCancellation = true;
      this.GridConnections.AutoGenerateColumns = false;
      this.IpCache.CacheLocation = this.MainMap.CacheLocation;
      this.timerPerf.Tick += new EventHandler(this.timer_Tick);
      this.routes = new GMapOverlay((GMapControl) this.MainMap, "routes");
      this.MainMap.Overlays.Add(this.routes);
      this.polygons = new GMapOverlay((GMapControl) this.MainMap, "polygons");
      this.MainMap.Overlays.Add(this.polygons);
      this.objects = new GMapOverlay((GMapControl) this.MainMap, "objects");
      this.MainMap.Overlays.Add(this.objects);
      this.top = new GMapOverlay((GMapControl) this.MainMap, "top");
      this.MainMap.Overlays.Add(this.top);
      this.routes.Routes.CollectionChanged += new NotifyCollectionChangedEventHandler(this.Routes_CollectionChanged);
      this.objects.Markers.CollectionChanged += new NotifyCollectionChangedEventHandler(this.Markers_CollectionChanged);
      this.currentMarker = (GMapMarker) new GMapMarkerGoogleRed(this.MainMap.Position);
      this.currentMarker.IsHitTestVisible = false;
      this.top.Markers.Add(this.currentMarker);
      this.center = (GMapMarker) new GMapMarkerCross(this.MainMap.Position);
      this.top.Markers.Add(this.center);
      GeoCoderStatusCode status = GeoCoderStatusCode.Unknow;
      PointLatLng? point = GMapProviders.GoogleMap.GetPoint("Lithuania, Vilnius", out status);
      if (point.HasValue && status == GeoCoderStatusCode.G_GEO_SUCCESS)
      {
        this.currentMarker.Position = point.Value;
        GMapMarker gmapMarker = (GMapMarker) new GMapMarkerGoogleGreen(point.Value);
        gmapMarker.ToolTipMode = MarkerTooltipMode.Always;
        gmapMarker.ToolTipText = "Welcome to Lithuania! ;}";
        this.objects.Markers.Add(gmapMarker);
      }
      this.AddLocationLithuania("Kaunas");
      this.AddLocationLithuania("Klaipėda");
      this.AddLocationLithuania("Šiauliai");
      this.AddLocationLithuania("Panevėžys");
      this.RegeneratePolygon();
    }

    private void Markers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      this.textBoxMarkerCount.Text = this.objects.Markers.Count.ToString();
    }

    private void Routes_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      this.textBoxrouteCount.Text = this.routes.Routes.Count.ToString();
    }

    private double NextDouble(Random rng, double min, double max)
    {
      return min + rng.NextDouble() * (max - min);
    }

    private void timer_Tick(object sender, EventArgs e)
    {
      GMapMarker gmapMarker = (GMapMarker) new GMapMarkerGoogleGreen(new PointLatLng(this.NextDouble(this.rnd, this.MainMap.CurrentViewArea.Top, this.MainMap.CurrentViewArea.Bottom), this.NextDouble(this.rnd, this.MainMap.CurrentViewArea.Left, this.MainMap.CurrentViewArea.Right)));
      gmapMarker.ToolTipText = checked (this.tt++).ToString();
      gmapMarker.ToolTipMode = MarkerTooltipMode.Always;
      this.objects.Markers.Add(gmapMarker);
      if (this.tt < 333)
        return;
      this.timerPerf.Stop();
      this.tt = 0;
    }

    private void flight_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      this.MainMap.HoldInvalidation = true;
      lock (this.flights)
      {
        foreach (FlightRadarData item_0 in this.flights)
        {
          GMapMarker local_1;
          if (!this.flightMarkers.TryGetValue(item_0.Id, out local_1))
          {
            local_1 = (GMapMarker) new GMapMarkerGoogleGreen(item_0.point);
            local_1.Tag = (object) item_0.Id;
            local_1.ToolTipMode = MarkerTooltipMode.OnMouseOver;
            (local_1 as GMapMarkerGoogleGreen).Bearing = new float?((float) item_0.bearing);
            this.flightMarkers[item_0.Id] = local_1;
            this.objects.Markers.Add(local_1);
          }
          else
          {
            local_1.Position = item_0.point;
            (local_1 as GMapMarkerGoogleGreen).Bearing = new float?((float) item_0.bearing);
          }
          local_1.ToolTipText = item_0.name + ", " + item_0.altitude + ", " + item_0.speed;
          if (this.currentFlight != null && this.currentFlight == local_1)
          {
            this.MainMap.Position = local_1.Position;
            this.MainMap.Bearing = (float) item_0.bearing;
          }
        }
      }
      if (this.firstLoadFlight)
      {
        this.MainMap.Zoom = 5.0;
        this.MainMap.ZoomAndCenterMarkers("objects");
        this.firstLoadFlight = false;
      }
      this.MainMap.Refresh();
    }

    private void flight_DoWork(object sender, DoWorkEventArgs e)
    {
      bool resetSession = true;
      while (!this.flightWorker.CancellationPending)
      {
        try
        {
          lock (this.flights)
          {
            Stuff.GetFlightRadarData(this.flights, this.lastPosition, this.lastZoom, resetSession);
            if (this.flights.Count > 0)
            {
              if (resetSession)
                resetSession = false;
            }
          }
          this.flightWorker.ReportProgress(100);
        }
        catch (Exception ex)
        {
        }
        Thread.Sleep(5000);
      }
      this.flightMarkers.Clear();
    }

    private void transport_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      this.MainMap.HoldInvalidation = true;
      lock (this.trolleybus)
      {
        foreach (VehicleData item_0 in this.trolleybus)
        {
          GMapMarker local_1;
          if (!this.trolleybusMarkers.TryGetValue(item_0.Id, out local_1))
          {
            local_1 = (GMapMarker) new GMapMarkerGoogleRed(new PointLatLng(item_0.Lat, item_0.Lng));
            local_1.Tag = (object) item_0.Id;
            local_1.ToolTipMode = MarkerTooltipMode.OnMouseOver;
            this.trolleybusMarkers[item_0.Id] = local_1;
            this.objects.Markers.Add(local_1);
          }
          else
          {
            local_1.Position = new PointLatLng(item_0.Lat, item_0.Lng);
            GMapMarkerGoogleRed temp_25 = local_1 as GMapMarkerGoogleRed;
            double? local_6 = item_0.Bearing;
            float? temp_34 = local_6.HasValue ? new float?((float) local_6.GetValueOrDefault()) : new float?();
            temp_25.Bearing = temp_34;
          }
          local_1.ToolTipText = "Trolley " + item_0.Line + (item_0.Bearing.HasValue ? ", bearing: " + item_0.Bearing.Value.ToString() : string.Empty) + ", " + item_0.Time;
          if (this.currentTransport != null && this.currentTransport == local_1)
          {
            this.MainMap.Position = local_1.Position;
            if (item_0.Bearing.HasValue)
              this.MainMap.Bearing = (float) item_0.Bearing.Value;
          }
        }
      }
      lock (this.bus)
      {
        foreach (VehicleData item_1 in this.bus)
        {
          GMapMarker local_3;
          if (!this.busMarkers.TryGetValue(item_1.Id, out local_3))
          {
            local_3 = (GMapMarker) new GMapMarkerGoogleGreen(new PointLatLng(item_1.Lat, item_1.Lng));
            local_3.Tag = (object) item_1.Id;
            local_3.ToolTipMode = MarkerTooltipMode.OnMouseOver;
            this.busMarkers[item_1.Id] = local_3;
            this.objects.Markers.Add(local_3);
          }
          else
          {
            local_3.Position = new PointLatLng(item_1.Lat, item_1.Lng);
            GMapMarkerGoogleGreen temp_136 = local_3 as GMapMarkerGoogleGreen;
            double? local_16 = item_1.Bearing;
            float? temp_145 = local_16.HasValue ? new float?((float) local_16.GetValueOrDefault()) : new float?();
            temp_136.Bearing = temp_145;
          }
          local_3.ToolTipText = "Bus " + item_1.Line + (item_1.Bearing.HasValue ? ", bearing: " + item_1.Bearing.Value.ToString() : string.Empty) + ", " + item_1.Time;
          if (this.currentTransport != null && this.currentTransport == local_3)
          {
            this.MainMap.Position = local_3.Position;
            if (item_1.Bearing.HasValue)
              this.MainMap.Bearing = (float) item_1.Bearing.Value;
          }
        }
      }
      if (this.firstLoadTrasport)
      {
        this.MainMap.Zoom = 5.0;
        this.MainMap.ZoomAndCenterMarkers("objects");
        this.firstLoadTrasport = false;
      }
      this.MainMap.Refresh();
    }

    private void transport_DoWork(object sender, DoWorkEventArgs e)
    {
      while (!this.transportWorker.CancellationPending)
      {
        try
        {
          lock (this.trolleybus)
            Stuff.GetVilniusTransportData(TransportType.TrolleyBus, string.Empty, this.trolleybus);
          lock (this.bus)
            Stuff.GetVilniusTransportData(TransportType.Bus, string.Empty, this.bus);
          this.transportWorker.ReportProgress(100);
        }
        catch (Exception ex)
        {
        }
        Thread.Sleep(2000);
      }
      this.trolleybusMarkers.Clear();
      this.busMarkers.Clear();
    }

    private void ipInfoSearchWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      while (!this.ipInfoSearchWorker.CancellationPending)
      {
        try
        {
          string iplist = string.Empty;
          lock (this.TcpStateNeedLocationInfo)
          {
            using (List<string>.Enumerator resource_0 = this.TcpStateNeedLocationInfo.GetEnumerator())
            {
              if (resource_0.MoveNext())
              {
                string local_1 = resource_0.Current;
                if (iplist.Length > 0)
                  iplist += ",";
                iplist += local_1;
              }
            }
          }
          if (string.IsNullOrEmpty(iplist))
            break;
          List<IpInfo> ipHostInfo = this.GetIpHostInfo(iplist);
          foreach (IpInfo ipInfo in ipHostInfo)
          {
            lock (this.TcpState)
            {
              IpInfo local_4;
              if (this.TcpState.TryGetValue(ipInfo.Ip, out local_4))
              {
                local_4.CountryName = ipInfo.CountryName;
                local_4.RegionName = ipInfo.RegionName;
                local_4.City = ipInfo.City;
                local_4.Latitude = ipInfo.Latitude;
                local_4.Longitude = ipInfo.Longitude;
                local_4.TracePoint = false;
                if (local_4.CountryName != "Reserved")
                {
                  local_4.Ip = ipInfo.Ip;
                  lock (this.TcpStateNeedtraceInfo)
                  {
                    if (!this.TcpStateNeedtraceInfo.Contains(ipInfo.Ip))
                      this.TcpStateNeedtraceInfo.Enqueue(ipInfo.Ip);
                  }
                }
                lock (this.TcpStateNeedLocationInfo)
                  this.TcpStateNeedLocationInfo.Remove(ipInfo.Ip);
              }
            }
          }
          ipHostInfo.Clear();
        }
        catch (Exception ex)
        {
        }
        Thread.Sleep(1111);
      }
    }

    private void iptracerWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      while (!this.iptracerWorker.CancellationPending)
      {
        try
        {
          string index1 = string.Empty;
          lock (this.TcpStateNeedtraceInfo)
          {
            if (this.TcpStateNeedtraceInfo.Count > 0)
              index1 = this.TcpStateNeedtraceInfo.Dequeue();
          }
          if (string.IsNullOrEmpty(index1))
            break;
          string iplist = string.Empty;
          bool flag = false;
          List<PingReply> traceRoute;
          lock (this.TraceRoutes)
            flag = this.TraceRoutes.TryGetValue(index1, out traceRoute);
          if (!flag)
          {
            traceRoute = TraceRoute.GetTraceRoute(index1);
            if (traceRoute != null)
            {
              if (traceRoute[checked (traceRoute.Count - 1)].Status == IPStatus.Success)
              {
                foreach (PingReply pingReply in traceRoute)
                {
                  if (!pingReply.ToString().StartsWith("192.168.") && !pingReply.ToString().StartsWith("127.0."))
                  {
                    if (iplist.Length > 0)
                      iplist += ",";
                    iplist += pingReply.Address.ToString();
                  }
                }
                if (!string.IsNullOrEmpty(iplist))
                {
                  List<IpInfo> ipHostInfo = this.GetIpHostInfo(iplist);
                  if (ipHostInfo.Count > 0)
                  {
                    int index2 = 0;
                    while (index2 < ipHostInfo.Count)
                    {
                      IpInfo ipInfo = ipHostInfo[index2];
                      ipInfo.TracePoint = true;
                      if (ipInfo.CountryName != "Reserved")
                      {
                        lock (this.TcpTracePoints)
                          this.TcpTracePoints[ipInfo.Ip] = ipInfo;
                      }
                      checked { ++index2; }
                    }
                    ipHostInfo.Clear();
                    lock (this.TraceRoutes)
                      this.TraceRoutes[index1] = traceRoute;
                  }
                }
              }
              else
              {
                lock (this.TcpStateNeedtraceInfo)
                  this.TcpStateNeedtraceInfo.Enqueue(index1);
              }
            }
          }
        }
        catch (Exception ex)
        {
        }
        Thread.Sleep(3333);
      }
    }

    private void connectionsWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      IPGlobalProperties.GetIPGlobalProperties();
      while (!this.connectionsWorker.CancellationPending)
      {
        try
        {
          lock (this.TcpState)
          {
            this.CountryStatus.Clear();
            ManagedIpHelper.UpdateExtendedTcpTable(false);
            foreach (TcpRow item_0 in ManagedIpHelper.TcpRows)
            {
              string local_1 = item_0.RemoteEndPoint.Address.ToString();
              if (!local_1.StartsWith("192.168.") && !local_1.StartsWith("127.0."))
              {
                IpInfo local_2;
                if (!this.TcpState.TryGetValue(local_1, out local_2))
                {
                  local_2 = new IpInfo();
                  this.TcpState[local_1] = local_2;
                  lock (this.TcpStateNeedLocationInfo)
                  {
                    if (!this.TcpStateNeedLocationInfo.Contains(local_1))
                    {
                      this.TcpStateNeedLocationInfo.Add(local_1);
                      if (!this.ipInfoSearchWorker.IsBusy)
                        this.ipInfoSearchWorker.RunWorkerAsync();
                    }
                  }
                }
                local_2.State = item_0.State;
                local_2.Port = item_0.RemoteEndPoint.Port;
                local_2.StatusTime = DateTime.Now;
                try
                {
                  Process local_3;
                  if (!this.ProcessList.TryGetValue(item_0.ProcessId, out local_3))
                  {
                    local_3 = Process.GetProcessById(item_0.ProcessId);
                    this.ProcessList[item_0.ProcessId] = local_3;
                  }
                  local_2.ProcessName = local_3.ProcessName;
                }
                catch
                {
                }
                if (!string.IsNullOrEmpty(local_2.CountryName))
                {
                  if (!this.CountryStatus.ContainsKey(local_2.CountryName))
                  {
                    this.CountryStatus[local_2.CountryName] = 1;
                  }
                  else
                  {
                    SortedDictionary<string, int> local_7;
                    string local_8;
                    (local_7 = this.CountryStatus)[local_8 = local_2.CountryName] = checked (local_7[local_8] + 1);
                  }
                }
              }
            }
          }
          if (this.TryTraceConnection && !this.iptracerWorker.IsBusy)
          {
            lock (this.TcpStateNeedtraceInfo)
            {
              if (this.TcpStateNeedtraceInfo.Count > 0)
                this.iptracerWorker.RunWorkerAsync();
            }
          }
          this.connectionsWorker.ReportProgress(100);
        }
        catch (Exception ex)
        {
        }
        Thread.Sleep(3333);
      }
      this.tcpConnections.Clear();
    }

    private void connectionsWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      try
      {
        this.MainMap.HoldInvalidation = true;
        this.SelectedCountries.Clear();
        int rowCount = this.GridConnections.Rows.GetRowCount(DataGridViewElementStates.Selected);
        if (rowCount > 0)
        {
          int index = 0;
          while (index < rowCount)
          {
            this.SelectedCountries.Add(this.GridConnections.SelectedRows[index].Cells[0].Value as string);
            checked { ++index; }
          }
        }
        this.ComparerIpStatus.SortOnlyCountryName = rowCount != 0;
        lock (this.TcpState)
        {
          bool local_3 = true;
          foreach (KeyValuePair<string, IpInfo> item_1 in this.TcpState)
          {
            GMapMarker local_5;
            if (!this.tcpConnections.TryGetValue(item_1.Key, out local_5))
            {
              if (!string.IsNullOrEmpty(item_1.Value.Ip))
              {
                local_5 = (GMapMarker) new GMapMarkerGoogleGreen(new PointLatLng(item_1.Value.Latitude, item_1.Value.Longitude));
                local_5.ToolTipMode = MarkerTooltipMode.OnMouseOver;
                local_5.Tag = (object) item_1.Value.CountryName;
                this.tcpConnections[item_1.Key] = local_5;
                if (rowCount <= 0 || this.SelectedCountries.Contains(item_1.Value.CountryName))
                {
                  this.objects.Markers.Add(local_5);
                  this.UpdateMarkerTcpIpToolTip(local_5, item_1.Value, "(" + (object) this.objects.Markers.Count + ") ");
                  if (local_3)
                  {
                    if (this.checkBoxTcpIpSnap.Checked && !this.MainMap.IsDragging)
                      this.MainMap.Position = local_5.Position;
                    local_3 = false;
                    if (this.lastTcpmarker != null)
                    {
                      local_5.ToolTipMode = MarkerTooltipMode.Always;
                      this.lastTcpmarker.ToolTipMode = MarkerTooltipMode.OnMouseOver;
                    }
                    this.lastTcpmarker = local_5;
                  }
                }
              }
            }
            else if (DateTime.Now - item_1.Value.StatusTime > TimeSpan.FromSeconds(8.0) || rowCount > 0 && !this.SelectedCountries.Contains(item_1.Value.CountryName))
            {
              this.objects.Markers.Remove(local_5);
              GMapRoute local_6;
              if (this.tcpRoutes.TryGetValue(item_1.Key, out local_6))
                this.routes.Routes.Remove(local_6);
              lock (this.TcpStateNeedLocationInfo)
                this.TcpStateNeedLocationInfo.Remove(item_1.Key);
            }
            else
            {
              local_5.Position = new PointLatLng(item_1.Value.Latitude, item_1.Value.Longitude);
              if (!this.objects.Markers.Contains(local_5))
                this.objects.Markers.Add(local_5);
              this.UpdateMarkerTcpIpToolTip(local_5, item_1.Value, string.Empty);
              if (this.TryTraceConnection)
              {
                GMapRoute local_8;
                if (!this.tcpRoutes.TryGetValue(item_1.Key, out local_8))
                {
                  lock (this.TraceRoutes)
                  {
                    List<PingReply> local_9;
                    if (this.TraceRoutes.TryGetValue(item_1.Key, out local_9))
                    {
                      if (local_9 != null)
                      {
                        List<PointLatLng> local_10 = new List<PointLatLng>();
                        foreach (PingReply item_0 in local_9)
                        {
                          lock (this.TcpTracePoints)
                          {
                            IpInfo local_12;
                            if (this.TcpTracePoints.TryGetValue(item_0.Address.ToString(), out local_12))
                            {
                              if (!string.IsNullOrEmpty(local_12.Ip))
                                local_10.Add(new PointLatLng(local_12.Latitude, local_12.Longitude));
                            }
                          }
                        }
                        if (local_10.Count > 0)
                        {
                          local_8 = new GMapRoute(local_10, item_1.Value.CountryName);
                          local_8.Stroke = new Pen(this.GetRandomColor());
                          local_8.Stroke.Width = 4f;
                          local_8.Stroke.DashStyle = DashStyle.DashDot;
                          local_8.Stroke.StartCap = LineCap.NoAnchor;
                          local_8.Stroke.EndCap = LineCap.ArrowAnchor;
                          local_8.Stroke.LineJoin = LineJoin.Round;
                          this.routes.Routes.Add(local_8);
                          this.tcpRoutes[item_1.Key] = local_8;
                        }
                      }
                    }
                  }
                }
                else if (!this.routes.Routes.Contains(local_8))
                  this.routes.Routes.Add(local_8);
              }
            }
          }
          if (this.panelMenu.Expand)
          {
            if (this.xPanderPanelLive.Expand)
            {
              bool local_13 = this.CountryStatusView.Count == 0;
              if (!this.ComparerIpStatus.SortOnlyCountryName)
                this.CountryStatusView.Clear();
              using (SortedDictionary<string, int>.Enumerator resource_0 = this.CountryStatus.GetEnumerator())
              {
                while (resource_0.MoveNext())
                {
                  KeyValuePair<string, int> c = resource_0.Current;
                  IpStatus local_14 = new IpStatus();
                  local_14.CountryName = c.Key;
                  local_14.ConnectionsCount = c.Value;
                  if (this.ComparerIpStatus.SortOnlyCountryName)
                  {
                    int local_15 = this.CountryStatusView.FindIndex((Predicate<IpStatus>) (p => p.CountryName == c.Key));
                    if (local_15 >= 0)
                      this.CountryStatusView[local_15] = local_14;
                  }
                  else
                    this.CountryStatusView.Add(local_14);
                }
              }
              this.CountryStatusView.Sort((IComparer<IpStatus>) this.ComparerIpStatus);
              this.GridConnections.RowCount = this.CountryStatusView.Count;
              this.GridConnections.Refresh();
              if (local_13)
                this.GridConnections.ClearSelection();
            }
          }
        }
        this.MainMap.Refresh();
      }
      catch (Exception ex)
      {
      }
    }

    private void GridConnections_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
    {
      if (e.RowIndex >= this.CountryStatusView.Count)
        return;
      IpStatus ipStatus = this.CountryStatusView[e.RowIndex];
      switch (this.GridConnections.Columns[e.ColumnIndex].Name)
      {
        case "CountryName":
          e.Value = (object) ipStatus.CountryName;
          break;
        case "ConnectionsCount":
          e.Value = (object) ipStatus.ConnectionsCount;
          break;
      }
    }

    private Color GetRandomColor()
    {
      return Color.FromArgb(144, (int) Convert.ToByte(this.rnd.Next(0, 111)), (int) Convert.ToByte(this.rnd.Next(0, 111)), (int) Convert.ToByte(this.rnd.Next(0, 111)));
    }

    private void UpdateMarkerTcpIpToolTip(GMapMarker marker, IpInfo tcp, string info)
    {
      marker.ToolTipText = tcp.State.ToString();
      if (!string.IsNullOrEmpty(tcp.ProcessName))
      {
        GMapMarker gmapMarker = marker;
        string str = gmapMarker.ToolTipText + " by " + tcp.ProcessName;
        gmapMarker.ToolTipText = str;
      }
      if (!string.IsNullOrEmpty(tcp.CountryName))
      {
        GMapMarker gmapMarker = marker;
        string str = gmapMarker.ToolTipText + "\n" + tcp.CountryName;
        gmapMarker.ToolTipText = str;
      }
      if (!string.IsNullOrEmpty(tcp.City))
      {
        GMapMarker gmapMarker = marker;
        string str = gmapMarker.ToolTipText + ", " + tcp.City;
        gmapMarker.ToolTipText = str;
      }
      else if (!string.IsNullOrEmpty(tcp.RegionName))
      {
        GMapMarker gmapMarker = marker;
        string str = gmapMarker.ToolTipText + ", " + tcp.RegionName;
        gmapMarker.ToolTipText = str;
      }
      GMapMarker gmapMarker1 = marker;
      string str1 = gmapMarker1.ToolTipText + (object) "\n" + tcp.Ip + ":" + (string) (object) tcp.Port + "\n" + info;
      gmapMarker1.ToolTipText = str1;
    }

    private List<IpInfo> GetIpHostInfo(string iplist)
    {
      List<IpInfo> list = new List<IpInfo>();
      bool flag = false;
      string str1 = string.Empty;
      string str2 = iplist;
      char[] chArray = new char[1]
      {
        ','
      };
      foreach (string ip in str2.Split(chArray))
      {
        IpInfo dataFromCache = this.IpCache.GetDataFromCache(ip);
        if (dataFromCache != null)
        {
          list.Add(dataFromCache);
        }
        else
        {
          if (str1.Length > 0)
            str1 += ",";
          str1 += ip;
        }
      }
      if (!string.IsNullOrEmpty(str1))
      {
        string requestUriString = string.Format("http://api.ipinfodb.com/v2/ip_query.php?key=fbea1992ab11f7125064590a417a8461ccaf06728798c718dbd2809b31a7a5e0&ip={0}&timezone=false", (object) str1);
        while (true)
        {
          list.Clear();
          try
          {
            HttpWebRequest httpWebRequest = WebRequest.Create(requestUriString) as HttpWebRequest;
            string xml = string.Empty;
            using (HttpWebResponse httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse)
            {
              using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
                xml = streamReader.ReadToEnd();
              httpWebResponse.Close();
            }
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xml);
            IEnumerator enumerator = xmlDocument.SelectNodes("/Response").GetEnumerator();
            try
            {
              while (enumerator.MoveNext())
              {
                XmlNode xmlNode = (XmlNode) enumerator.Current;
                string innerText = xmlNode.SelectSingleNode("Ip").InnerText;
                IpInfo data = new IpInfo();
                data.Ip = innerText;
                data.CountryName = xmlNode.SelectSingleNode("CountryName").InnerText;
                data.RegionName = xmlNode.SelectSingleNode("RegionName").InnerText;
                data.City = xmlNode.SelectSingleNode("City").InnerText;
                data.Latitude = double.Parse(xmlNode.SelectSingleNode("Latitude").InnerText, (IFormatProvider) CultureInfo.InvariantCulture);
                data.Longitude = double.Parse(xmlNode.SelectSingleNode("Longitude").InnerText, (IFormatProvider) CultureInfo.InvariantCulture);
                data.CacheTime = DateTime.Now;
                list.Add(data);
                this.IpCache.PutDataToCache(innerText, data);
              }
              break;
            }
            finally
            {
              IDisposable disposable = enumerator as IDisposable;
              if (disposable != null)
                disposable.Dispose();
            }
          }
          catch (Exception ex)
          {
            if (!flag)
            {
              flag = true;
              requestUriString = string.Format("http://backup.ipinfodb.com/v2/ip_query.php?key=fbea1992ab11f7125064590a417a8461ccaf06728798c718dbd2809b31a7a5e0&ip={0}&timezone=false", (object) iplist);
            }
            else
              break;
          }
        }
      }
      return list;
    }

    private void RegeneratePolygon()
    {
      List<PointLatLng> points = new List<PointLatLng>();
      foreach (GMapMarker gmapMarker in (Collection<GMapMarker>) this.objects.Markers)
      {
        if (gmapMarker is GMapMarkerRect)
        {
          gmapMarker.Tag = (object) points.Count;
          points.Add(gmapMarker.Position);
        }
      }
      if (this.polygon == null)
      {
        this.polygon = new GMapPolygon(points, "polygon test");
        this.polygons.Polygons.Add(this.polygon);
      }
      else
      {
        this.polygon.Points.Clear();
        this.polygon.Points.AddRange((IEnumerable<PointLatLng>) points);
        if (this.polygons.Polygons.Count == 0)
          this.polygons.Polygons.Add(this.polygon);
        else
          this.MainMap.UpdatePolygonLocalPosition(this.polygon);
      }
    }

    private void AddGpsMobileLogRoutes(string file)
    {
      try
      {
        DateTime? start = new DateTime?();
        DateTime? end = new DateTime?();
        if (this.MobileLogFrom.Checked)
          start = new DateTime?(this.MobileLogFrom.Value.ToUniversalTime());
        if (this.MobileLogTo.Checked)
          end = new DateTime?(this.MobileLogTo.Value.ToUniversalTime());
        IEnumerable<List<GpsLog>> routesFromMobileLog = Stuff.GetRoutesFromMobileLog(file, start, end, new double?(3.3));
        if (this.routes == null)
          return;
        List<PointLatLng> points = new List<PointLatLng>();
        List<List<GpsLog>> list1 = new List<List<GpsLog>>(routesFromMobileLog);
        foreach (List<GpsLog> list2 in list1)
        {
          points.Clear();
          foreach (GpsLog gpsLog in list2)
            points.Add(gpsLog.Position);
          this.routes.Routes.Add(new GMapRoute(points, ""));
        }
        list1.Clear();
        points.Clear();
      }
      catch (Exception ex)
      {
      }
    }

    private void AddLocationLithuania(string place)
    {
      GeoCoderStatusCode status = GeoCoderStatusCode.Unknow;
      PointLatLng? point = GMapProviders.GoogleMap.GetPoint("Lithuania, " + place, out status);
      if (!point.HasValue || status != GeoCoderStatusCode.G_GEO_SUCCESS)
        return;
      GMapMarkerGoogleGreen markerGoogleGreen = new GMapMarkerGoogleGreen(point.Value);
      markerGoogleGreen.ToolTip = (GMapToolTip) new GMapRoundedToolTip((GMapMarker) markerGoogleGreen);
      GMapMarkerRect gmapMarkerRect = new GMapMarkerRect(point.Value);
      gmapMarkerRect.InnerMarker = markerGoogleGreen;
      gmapMarkerRect.ToolTipText = place;
      gmapMarkerRect.ToolTipMode = MarkerTooltipMode.Always;
      this.objects.Markers.Add((GMapMarker) markerGoogleGreen);
      this.objects.Markers.Add((GMapMarker) gmapMarkerRect);
    }

    private void AddTmpPoint(string place, PointLatLng pos)
    {
      GMapMarkerGoogleGreen markerGoogleGreen = new GMapMarkerGoogleGreen(pos);
      markerGoogleGreen.ToolTipText = place;
      markerGoogleGreen.ToolTipMode = MarkerTooltipMode.Always;
      GMapMarkerRect gmapMarkerRect = new GMapMarkerRect(pos);
      gmapMarkerRect.InnerMarker = markerGoogleGreen;
      gmapMarkerRect.ToolTipText = place;
      gmapMarkerRect.ToolTipMode = MarkerTooltipMode.Always;
      gmapMarkerRect.IsVisible = false;
      this.objects.Markers.Add((GMapMarker) markerGoogleGreen);
      this.objects.Markers.Add((GMapMarker) gmapMarkerRect);
    }

    private void MainMap_OnMarkerLeave(GMapMarker item)
    {
      if (!(item is GMapMarkerRect))
        return;
      this.CurentRectMarker = (GMapMarkerRect) null;
      (item as GMapMarkerRect).Pen.Color = Color.Blue;
      this.MainMap.Invalidate(false);
    }

    private void MainMap_OnMarkerEnter(GMapMarker item)
    {
      if (!(item is GMapMarkerRect))
        return;
      GMapMarkerRect gmapMarkerRect = item as GMapMarkerRect;
      gmapMarkerRect.Pen.Color = Color.Red;
      this.MainMap.Invalidate(false);
      this.CurentRectMarker = gmapMarkerRect;
    }

    private void MainMap_OnMapTypeChanged(GMapProvider type)
    {
      this.comboBoxMapType.SelectedItem = (object) type;
      this.trackBar1.Minimum = this.MainMap.MinZoom;
      this.trackBar1.Maximum = this.MainMap.MaxZoom;
      if (!this.radioButtonFlight.Checked)
        return;
      this.MainMap.ZoomAndCenterMarkers("objects");
    }

    private void MainMap_MouseUp(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Left)
        return;
      this.isMouseDown = false;
    }

    private void MainMap_MouseDown(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Left)
        return;
      this.isMouseDown = true;
      if (!this.currentMarker.IsVisible)
        return;
      this.currentMarker.Position = this.MainMap.FromLocalToLatLng(e.X, e.Y);
      this.MainMap.MapProvider.Projection.FromPixelToTileXY(this.MainMap.MapProvider.Projection.FromLatLngToPixel(this.currentMarker.Position.Lat, this.currentMarker.Position.Lng, checked ((int) this.MainMap.Zoom)));
    }

    private void MainMap_MouseMove(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Left || !this.isMouseDown)
        return;
      if (this.CurentRectMarker == null)
      {
        if (this.currentMarker.IsVisible)
          this.currentMarker.Position = this.MainMap.FromLocalToLatLng(e.X, e.Y);
      }
      else
      {
        PointLatLng pointLatLng = this.MainMap.FromLocalToLatLng(e.X, e.Y);
        int? nullable1 = (int?) this.CurentRectMarker.Tag;
        if (nullable1.HasValue)
        {
          int? nullable2 = nullable1;
          int count = this.polygon.Points.Count;
          if ((nullable2.GetValueOrDefault() >= count ? 0 : (nullable2.HasValue ? 1 : 0)) != 0)
          {
            this.polygon.Points[nullable1.Value] = pointLatLng;
            this.MainMap.UpdatePolygonLocalPosition(this.polygon);
          }
        }
        if (this.currentMarker.IsVisible)
          this.currentMarker.Position = pointLatLng;
        this.CurentRectMarker.Position = pointLatLng;
        if (this.CurentRectMarker.InnerMarker != null)
          this.CurentRectMarker.InnerMarker.Position = pointLatLng;
      }
      this.MainMap.Refresh();
    }

    private void MainMap_OnMapZoomChanged()
    {
      this.trackBar1.Value = checked ((int) this.MainMap.Zoom);
      this.textBoxZoomCurrent.Text = this.MainMap.Zoom.ToString();
      this.center.Position = this.MainMap.Position;
    }

    private void MainMap_OnMarkerClick(GMapMarker item, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Left)
        return;
      if (item is GMapMarkerRect)
      {
        GeoCoderStatusCode status;
        Placemark placemark = GMapProviders.GoogleMap.GetPlacemark(item.Position, out status);
        if (status != GeoCoderStatusCode.G_GEO_SUCCESS || placemark == null)
          return;
        (item as GMapMarkerRect).ToolTipText = placemark.Address;
        this.MainMap.Invalidate(false);
      }
      else
      {
        if (item.Tag == null)
          return;
        if (this.currentTransport != null)
        {
          this.currentTransport.ToolTipMode = MarkerTooltipMode.OnMouseOver;
          this.currentTransport = (GMapMarker) null;
        }
        this.currentTransport = item;
        this.currentTransport.ToolTipMode = MarkerTooltipMode.Always;
      }
    }

    private void MainMap_OnTileLoadStart()
    {
      MethodInvoker methodInvoker = (MethodInvoker) (() => this.panelMenu.Text = "Menu: loading tiles...");
      try
      {
        this.BeginInvoke((Delegate) methodInvoker);
      }
      catch
      {
      }
    }

    private void MainMap_OnTileLoadComplete(long ElapsedMilliseconds)
    {
      this.MainMap.ElapsedMilliseconds = ElapsedMilliseconds;
      MethodInvoker methodInvoker = (MethodInvoker) (() =>
      {
        this.panelMenu.Text = "Menu, last load in " + (object) this.MainMap.ElapsedMilliseconds + "ms";
        this.textBoxMemory.Text = string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0:0.00}MB of {1:0.00}MB", new object[2]
        {
          (object) this.MainMap.Manager.MemoryCacheSize,
          (object) this.MainMap.Manager.MemoryCacheCapacity
        });
      });
      try
      {
        this.BeginInvoke((Delegate) methodInvoker);
      }
      catch
      {
      }
    }

    private void MainMap_OnPositionChanged(PointLatLng point)
    {
      this.center.Position = point;
      this.textBoxLatCurrent.Text = point.Lat.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      this.textBoxLngCurrent.Text = point.Lng.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      lock (this.flights)
      {
        this.lastPosition = point;
        this.lastZoom = checked ((int) this.MainMap.Zoom);
      }
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
      if (this.objects.Markers.Count <= 0)
        return;
      this.MainMap.ZoomAndCenterMarkers((string) null);
      this.trackBar1.Value = checked ((int) this.MainMap.Zoom);
    }

    private void MainMap_MouseEnter(object sender, EventArgs e)
    {
      this.MainMap.Focus();
    }

    private void xPanderPanel1_Click(object sender, EventArgs e)
    {
      this.xPanderPanelList1.Expand((BasePanel) this.xPanderPanelMain);
    }

    private void xPanderPanelCache_Click(object sender, EventArgs e)
    {
      this.xPanderPanelList1.Expand((BasePanel) this.xPanderPanelCache);
    }

    private void xPanderPanelLive_Click(object sender, EventArgs e)
    {
      this.xPanderPanelList1.Expand((BasePanel) this.xPanderPanelLive);
    }

    private void xPanderPanelInfo_Click(object sender, EventArgs e)
    {
      this.xPanderPanelList1.Expand((BasePanel) this.xPanderPanelInfo);
    }

    private void comboBoxMapType_DropDownClosed(object sender, EventArgs e)
    {
      if (!this.UserAcceptedLicenseOnce)
      {
        if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + (object) Path.DirectorySeparatorChar + "License.txt"))
        {
          string str1 = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + (object) Path.DirectorySeparatorChar + "License.txt");
          int startIndex = str1.IndexOf("License");
          string str2 = str1.Substring(startIndex);
          Demo.WindowsForms.Forms.Message message = new Demo.WindowsForms.Forms.Message();
          message.richTextBox1.Text = str2;
          if (DialogResult.Yes == message.ShowDialog())
          {
            this.UserAcceptedLicenseOnce = true;
            MainForm mainForm = this;
            string str3 = mainForm.Text + (object) " - license accepted by " + Environment.UserName + " at " + (string) (object) DateTime.Now;
            mainForm.Text = str3;
          }
        }
        else
          this.UserAcceptedLicenseOnce = true;
      }
      if (this.UserAcceptedLicenseOnce)
      {
        this.MainMap.MapProvider = this.comboBoxMapType.SelectedItem as GMapProvider;
      }
      else
      {
        this.MainMap.MapProvider = (GMapProvider) GMapProviders.EmptyProvider;
        this.comboBoxMapType.SelectedItem = (object) this.MainMap.MapProvider;
      }
    }

    private void comboBoxMode_DropDownClosed(object sender, EventArgs e)
    {
      Singleton<GMaps>.Instance.Mode = (AccessMode) this.comboBoxMode.SelectedValue;
      this.MainMap.ReloadMap();
    }

    private void trackBar1_ValueChanged(object sender, EventArgs e)
    {
      this.MainMap.Zoom = (double) this.trackBar1.Value;
    }

    private void button8_Click(object sender, EventArgs e)
    {
      this.MainMap.Position = new PointLatLng(double.Parse(this.textBoxLat.Text, (IFormatProvider) CultureInfo.InvariantCulture), double.Parse(this.textBoxLng.Text, (IFormatProvider) CultureInfo.InvariantCulture));
    }

    private void textBoxGeo_KeyPress(object sender, KeyPressEventArgs e)
    {
      if ((int) e.KeyChar != 13)
        return;
      GeoCoderStatusCode geoCoderStatusCode = this.MainMap.SetCurrentPositionByKeywords(this.textBoxGeo.Text);
      if (geoCoderStatusCode == GeoCoderStatusCode.G_GEO_SUCCESS)
        return;
      int num = (int) MessageBox.Show("Google Maps Geocoder can't find: '" + this.textBoxGeo.Text + "', reason: " + geoCoderStatusCode.ToString(), "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.MainMap.ReloadMap();
    }

    private void checkBoxUseCache_CheckedChanged(object sender, EventArgs e)
    {
      Singleton<GMaps>.Instance.UseRouteCache = this.checkBoxUseRouteCache.Checked;
      Singleton<GMaps>.Instance.UseGeocoderCache = this.checkBoxUseGeoCache.Checked;
      Singleton<GMaps>.Instance.UsePlacemarkCache = Singleton<GMaps>.Instance.UseGeocoderCache;
    }

    private void button2_Click(object sender, EventArgs e)
    {
      if (MessageBox.Show("Are You sure?", "Clear GMap.NET cache?", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) != DialogResult.OK)
        return;
      try
      {
        Directory.Delete(this.MainMap.CacheLocation, true);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message);
      }
    }

    private void button3_Click(object sender, EventArgs e)
    {
      MapRoute routeBetweenPoints = (this.MainMap.MapProvider as RoutingProvider ?? (RoutingProvider) GMapProviders.GoogleMap).GetRouteBetweenPoints(this.start, this.end, false, false, checked ((int) this.MainMap.Zoom));
      if (routeBetweenPoints == null)
        return;
      GMapRoute gmapRoute = new GMapRoute(routeBetweenPoints.Points, routeBetweenPoints.Name);
      this.routes.Routes.Add(gmapRoute);
      GMapMarker gmapMarker1 = (GMapMarker) new GMapMarkerGoogleRed(this.start);
      gmapMarker1.ToolTipText = "Start: " + routeBetweenPoints.Name;
      gmapMarker1.ToolTipMode = MarkerTooltipMode.Always;
      GMapMarker gmapMarker2 = (GMapMarker) new GMapMarkerGoogleGreen(this.end);
      gmapMarker2.ToolTipText = "End: " + this.end.ToString();
      gmapMarker2.ToolTipMode = MarkerTooltipMode.Always;
      this.objects.Markers.Add(gmapMarker1);
      this.objects.Markers.Add(gmapMarker2);
      this.MainMap.ZoomAndCenterRoute((MapRoute) gmapRoute);
    }

    private void button4_Click(object sender, EventArgs e)
    {
      GMapMarkerGoogleGreen markerGoogleGreen = new GMapMarkerGoogleGreen(this.currentMarker.Position);
      GMapMarkerRect gmapMarkerRect = new GMapMarkerRect(this.currentMarker.Position);
      gmapMarkerRect.InnerMarker = markerGoogleGreen;
      if (this.polygon != null)
        gmapMarkerRect.Tag = (object) this.polygon.Points.Count;
      gmapMarkerRect.ToolTipMode = MarkerTooltipMode.Always;
      Placemark placemark1 = (Placemark) null;
      if (this.checkBoxPlacemarkInfo.Checked)
      {
        GeoCoderStatusCode status;
        Placemark placemark2 = GMapProviders.GoogleMap.GetPlacemark(this.currentMarker.Position, out status);
        if (status == GeoCoderStatusCode.G_GEO_SUCCESS && placemark2 != null)
          placemark1 = placemark2;
      }
      if (placemark1 != null)
        gmapMarkerRect.ToolTipText = placemark1.Address;
      else
        gmapMarkerRect.ToolTipText = this.currentMarker.Position.ToString();
      this.objects.Markers.Add((GMapMarker) markerGoogleGreen);
      this.objects.Markers.Add((GMapMarker) gmapMarkerRect);
      this.RegeneratePolygon();
    }

    private void button6_Click(object sender, EventArgs e)
    {
      this.routes.Routes.Clear();
    }

    private void button15_Click(object sender, EventArgs e)
    {
      this.polygons.Polygons.Clear();
    }

    private void button5_Click(object sender, EventArgs e)
    {
      this.objects.Markers.Clear();
    }

    private void checkBoxCurrentMarker_CheckedChanged(object sender, EventArgs e)
    {
      this.currentMarker.IsVisible = this.checkBoxCurrentMarker.Checked;
    }

    private void checkBoxCanDrag_CheckedChanged(object sender, EventArgs e)
    {
      this.MainMap.CanDragMap = this.checkBoxCanDrag.Checked;
    }

    private void buttonSetStart_Click(object sender, EventArgs e)
    {
      this.start = this.currentMarker.Position;
    }

    private void buttonSetEnd_Click(object sender, EventArgs e)
    {
      this.end = this.currentMarker.Position;
    }

    private void button7_Click(object sender, EventArgs e)
    {
      this.MainMap.ZoomAndCenterMarkers("objects");
    }

    private void button9_Click(object sender, EventArgs e)
    {
      this.MainMap.ShowExportDialog();
    }

    private void button10_Click(object sender, EventArgs e)
    {
      this.MainMap.ShowImportDialog();
    }

    private void button11_Click(object sender, EventArgs e)
    {
      RectLatLng selectedArea = this.MainMap.SelectedArea;
      if (!selectedArea.IsEmpty)
      {
        int zoom = checked ((int) this.MainMap.Zoom);
        while (zoom <= this.MainMap.MaxZoom)
        {
          switch (MessageBox.Show("Ready ripp at Zoom = " + (object) zoom + " ?", "GMap.NET", MessageBoxButtons.YesNoCancel))
          {
            case DialogResult.Yes:
              TilePrefetcher tilePrefetcher = new TilePrefetcher();
              tilePrefetcher.Owner = (Form) this;
              tilePrefetcher.ShowCompleteMessage = true;
              tilePrefetcher.Start(selectedArea, zoom, this.MainMap.MapProvider, 100);
              break;
            case DialogResult.Cancel:
              return;
          }
          checked { ++zoom; }
        }
      }
      else
      {
        int num = (int) MessageBox.Show("Select map area holding ALT", "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
    }

    private void button12_Click(object sender, EventArgs e)
    {
      try
      {
        using (SaveFileDialog saveFileDialog = new SaveFileDialog())
        {
          saveFileDialog.Filter = "PNG (*.png)|*.png";
          saveFileDialog.FileName = "GMap.NET image";
          Image image = this.MainMap.ToImage();
          if (image == null)
            return;
          using (image)
          {
            if (saveFileDialog.ShowDialog() != DialogResult.OK)
              return;
            image.Save(saveFileDialog.FileName);
            int num = (int) MessageBox.Show("Image saved: " + saveFileDialog.FileName, "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
          }
        }
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Image failed to save: " + ex.Message, "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void checkBoxDebug_CheckedChanged(object sender, EventArgs e)
    {
      this.MainMap.ShowTileGridLines = this.checkBoxDebug.Checked;
    }

    private void button13_Click(object sender, EventArgs e)
    {
      StaticImage staticImage = new StaticImage(this);
      staticImage.Owner = (Form) this;
      staticImage.Show();
    }

    private void button14_Click(object sender, EventArgs e)
    {
      using (FileDialog fileDialog = (FileDialog) new OpenFileDialog())
      {
        fileDialog.CheckPathExists = true;
        fileDialog.CheckFileExists = false;
        fileDialog.AddExtension = true;
        fileDialog.DefaultExt = "gpsd";
        fileDialog.ValidateNames = true;
        fileDialog.Title = "GMap.NET: open gps log generated in your windows mobile";
        fileDialog.Filter = "GMap.NET gps log DB files (*.gpsd)|*.gpsd";
        fileDialog.FilterIndex = 1;
        fileDialog.RestoreDirectory = true;
        if (fileDialog.ShowDialog() != DialogResult.OK)
          return;
        this.routes.Routes.Clear();
        this.mobileGpsLog = fileDialog.FileName;
        this.AddGpsMobileLogRoutes(fileDialog.FileName);
        if (this.routes.Routes.Count <= 0)
          return;
        this.MainMap.ZoomAndCenterRoutes((string) null);
      }
    }

    private void MainForm_KeyUp(object sender, KeyEventArgs e)
    {
      int num = -22;
      if (e.KeyCode == Keys.Left)
        this.MainMap.Offset(checked (-num), 0);
      else if (e.KeyCode == Keys.Right)
        this.MainMap.Offset(num, 0);
      else if (e.KeyCode == Keys.Up)
        this.MainMap.Offset(0, checked (-num));
      else if (e.KeyCode == Keys.Down)
        this.MainMap.Offset(0, num);
      else if (e.KeyCode == Keys.Delete)
      {
        if (this.CurentRectMarker == null)
          return;
        this.objects.Markers.Remove((GMapMarker) this.CurentRectMarker);
        if (this.CurentRectMarker.InnerMarker != null)
          this.objects.Markers.Remove((GMapMarker) this.CurentRectMarker.InnerMarker);
        this.CurentRectMarker = (GMapMarkerRect) null;
        this.RegeneratePolygon();
      }
      else
      {
        if (e.KeyCode != Keys.Escape)
          return;
        this.MainMap.Bearing = 0.0f;
        if (this.currentTransport == null || this.MainMap.IsMouseOverMarker)
          return;
        this.currentTransport.ToolTipMode = MarkerTooltipMode.OnMouseOver;
        this.currentTransport = (GMapMarker) null;
      }
    }

    private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (!this.MainMap.Focused)
        return;
      if ((int) e.KeyChar == 43)
        ++this.MainMap.Zoom;
      else if ((int) e.KeyChar == 45)
        --this.MainMap.Zoom;
      else if ((int) e.KeyChar == 97)
      {
        --this.MainMap.Bearing;
      }
      else
      {
        if ((int) e.KeyChar != 122)
          return;
        ++this.MainMap.Bearing;
      }
    }

    private void RealTimeChanged(object sender, EventArgs e)
    {
      this.objects.Markers.Clear();
      this.polygons.Polygons.Clear();
      this.routes.Routes.Clear();
      if (this.radioButtonPerf.Checked)
      {
        this.timerPerf.Interval = 44;
        this.timerPerf.Start();
      }
      else
        this.timerPerf.Stop();
      if (this.radioButtonFlight.Checked)
      {
        if (!this.flightWorker.IsBusy)
        {
          this.firstLoadFlight = true;
          this.flightWorker.RunWorkerAsync();
        }
      }
      else if (this.flightWorker.IsBusy)
        this.flightWorker.CancelAsync();
      if (this.radioButtonVehicle.Checked)
      {
        if (!this.transportWorker.IsBusy)
        {
          this.firstLoadTrasport = true;
          this.transportWorker.RunWorkerAsync();
        }
      }
      else if (this.transportWorker.IsBusy)
        this.transportWorker.CancelAsync();
      if (this.radioButtonTcpIp.Checked)
      {
        this.GridConnections.Visible = true;
        this.checkBoxTcpIpSnap.Visible = true;
        this.checkBoxTraceRoute.Visible = true;
        this.GridConnections.Refresh();
        if (this.connectionsWorker.IsBusy)
          return;
        this.MainMap.Zoom = 5.0;
        this.connectionsWorker.RunWorkerAsync();
      }
      else
      {
        this.CountryStatusView.Clear();
        this.GridConnections.Visible = false;
        this.checkBoxTcpIpSnap.Visible = false;
        this.checkBoxTraceRoute.Visible = false;
        if (!this.connectionsWorker.IsBusy)
          return;
        this.connectionsWorker.CancelAsync();
      }
    }

    private void buttonExportToGpx_Click(object sender, EventArgs e)
    {
      try
      {
        using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
        {
          saveFileDialog1.Filter = "GPX (*.gpx)|*.gpx";
          saveFileDialog1.FileName = "mobile gps log";
          DateTime? start = new DateTime?();
          DateTime? end = new DateTime?();
          if (this.MobileLogFrom.Checked)
          {
            start = new DateTime?(this.MobileLogFrom.Value.ToUniversalTime());
            SaveFileDialog saveFileDialog2 = saveFileDialog1;
            string str = saveFileDialog2.FileName + " from " + this.MobileLogFrom.Value.ToString("yyyy-MM-dd HH-mm");
            saveFileDialog2.FileName = str;
          }
          if (this.MobileLogTo.Checked)
          {
            end = new DateTime?(this.MobileLogTo.Value.ToUniversalTime());
            SaveFileDialog saveFileDialog2 = saveFileDialog1;
            string str = saveFileDialog2.FileName + " to " + this.MobileLogTo.Value.ToString("yyyy-MM-dd HH-mm");
            saveFileDialog2.FileName = str;
          }
          if (saveFileDialog1.ShowDialog() != DialogResult.OK)
            return;
          IEnumerable<List<GpsLog>> routesFromMobileLog = Stuff.GetRoutesFromMobileLog(this.mobileGpsLog, start, end, new double?(3.3));
          if (routesFromMobileLog == null || !this.MainMap.Manager.ExportGPX(routesFromMobileLog, saveFileDialog1.FileName))
            return;
          int num = (int) MessageBox.Show("GPX saved: " + saveFileDialog1.FileName, "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("GPX failed to save: " + ex.Message, "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void button16_Click(object sender, EventArgs e)
    {
      using (FileDialog fileDialog = (FileDialog) new OpenFileDialog())
      {
        fileDialog.CheckPathExists = true;
        fileDialog.CheckFileExists = false;
        fileDialog.AddExtension = true;
        fileDialog.DefaultExt = "gpx";
        fileDialog.ValidateNames = true;
        fileDialog.Title = "GMap.NET: open gpx log";
        fileDialog.Filter = "gpx files (*.gpx)|*.gpx";
        fileDialog.FilterIndex = 1;
        fileDialog.RestoreDirectory = true;
        if (fileDialog.ShowDialog() != DialogResult.OK)
          return;
        try
        {
          gpxType gpxType = Singleton<GMaps>.Instance.DeserializeGPX(System.IO.File.ReadAllText(fileDialog.FileName));
          if (gpxType == null || gpxType.trk.Length <= 0)
            return;
          foreach (trkType trkType in gpxType.trk)
          {
            List<PointLatLng> points = new List<PointLatLng>();
            foreach (trksegType trksegType in trkType.trkseg)
            {
              foreach (wptType wptType in trksegType.trkpt)
                points.Add(new PointLatLng((double) wptType.lat, (double) wptType.lon));
            }
            GMapRoute gmapRoute = new GMapRoute(points, string.Empty);
            gmapRoute.Stroke = new Pen(Color.FromArgb(144, Color.Red));
            gmapRoute.Stroke.Width = 5f;
            gmapRoute.Stroke.DashStyle = DashStyle.DashDot;
            this.routes.Routes.Add(gmapRoute);
          }
          this.MainMap.ZoomAndCenterRoutes((string) null);
        }
        catch (Exception ex)
        {
          int num = (int) MessageBox.Show("Error importing gpx: " + ex.Message, "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
      }
    }

    private void checkBoxTraceRoute_CheckedChanged(object sender, EventArgs e)
    {
      this.TryTraceConnection = this.checkBoxTraceRoute.Checked;
      if (this.TryTraceConnection)
        return;
      if (this.iptracerWorker.IsBusy)
        this.iptracerWorker.CancelAsync();
      this.routes.Routes.Clear();
    }

    private void GridConnections_DoubleClick(object sender, EventArgs e)
    {
      this.GridConnections.ClearSelection();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      DataGridViewCellStyle gridViewCellStyle = new DataGridViewCellStyle();
      this.label7 = new Label();
      this.comboBoxMapType = new ComboBox();
      this.groupBox3 = new GroupBox();
      this.label6 = new Label();
      this.textBoxGeo = new TextBox();
      this.button1 = new Button();
      this.button8 = new Button();
      this.label2 = new Label();
      this.label1 = new Label();
      this.textBoxLng = new TextBox();
      this.textBoxLat = new TextBox();
      this.button5 = new Button();
      this.button4 = new Button();
      this.trackBar1 = new TrackBar();
      this.groupBox5 = new GroupBox();
      this.button16 = new Button();
      this.button13 = new Button();
      this.checkBoxDebug = new CheckBox();
      this.button12 = new Button();
      this.label8 = new Label();
      this.comboBoxMode = new ComboBox();
      this.checkBoxCanDrag = new CheckBox();
      this.checkBoxCurrentMarker = new CheckBox();
      this.button11 = new Button();
      this.button10 = new Button();
      this.button9 = new Button();
      this.checkBoxUseGeoCache = new CheckBox();
      this.checkBoxUseRouteCache = new CheckBox();
      this.button3 = new Button();
      this.groupBox7 = new GroupBox();
      this.buttonExportToGpx = new Button();
      this.MobileLogTo = new DateTimePicker();
      this.label3 = new Label();
      this.button15 = new Button();
      this.MobileLogFrom = new DateTimePicker();
      this.button14 = new Button();
      this.buttonSetEnd = new Button();
      this.buttonSetStart = new Button();
      this.button6 = new Button();
      this.groupBox8 = new GroupBox();
      this.button7 = new Button();
      this.checkBoxPlacemarkInfo = new CheckBox();
      this.panelMenu = new BSE.Windows.Forms.Panel();
      this.splitter1 = new BSE.Windows.Forms.Splitter();
      this.xPanderPanelList1 = new XPanderPanelList();
      this.xPanderPanelMain = new XPanderPanel();
      this.tableLayoutPanel4 = new TableLayoutPanel();
      this.tableLayoutPanel5 = new TableLayoutPanel();
      this.xPanderPanelCache = new XPanderPanel();
      this.tableLayoutPanel1 = new TableLayoutPanel();
      this.textBoxMemory = new TextBox();
      this.label10 = new Label();
      this.button2 = new Button();
      this.xPanderPanelInfo = new XPanderPanel();
      this.tableLayoutPanel2 = new TableLayoutPanel();
      this.textBoxZoomCurrent = new TextBox();
      this.textBoxrouteCount = new TextBox();
      this.label12 = new Label();
      this.label9 = new Label();
      this.textBoxLngCurrent = new TextBox();
      this.textBoxMarkerCount = new TextBox();
      this.label11 = new Label();
      this.label4 = new Label();
      this.textBoxLatCurrent = new TextBox();
      this.label5 = new Label();
      this.xPanderPanelLive = new XPanderPanel();
      this.tableLayoutPanel3 = new TableLayoutPanel();
      this.radioButtonNone = new RadioButton();
      this.checkBoxTcpIpSnap = new CheckBox();
      this.GridConnections = new DataGridView();
      this.CountryName = new DataGridViewTextBoxColumn();
      this.ConnectionsCount = new DataGridViewTextBoxColumn();
      this.radioButtonFlight = new RadioButton();
      this.radioButtonPerf = new RadioButton();
      this.radioButtonTcpIp = new RadioButton();
      this.checkBoxTraceRoute = new CheckBox();
      this.radioButtonVehicle = new RadioButton();
      this.panel2 = new System.Windows.Forms.Panel();
      this.MainMap = new Map();
      this.panel4 = new System.Windows.Forms.Panel();
      this.groupBox3.SuspendLayout();
      this.trackBar1.BeginInit();
      this.groupBox5.SuspendLayout();
      this.groupBox7.SuspendLayout();
      this.groupBox8.SuspendLayout();
      this.panelMenu.SuspendLayout();
      this.xPanderPanelList1.SuspendLayout();
      this.xPanderPanelMain.SuspendLayout();
      this.tableLayoutPanel4.SuspendLayout();
      this.tableLayoutPanel5.SuspendLayout();
      this.xPanderPanelCache.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.xPanderPanelInfo.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.xPanderPanelLive.SuspendLayout();
      this.tableLayoutPanel3.SuspendLayout();
      ((ISupportInitialize) this.GridConnections).BeginInit();
      this.panel2.SuspendLayout();
      this.panel4.SuspendLayout();
      this.SuspendLayout();
      this.label7.AutoSize = true;
      this.label7.Location = new Point(176, 27);
      this.label7.Margin = new Padding(4, 0, 4, 0);
      this.label7.Name = "label7";
      this.label7.Size = new Size(35, 17);
      this.label7.TabIndex = 31;
      this.label7.Text = "type";
      this.comboBoxMapType.DropDownStyle = ComboBoxStyle.DropDownList;
      this.comboBoxMapType.FormattingEnabled = true;
      this.comboBoxMapType.Location = new Point(11, 23);
      this.comboBoxMapType.Margin = new Padding(4);
      this.comboBoxMapType.Name = "comboBoxMapType";
      this.comboBoxMapType.Size = new Size(163, 24);
      this.comboBoxMapType.TabIndex = 9;
      this.comboBoxMapType.DropDownClosed += new EventHandler(this.comboBoxMapType_DropDownClosed);
      this.groupBox3.Controls.Add((Control) this.label6);
      this.groupBox3.Controls.Add((Control) this.textBoxGeo);
      this.groupBox3.Controls.Add((Control) this.button1);
      this.groupBox3.Controls.Add((Control) this.button8);
      this.groupBox3.Controls.Add((Control) this.label2);
      this.groupBox3.Controls.Add((Control) this.label1);
      this.groupBox3.Controls.Add((Control) this.textBoxLng);
      this.groupBox3.Controls.Add((Control) this.textBoxLat);
      this.groupBox3.Dock = DockStyle.Fill;
      this.groupBox3.Location = new Point(4, 4);
      this.groupBox3.Margin = new Padding(4);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Padding = new Padding(4);
      this.groupBox3.Size = new Size(224, 158);
      this.groupBox3.TabIndex = 28;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "coordinates";
      this.label6.AutoSize = true;
      this.label6.Location = new Point(176, 91);
      this.label6.Margin = new Padding(4, 0, 4, 0);
      this.label6.Name = "label6";
      this.label6.Size = new Size(36, 17);
      this.label6.TabIndex = 11;
      this.label6.Text = "goto";
      this.textBoxGeo.Location = new Point(12, 87);
      this.textBoxGeo.Margin = new Padding(4);
      this.textBoxGeo.Name = "textBoxGeo";
      this.textBoxGeo.Size = new Size(161, 22);
      this.textBoxGeo.TabIndex = 10;
      this.textBoxGeo.Text = "lietuva vilnius";
      this.textBoxGeo.KeyPress += new KeyPressEventHandler(this.textBoxGeo_KeyPress);
      this.button1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.button1.Location = new Point(113, 121);
      this.button1.Margin = new Padding(4);
      this.button1.Name = "button1";
      this.button1.Size = new Size(99, 30);
      this.button1.TabIndex = 9;
      this.button1.Text = "Reload";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button8.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.button8.Location = new Point(12, 121);
      this.button8.Margin = new Padding(4);
      this.button8.Name = "button8";
      this.button8.Size = new Size(96, 30);
      this.button8.TabIndex = 8;
      this.button8.Text = "GoTo !";
      this.button8.UseVisualStyleBackColor = true;
      this.button8.Click += new EventHandler(this.button8_Click);
      this.label2.AutoSize = true;
      this.label2.Location = new Point(176, 59);
      this.label2.Margin = new Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new Size(27, 17);
      this.label2.TabIndex = 3;
      this.label2.Text = "lng";
      this.label1.AutoSize = true;
      this.label1.Location = new Point(176, 27);
      this.label1.Margin = new Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new Size(23, 17);
      this.label1.TabIndex = 2;
      this.label1.Text = "lat";
      this.textBoxLng.Location = new Point(12, 55);
      this.textBoxLng.Margin = new Padding(4);
      this.textBoxLng.Name = "textBoxLng";
      this.textBoxLng.Size = new Size(161, 22);
      this.textBoxLng.TabIndex = 1;
      this.textBoxLng.Text = "25.2985095977783";
      this.textBoxLat.Location = new Point(12, 23);
      this.textBoxLat.Margin = new Padding(4);
      this.textBoxLat.Name = "textBoxLat";
      this.textBoxLat.Size = new Size(161, 22);
      this.textBoxLat.TabIndex = 0;
      this.textBoxLat.Text = "54.6961334816182";
      this.button5.Location = new Point(125, 55);
      this.button5.Margin = new Padding(4);
      this.button5.Name = "button5";
      this.button5.Size = new Size(84, 30);
      this.button5.TabIndex = 13;
      this.button5.Text = "Clear All";
      this.button5.UseVisualStyleBackColor = true;
      this.button5.Click += new EventHandler(this.button5_Click);
      this.button4.Location = new Point(8, 18);
      this.button4.Margin = new Padding(4);
      this.button4.Name = "button4";
      this.button4.Size = new Size(109, 30);
      this.button4.TabIndex = 12;
      this.button4.Text = "Add Marker";
      this.button4.UseVisualStyleBackColor = true;
      this.button4.Click += new EventHandler(this.button4_Click);
      this.trackBar1.BackColor = Color.AliceBlue;
      this.trackBar1.Dock = DockStyle.Left;
      this.trackBar1.LargeChange = 1;
      this.trackBar1.Location = new Point(4, 4);
      this.trackBar1.Margin = new Padding(4);
      this.trackBar1.Maximum = 17;
      this.trackBar1.Minimum = 1;
      this.trackBar1.Name = "trackBar1";
      this.trackBar1.Orientation = Orientation.Vertical;
      this.trackBar1.Size = new Size(56, 681);
      this.trackBar1.TabIndex = 29;
      this.trackBar1.TickStyle = TickStyle.TopLeft;
      this.trackBar1.Value = 12;
      this.trackBar1.ValueChanged += new EventHandler(this.trackBar1_ValueChanged);
      this.groupBox5.Controls.Add((Control) this.button16);
      this.groupBox5.Controls.Add((Control) this.button13);
      this.groupBox5.Controls.Add((Control) this.checkBoxDebug);
      this.groupBox5.Controls.Add((Control) this.button12);
      this.groupBox5.Controls.Add((Control) this.label8);
      this.groupBox5.Controls.Add((Control) this.comboBoxMode);
      this.groupBox5.Controls.Add((Control) this.checkBoxCanDrag);
      this.groupBox5.Controls.Add((Control) this.checkBoxCurrentMarker);
      this.groupBox5.Controls.Add((Control) this.label7);
      this.groupBox5.Controls.Add((Control) this.comboBoxMapType);
      this.groupBox5.Dock = DockStyle.Fill;
      this.groupBox5.Location = new Point(4, 170);
      this.groupBox5.Margin = new Padding(4);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Padding = new Padding(4);
      this.groupBox5.Size = new Size(224, 180);
      this.groupBox5.TabIndex = 31;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "gmap";
      this.button16.Location = new Point(123, 113);
      this.button16.Margin = new Padding(4);
      this.button16.Name = "button16";
      this.button16.Size = new Size(92, 30);
      this.button16.TabIndex = 49;
      this.button16.Text = "GPX...";
      this.button16.UseVisualStyleBackColor = true;
      this.button16.Click += new EventHandler(this.button16_Click);
      this.button13.Location = new Point(12, 144);
      this.button13.Margin = new Padding(3, 2, 3, 2);
      this.button13.Name = "button13";
      this.button13.Size = new Size(97, 30);
      this.button13.TabIndex = 41;
      this.button13.Text = "Get Static";
      this.button13.UseVisualStyleBackColor = true;
      this.button13.Click += new EventHandler(this.button13_Click);
      this.checkBoxDebug.AutoSize = true;
      this.checkBoxDebug.Location = new Point(139, 91);
      this.checkBoxDebug.Margin = new Padding(3, 2, 3, 2);
      this.checkBoxDebug.Name = "checkBoxDebug";
      this.checkBoxDebug.Size = new Size(57, 21);
      this.checkBoxDebug.TabIndex = 40;
      this.checkBoxDebug.Text = "Grid";
      this.checkBoxDebug.UseVisualStyleBackColor = true;
      this.checkBoxDebug.CheckedChanged += new EventHandler(this.checkBoxDebug_CheckedChanged);
      this.button12.Location = new Point(123, 144);
      this.button12.Margin = new Padding(3, 2, 3, 2);
      this.button12.Name = "button12";
      this.button12.Size = new Size(92, 30);
      this.button12.TabIndex = 39;
      this.button12.Text = "Save View";
      this.button12.UseVisualStyleBackColor = true;
      this.button12.Click += new EventHandler(this.button12_Click);
      this.label8.AutoSize = true;
      this.label8.Location = new Point(176, 60);
      this.label8.Margin = new Padding(4, 0, 4, 0);
      this.label8.Name = "label8";
      this.label8.Size = new Size(43, 17);
      this.label8.TabIndex = 38;
      this.label8.Text = "mode";
      this.comboBoxMode.DropDownStyle = ComboBoxStyle.DropDownList;
      this.comboBoxMode.FormattingEnabled = true;
      this.comboBoxMode.Location = new Point(11, 57);
      this.comboBoxMode.Margin = new Padding(4);
      this.comboBoxMode.Name = "comboBoxMode";
      this.comboBoxMode.Size = new Size(163, 24);
      this.comboBoxMode.TabIndex = 37;
      this.comboBoxMode.DropDownClosed += new EventHandler(this.comboBoxMode_DropDownClosed);
      this.checkBoxCanDrag.AutoSize = true;
      this.checkBoxCanDrag.Checked = true;
      this.checkBoxCanDrag.CheckState = CheckState.Checked;
      this.checkBoxCanDrag.Location = new Point(12, 117);
      this.checkBoxCanDrag.Margin = new Padding(4);
      this.checkBoxCanDrag.Name = "checkBoxCanDrag";
      this.checkBoxCanDrag.Size = new Size(92, 21);
      this.checkBoxCanDrag.TabIndex = 36;
      this.checkBoxCanDrag.Text = "Drag Map";
      this.checkBoxCanDrag.UseVisualStyleBackColor = true;
      this.checkBoxCanDrag.CheckedChanged += new EventHandler(this.checkBoxCanDrag_CheckedChanged);
      this.checkBoxCurrentMarker.AutoSize = true;
      this.checkBoxCurrentMarker.Checked = true;
      this.checkBoxCurrentMarker.CheckState = CheckState.Checked;
      this.checkBoxCurrentMarker.Location = new Point(12, 90);
      this.checkBoxCurrentMarker.Margin = new Padding(4);
      this.checkBoxCurrentMarker.Name = "checkBoxCurrentMarker";
      this.checkBoxCurrentMarker.Size = new Size(125, 21);
      this.checkBoxCurrentMarker.TabIndex = 35;
      this.checkBoxCurrentMarker.Text = "Current Marker";
      this.checkBoxCurrentMarker.UseVisualStyleBackColor = true;
      this.checkBoxCurrentMarker.CheckedChanged += new EventHandler(this.checkBoxCurrentMarker_CheckedChanged);
      this.button11.Dock = DockStyle.Top;
      this.button11.Location = new Point(24, 89);
      this.button11.Margin = new Padding(4);
      this.button11.Name = "button11";
      this.button11.Size = new Size(254, 25);
      this.button11.TabIndex = 38;
      this.button11.Text = "Prefetch";
      this.button11.UseVisualStyleBackColor = true;
      this.button11.Click += new EventHandler(this.button11_Click);
      this.button10.Dock = DockStyle.Top;
      this.button10.Location = new Point(24, 24);
      this.button10.Margin = new Padding(4);
      this.button10.Name = "button10";
      this.button10.Size = new Size(254, 25);
      this.button10.TabIndex = 5;
      this.button10.Text = "Import";
      this.button10.UseVisualStyleBackColor = true;
      this.button10.Click += new EventHandler(this.button10_Click);
      this.button9.Dock = DockStyle.Top;
      this.button9.Location = new Point(24, 57);
      this.button9.Margin = new Padding(4);
      this.button9.Name = "button9";
      this.button9.Size = new Size(254, 24);
      this.button9.TabIndex = 4;
      this.button9.Text = "Export";
      this.button9.UseVisualStyleBackColor = true;
      this.button9.Click += new EventHandler(this.button9_Click);
      this.checkBoxUseGeoCache.AutoSize = true;
      this.checkBoxUseGeoCache.Checked = true;
      this.checkBoxUseGeoCache.CheckState = CheckState.Checked;
      this.checkBoxUseGeoCache.Location = new Point(24, 283);
      this.checkBoxUseGeoCache.Margin = new Padding(4);
      this.checkBoxUseGeoCache.Name = "checkBoxUseGeoCache";
      this.checkBoxUseGeoCache.Size = new Size(96, 21);
      this.checkBoxUseGeoCache.TabIndex = 3;
      this.checkBoxUseGeoCache.Text = "geocoding";
      this.checkBoxUseGeoCache.UseVisualStyleBackColor = true;
      this.checkBoxUseGeoCache.CheckedChanged += new EventHandler(this.checkBoxUseCache_CheckedChanged);
      this.checkBoxUseRouteCache.AutoSize = true;
      this.checkBoxUseRouteCache.Checked = true;
      this.checkBoxUseRouteCache.CheckState = CheckState.Checked;
      this.checkBoxUseRouteCache.Location = new Point(24, 254);
      this.checkBoxUseRouteCache.Margin = new Padding(4);
      this.checkBoxUseRouteCache.Name = "checkBoxUseRouteCache";
      this.checkBoxUseRouteCache.Size = new Size(74, 21);
      this.checkBoxUseRouteCache.TabIndex = 2;
      this.checkBoxUseRouteCache.Text = "routing";
      this.checkBoxUseRouteCache.UseVisualStyleBackColor = true;
      this.checkBoxUseRouteCache.CheckedChanged += new EventHandler(this.checkBoxUseCache_CheckedChanged);
      this.button3.Location = new Point(12, 60);
      this.button3.Margin = new Padding(4);
      this.button3.Name = "button3";
      this.button3.Size = new Size(97, 30);
      this.button3.TabIndex = 33;
      this.button3.Text = "Add Route";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new EventHandler(this.button3_Click);
      this.groupBox7.Controls.Add((Control) this.buttonExportToGpx);
      this.groupBox7.Controls.Add((Control) this.MobileLogTo);
      this.groupBox7.Controls.Add((Control) this.label3);
      this.groupBox7.Controls.Add((Control) this.button15);
      this.groupBox7.Controls.Add((Control) this.MobileLogFrom);
      this.groupBox7.Controls.Add((Control) this.button14);
      this.groupBox7.Controls.Add((Control) this.buttonSetEnd);
      this.groupBox7.Controls.Add((Control) this.buttonSetStart);
      this.groupBox7.Controls.Add((Control) this.button6);
      this.groupBox7.Controls.Add((Control) this.button3);
      this.groupBox7.Dock = DockStyle.Fill;
      this.groupBox7.Location = new Point(4, 358);
      this.groupBox7.Margin = new Padding(4);
      this.groupBox7.Name = "groupBox7";
      this.groupBox7.Padding = new Padding(4);
      this.groupBox7.Size = new Size(224, 191);
      this.groupBox7.TabIndex = 35;
      this.groupBox7.TabStop = false;
      this.groupBox7.Text = "routing";
      this.buttonExportToGpx.Location = new Point(165, 96);
      this.buttonExportToGpx.Margin = new Padding(3, 2, 3, 2);
      this.buttonExportToGpx.Name = "buttonExportToGpx";
      this.buttonExportToGpx.Size = new Size(49, 46);
      this.buttonExportToGpx.TabIndex = 48;
      this.buttonExportToGpx.Text = "to GPX";
      this.buttonExportToGpx.UseVisualStyleBackColor = true;
      this.buttonExportToGpx.Click += new EventHandler(this.buttonExportToGpx_Click);
      this.MobileLogTo.CustomFormat = "yyyy'.'MM'.'dd HH':'mm";
      this.MobileLogTo.Format = DateTimePickerFormat.Custom;
      this.MobileLogTo.Location = new Point(12, 122);
      this.MobileLogTo.Margin = new Padding(3, 2, 3, 2);
      this.MobileLogTo.Name = "MobileLogTo";
      this.MobileLogTo.ShowCheckBox = true;
      this.MobileLogTo.Size = new Size(145, 22);
      this.MobileLogTo.TabIndex = 47;
      this.label3.AutoSize = true;
      this.label3.Location = new Point(12, 158);
      this.label3.Margin = new Padding(4, 0, 4, 0);
      this.label3.Name = "label3";
      this.label3.Size = new Size(45, 17);
      this.label3.TabIndex = 46;
      this.label3.Text = "Clear:";
      this.button15.Location = new Point(139, 153);
      this.button15.Margin = new Padding(4);
      this.button15.Name = "button15";
      this.button15.Size = new Size(75, 30);
      this.button15.TabIndex = 45;
      this.button15.Text = "Polygons";
      this.button15.UseVisualStyleBackColor = true;
      this.button15.Click += new EventHandler(this.button15_Click);
      this.MobileLogFrom.CustomFormat = "yyyy'.'MM'.'dd HH':'mm";
      this.MobileLogFrom.Format = DateTimePickerFormat.Custom;
      this.MobileLogFrom.Location = new Point(12, 96);
      this.MobileLogFrom.Margin = new Padding(3, 2, 3, 2);
      this.MobileLogFrom.Name = "MobileLogFrom";
      this.MobileLogFrom.ShowCheckBox = true;
      this.MobileLogFrom.Size = new Size(145, 22);
      this.MobileLogFrom.TabIndex = 44;
      this.MobileLogFrom.Value = new DateTime(2010, 5, 10, 15, 41, 0, 0);
      this.button14.Location = new Point(117, 60);
      this.button14.Margin = new Padding(4);
      this.button14.Name = "button14";
      this.button14.Size = new Size(97, 30);
      this.button14.TabIndex = 43;
      this.button14.Text = "Mobile log...";
      this.button14.UseVisualStyleBackColor = true;
      this.button14.Click += new EventHandler(this.button14_Click);
      this.buttonSetEnd.Location = new Point(117, 23);
      this.buttonSetEnd.Margin = new Padding(4);
      this.buttonSetEnd.Name = "buttonSetEnd";
      this.buttonSetEnd.Size = new Size(97, 30);
      this.buttonSetEnd.TabIndex = 42;
      this.buttonSetEnd.Text = "set End";
      this.buttonSetEnd.UseVisualStyleBackColor = true;
      this.buttonSetEnd.Click += new EventHandler(this.buttonSetEnd_Click);
      this.buttonSetStart.Location = new Point(12, 23);
      this.buttonSetStart.Margin = new Padding(4);
      this.buttonSetStart.Name = "buttonSetStart";
      this.buttonSetStart.Size = new Size(97, 30);
      this.buttonSetStart.TabIndex = 41;
      this.buttonSetStart.Text = "set Start";
      this.buttonSetStart.UseVisualStyleBackColor = true;
      this.buttonSetStart.Click += new EventHandler(this.buttonSetStart_Click);
      this.button6.Location = new Point(60, 153);
      this.button6.Margin = new Padding(4);
      this.button6.Name = "button6";
      this.button6.Size = new Size(73, 30);
      this.button6.TabIndex = 34;
      this.button6.Text = "Routes";
      this.button6.UseVisualStyleBackColor = true;
      this.button6.Click += new EventHandler(this.button6_Click);
      this.groupBox8.Controls.Add((Control) this.button7);
      this.groupBox8.Controls.Add((Control) this.checkBoxPlacemarkInfo);
      this.groupBox8.Controls.Add((Control) this.button5);
      this.groupBox8.Controls.Add((Control) this.button4);
      this.groupBox8.Dock = DockStyle.Fill;
      this.groupBox8.Location = new Point(4, 557);
      this.groupBox8.Margin = new Padding(4);
      this.groupBox8.Name = "groupBox8";
      this.groupBox8.Padding = new Padding(4);
      this.groupBox8.Size = new Size(224, 91);
      this.groupBox8.TabIndex = 37;
      this.groupBox8.TabStop = false;
      this.groupBox8.Text = "markers";
      this.button7.Location = new Point(8, 55);
      this.button7.Margin = new Padding(4);
      this.button7.Name = "button7";
      this.button7.Size = new Size(109, 30);
      this.button7.TabIndex = 15;
      this.button7.Text = "Zoom Center";
      this.button7.UseVisualStyleBackColor = true;
      this.button7.Click += new EventHandler(this.button7_Click);
      this.checkBoxPlacemarkInfo.AutoSize = true;
      this.checkBoxPlacemarkInfo.CheckAlign = ContentAlignment.TopLeft;
      this.checkBoxPlacemarkInfo.Checked = true;
      this.checkBoxPlacemarkInfo.CheckState = CheckState.Checked;
      this.checkBoxPlacemarkInfo.Location = new Point(125, 23);
      this.checkBoxPlacemarkInfo.Margin = new Padding(4);
      this.checkBoxPlacemarkInfo.Name = "checkBoxPlacemarkInfo";
      this.checkBoxPlacemarkInfo.Size = new Size(91, 21);
      this.checkBoxPlacemarkInfo.TabIndex = 14;
      this.checkBoxPlacemarkInfo.Text = "place info";
      this.checkBoxPlacemarkInfo.UseVisualStyleBackColor = true;
      this.panelMenu.AssociatedSplitter = (System.Windows.Forms.Splitter) this.splitter1;
      this.panelMenu.BackColor = Color.Transparent;
      this.panelMenu.CaptionFont = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.panelMenu.CaptionHeight = 27;
      this.panelMenu.Controls.Add((Control) this.xPanderPanelList1);
      this.panelMenu.CustomColors.BorderColor = Color.FromArgb(184, 184, 184);
      this.panelMenu.CustomColors.CaptionCloseIcon = SystemColors.ControlText;
      this.panelMenu.CustomColors.CaptionExpandIcon = SystemColors.ControlText;
      this.panelMenu.CustomColors.CaptionGradientBegin = Color.FromArgb(252, 252, 252);
      this.panelMenu.CustomColors.CaptionGradientEnd = SystemColors.ButtonFace;
      this.panelMenu.CustomColors.CaptionGradientMiddle = Color.FromArgb(248, 248, 248);
      this.panelMenu.CustomColors.CaptionSelectedGradientBegin = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.panelMenu.CustomColors.CaptionSelectedGradientEnd = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.panelMenu.CustomColors.CaptionText = SystemColors.ControlText;
      this.panelMenu.CustomColors.CollapsedCaptionText = SystemColors.ControlText;
      this.panelMenu.CustomColors.ContentGradientBegin = SystemColors.ButtonFace;
      this.panelMenu.CustomColors.ContentGradientEnd = Color.FromArgb(252, 252, 252);
      this.panelMenu.CustomColors.InnerBorderColor = SystemColors.Window;
      this.panelMenu.Dock = DockStyle.Right;
      this.panelMenu.ForeColor = SystemColors.ControlText;
      this.panelMenu.Image = (Image) null;
      this.panelMenu.LinearGradientMode = LinearGradientMode.Vertical;
      this.panelMenu.Location = new Point(884, 0);
      this.panelMenu.Margin = new Padding(3, 2, 3, 2);
      this.panelMenu.MinimumSize = new Size(27, 27);
      this.panelMenu.Name = "panelMenu";
      this.panelMenu.PanelStyle = PanelStyle.Office2007;
      this.panelMenu.ShowExpandIcon = true;
      this.panelMenu.Size = new Size(304, 818);
      this.panelMenu.TabIndex = 40;
      this.panelMenu.Text = "Menu";
      this.panelMenu.ToolTipTextCloseIcon = (string) null;
      this.panelMenu.ToolTipTextExpandIconPanelCollapsed = "maximize";
      this.panelMenu.ToolTipTextExpandIconPanelExpanded = "minimize";
      this.splitter1.BackColor = Color.Transparent;
      this.splitter1.Dock = DockStyle.Right;
      this.splitter1.Enabled = false;
      this.splitter1.Location = new Point(881, 0);
      this.splitter1.Margin = new Padding(3, 2, 3, 2);
      this.splitter1.MinExtra = 390;
      this.splitter1.MinSize = 390;
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new Size(3, 818);
      this.splitter1.TabIndex = 42;
      this.splitter1.TabStop = false;
      this.xPanderPanelList1.CaptionStyle = CaptionStyle.Flat;
      this.xPanderPanelList1.Controls.Add((Control) this.xPanderPanelMain);
      this.xPanderPanelList1.Controls.Add((Control) this.xPanderPanelCache);
      this.xPanderPanelList1.Controls.Add((Control) this.xPanderPanelInfo);
      this.xPanderPanelList1.Controls.Add((Control) this.xPanderPanelLive);
      this.xPanderPanelList1.Dock = DockStyle.Fill;
      this.xPanderPanelList1.GradientBackground = Color.Empty;
      this.xPanderPanelList1.Location = new Point(0, 28);
      this.xPanderPanelList1.Margin = new Padding(3, 2, 3, 2);
      this.xPanderPanelList1.Name = "xPanderPanelList1";
      this.xPanderPanelList1.PanelColors = (PanelColors) null;
      this.xPanderPanelList1.PanelStyle = PanelStyle.Office2007;
      this.xPanderPanelList1.ShowExpandIcon = true;
      this.xPanderPanelList1.Size = new Size(304, 789);
      this.xPanderPanelList1.TabIndex = 0;
      this.xPanderPanelList1.Text = "xPanderPanelList1";
      this.xPanderPanelMain.CaptionFont = new Font("Segoe UI", 8f, FontStyle.Bold);
      this.xPanderPanelMain.Controls.Add((Control) this.tableLayoutPanel4);
      this.xPanderPanelMain.CustomColors.BackColor = SystemColors.Control;
      this.xPanderPanelMain.CustomColors.BorderColor = Color.FromArgb(184, 184, 184);
      this.xPanderPanelMain.CustomColors.CaptionCheckedGradientBegin = Color.Empty;
      this.xPanderPanelMain.CustomColors.CaptionCheckedGradientEnd = Color.Empty;
      this.xPanderPanelMain.CustomColors.CaptionCheckedGradientMiddle = Color.Empty;
      this.xPanderPanelMain.CustomColors.CaptionCloseIcon = SystemColors.ControlText;
      this.xPanderPanelMain.CustomColors.CaptionExpandIcon = SystemColors.ControlText;
      this.xPanderPanelMain.CustomColors.CaptionGradientBegin = Color.FromArgb(252, 252, 252);
      this.xPanderPanelMain.CustomColors.CaptionGradientEnd = SystemColors.ButtonFace;
      this.xPanderPanelMain.CustomColors.CaptionGradientMiddle = Color.FromArgb(248, 248, 248);
      this.xPanderPanelMain.CustomColors.CaptionPressedGradientBegin = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelMain.CustomColors.CaptionPressedGradientEnd = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelMain.CustomColors.CaptionPressedGradientMiddle = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelMain.CustomColors.CaptionSelectedGradientBegin = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelMain.CustomColors.CaptionSelectedGradientEnd = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelMain.CustomColors.CaptionSelectedGradientMiddle = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelMain.CustomColors.CaptionSelectedText = SystemColors.ControlText;
      this.xPanderPanelMain.CustomColors.CaptionText = SystemColors.ControlText;
      this.xPanderPanelMain.CustomColors.FlatCaptionGradientBegin = Color.FromArgb(248, 248, 248);
      this.xPanderPanelMain.CustomColors.FlatCaptionGradientEnd = Color.FromArgb(252, 252, 252);
      this.xPanderPanelMain.CustomColors.InnerBorderColor = SystemColors.Window;
      this.xPanderPanelMain.Expand = true;
      this.xPanderPanelMain.ForeColor = SystemColors.ControlText;
      this.xPanderPanelMain.Image = (Image) null;
      this.xPanderPanelMain.IsClosable = false;
      this.xPanderPanelMain.Margin = new Padding(3, 2, 3, 2);
      this.xPanderPanelMain.Name = "xPanderPanelMain";
      this.xPanderPanelMain.PanelStyle = PanelStyle.Office2007;
      this.xPanderPanelMain.Size = new Size(304, 714);
      this.xPanderPanelMain.TabIndex = 0;
      this.xPanderPanelMain.Text = "map";
      this.xPanderPanelMain.ToolTipTextCloseIcon = (string) null;
      this.xPanderPanelMain.ToolTipTextExpandIconPanelCollapsed = (string) null;
      this.xPanderPanelMain.ToolTipTextExpandIconPanelExpanded = (string) null;
      this.xPanderPanelMain.Click += new EventHandler(this.xPanderPanel1_Click);
      this.tableLayoutPanel4.ColumnCount = 2;
      this.tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 49.27536f));
      this.tableLayoutPanel4.Controls.Add((Control) this.trackBar1, 0, 0);
      this.tableLayoutPanel4.Controls.Add((Control) this.tableLayoutPanel5, 1, 0);
      this.tableLayoutPanel4.Dock = DockStyle.Fill;
      this.tableLayoutPanel4.Location = new Point(1, 25);
      this.tableLayoutPanel4.Margin = new Padding(3, 2, 3, 2);
      this.tableLayoutPanel4.Name = "tableLayoutPanel4";
      this.tableLayoutPanel4.RowCount = 1;
      this.tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel4.Size = new Size(302, 689);
      this.tableLayoutPanel4.TabIndex = 38;
      this.tableLayoutPanel5.ColumnCount = 1;
      this.tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel5.Controls.Add((Control) this.groupBox8, 0, 3);
      this.tableLayoutPanel5.Controls.Add((Control) this.groupBox7, 0, 2);
      this.tableLayoutPanel5.Controls.Add((Control) this.groupBox5, 0, 1);
      this.tableLayoutPanel5.Controls.Add((Control) this.groupBox3, 0, 0);
      this.tableLayoutPanel5.Dock = DockStyle.Fill;
      this.tableLayoutPanel5.Location = new Point(67, 2);
      this.tableLayoutPanel5.Margin = new Padding(3, 2, 3, 2);
      this.tableLayoutPanel5.Name = "tableLayoutPanel5";
      this.tableLayoutPanel5.RowCount = 5;
      this.tableLayoutPanel5.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel5.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel5.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel5.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Absolute, 7f));
      this.tableLayoutPanel5.Size = new Size(232, 685);
      this.tableLayoutPanel5.TabIndex = 30;
      this.xPanderPanelCache.CaptionFont = new Font("Segoe UI", 8f, FontStyle.Bold);
      this.xPanderPanelCache.Controls.Add((Control) this.tableLayoutPanel1);
      this.xPanderPanelCache.CustomColors.BackColor = SystemColors.Control;
      this.xPanderPanelCache.CustomColors.BorderColor = Color.FromArgb(184, 184, 184);
      this.xPanderPanelCache.CustomColors.CaptionCheckedGradientBegin = Color.Empty;
      this.xPanderPanelCache.CustomColors.CaptionCheckedGradientEnd = Color.Empty;
      this.xPanderPanelCache.CustomColors.CaptionCheckedGradientMiddle = Color.Empty;
      this.xPanderPanelCache.CustomColors.CaptionCloseIcon = SystemColors.ControlText;
      this.xPanderPanelCache.CustomColors.CaptionExpandIcon = SystemColors.ControlText;
      this.xPanderPanelCache.CustomColors.CaptionGradientBegin = Color.FromArgb(252, 252, 252);
      this.xPanderPanelCache.CustomColors.CaptionGradientEnd = SystemColors.ButtonFace;
      this.xPanderPanelCache.CustomColors.CaptionGradientMiddle = Color.FromArgb(248, 248, 248);
      this.xPanderPanelCache.CustomColors.CaptionPressedGradientBegin = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelCache.CustomColors.CaptionPressedGradientEnd = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelCache.CustomColors.CaptionPressedGradientMiddle = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelCache.CustomColors.CaptionSelectedGradientBegin = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelCache.CustomColors.CaptionSelectedGradientEnd = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelCache.CustomColors.CaptionSelectedGradientMiddle = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelCache.CustomColors.CaptionSelectedText = SystemColors.ControlText;
      this.xPanderPanelCache.CustomColors.CaptionText = SystemColors.ControlText;
      this.xPanderPanelCache.CustomColors.FlatCaptionGradientBegin = Color.FromArgb(248, 248, 248);
      this.xPanderPanelCache.CustomColors.FlatCaptionGradientEnd = Color.FromArgb(252, 252, 252);
      this.xPanderPanelCache.CustomColors.InnerBorderColor = SystemColors.Window;
      this.xPanderPanelCache.ForeColor = SystemColors.ControlText;
      this.xPanderPanelCache.Image = (Image) null;
      this.xPanderPanelCache.IsClosable = false;
      this.xPanderPanelCache.Margin = new Padding(3, 2, 3, 2);
      this.xPanderPanelCache.Name = "xPanderPanelCache";
      this.xPanderPanelCache.PanelStyle = PanelStyle.Office2007;
      this.xPanderPanelCache.Size = new Size(304, 25);
      this.xPanderPanelCache.TabIndex = 1;
      this.xPanderPanelCache.Text = "cache";
      this.xPanderPanelCache.ToolTipTextCloseIcon = (string) null;
      this.xPanderPanelCache.ToolTipTextExpandIconPanelCollapsed = (string) null;
      this.xPanderPanelCache.ToolTipTextExpandIconPanelExpanded = (string) null;
      this.xPanderPanelCache.Click += new EventHandler(this.xPanderPanelCache_Click);
      this.tableLayoutPanel1.ColumnCount = 3;
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel1.Controls.Add((Control) this.button10, 1, 1);
      this.tableLayoutPanel1.Controls.Add((Control) this.checkBoxUseGeoCache, 1, 11);
      this.tableLayoutPanel1.Controls.Add((Control) this.textBoxMemory, 1, 8);
      this.tableLayoutPanel1.Controls.Add((Control) this.checkBoxUseRouteCache, 1, 10);
      this.tableLayoutPanel1.Controls.Add((Control) this.button9, 1, 2);
      this.tableLayoutPanel1.Controls.Add((Control) this.button11, 1, 3);
      this.tableLayoutPanel1.Controls.Add((Control) this.label10, 1, 7);
      this.tableLayoutPanel1.Controls.Add((Control) this.button2, 1, 5);
      this.tableLayoutPanel1.Dock = DockStyle.Fill;
      this.tableLayoutPanel1.Location = new Point(1, 25);
      this.tableLayoutPanel1.Margin = new Padding(3, 2, 3, 2);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 13;
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 32f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 33f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel1.Size = new Size(302, 0);
      this.tableLayoutPanel1.TabIndex = 41;
      this.textBoxMemory.Dock = DockStyle.Top;
      this.textBoxMemory.Font = new Font("Microsoft Sans Serif", 13.8f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.textBoxMemory.Location = new Point(24, 192);
      this.textBoxMemory.Margin = new Padding(4);
      this.textBoxMemory.Name = "textBoxMemory";
      this.textBoxMemory.ReadOnly = true;
      this.textBoxMemory.Size = new Size(254, 34);
      this.textBoxMemory.TabIndex = 39;
      this.textBoxMemory.Text = "...";
      this.label10.AutoSize = true;
      this.label10.Location = new Point(24, 171);
      this.label10.Margin = new Padding(4, 0, 4, 0);
      this.label10.Name = "label10";
      this.label10.Size = new Size(104, 17);
      this.label10.TabIndex = 40;
      this.label10.Text = "memory cache:";
      this.button2.Dock = DockStyle.Top;
      this.button2.Location = new Point(24, 122);
      this.button2.Margin = new Padding(4);
      this.button2.Name = "button2";
      this.button2.Size = new Size(254, 25);
      this.button2.TabIndex = 43;
      this.button2.Text = "Clear All";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.xPanderPanelInfo.CaptionFont = new Font("Segoe UI", 8f, FontStyle.Bold);
      this.xPanderPanelInfo.Controls.Add((Control) this.tableLayoutPanel2);
      this.xPanderPanelInfo.CustomColors.BackColor = SystemColors.Control;
      this.xPanderPanelInfo.CustomColors.BorderColor = Color.FromArgb(184, 184, 184);
      this.xPanderPanelInfo.CustomColors.CaptionCheckedGradientBegin = Color.Empty;
      this.xPanderPanelInfo.CustomColors.CaptionCheckedGradientEnd = Color.Empty;
      this.xPanderPanelInfo.CustomColors.CaptionCheckedGradientMiddle = Color.Empty;
      this.xPanderPanelInfo.CustomColors.CaptionCloseIcon = SystemColors.ControlText;
      this.xPanderPanelInfo.CustomColors.CaptionExpandIcon = SystemColors.ControlText;
      this.xPanderPanelInfo.CustomColors.CaptionGradientBegin = Color.FromArgb(252, 252, 252);
      this.xPanderPanelInfo.CustomColors.CaptionGradientEnd = SystemColors.ButtonFace;
      this.xPanderPanelInfo.CustomColors.CaptionGradientMiddle = Color.FromArgb(248, 248, 248);
      this.xPanderPanelInfo.CustomColors.CaptionPressedGradientBegin = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelInfo.CustomColors.CaptionPressedGradientEnd = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelInfo.CustomColors.CaptionPressedGradientMiddle = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelInfo.CustomColors.CaptionSelectedGradientBegin = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelInfo.CustomColors.CaptionSelectedGradientEnd = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelInfo.CustomColors.CaptionSelectedGradientMiddle = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelInfo.CustomColors.CaptionSelectedText = SystemColors.ControlText;
      this.xPanderPanelInfo.CustomColors.CaptionText = SystemColors.ControlText;
      this.xPanderPanelInfo.CustomColors.FlatCaptionGradientBegin = Color.FromArgb(248, 248, 248);
      this.xPanderPanelInfo.CustomColors.FlatCaptionGradientEnd = Color.FromArgb(252, 252, 252);
      this.xPanderPanelInfo.CustomColors.InnerBorderColor = SystemColors.Window;
      this.xPanderPanelInfo.ForeColor = SystemColors.ControlText;
      this.xPanderPanelInfo.Image = (Image) null;
      this.xPanderPanelInfo.Margin = new Padding(3, 2, 3, 2);
      this.xPanderPanelInfo.Name = "xPanderPanelInfo";
      this.xPanderPanelInfo.PanelStyle = PanelStyle.Office2007;
      this.xPanderPanelInfo.Size = new Size(304, 25);
      this.xPanderPanelInfo.TabIndex = 3;
      this.xPanderPanelInfo.Text = "info";
      this.xPanderPanelInfo.ToolTipTextCloseIcon = (string) null;
      this.xPanderPanelInfo.ToolTipTextExpandIconPanelCollapsed = (string) null;
      this.xPanderPanelInfo.ToolTipTextExpandIconPanelExpanded = (string) null;
      this.xPanderPanelInfo.Click += new EventHandler(this.xPanderPanelInfo_Click);
      this.tableLayoutPanel2.ColumnCount = 3;
      this.tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel2.Controls.Add((Control) this.textBoxZoomCurrent, 1, 7);
      this.tableLayoutPanel2.Controls.Add((Control) this.textBoxrouteCount, 1, 11);
      this.tableLayoutPanel2.Controls.Add((Control) this.label12, 1, 10);
      this.tableLayoutPanel2.Controls.Add((Control) this.label9, 1, 6);
      this.tableLayoutPanel2.Controls.Add((Control) this.textBoxLngCurrent, 1, 4);
      this.tableLayoutPanel2.Controls.Add((Control) this.textBoxMarkerCount, 1, 9);
      this.tableLayoutPanel2.Controls.Add((Control) this.label11, 1, 8);
      this.tableLayoutPanel2.Controls.Add((Control) this.label4, 1, 3);
      this.tableLayoutPanel2.Controls.Add((Control) this.textBoxLatCurrent, 1, 2);
      this.tableLayoutPanel2.Controls.Add((Control) this.label5, 1, 1);
      this.tableLayoutPanel2.Dock = DockStyle.Fill;
      this.tableLayoutPanel2.Location = new Point(1, 25);
      this.tableLayoutPanel2.Margin = new Padding(3, 2, 3, 2);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 13;
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel2.Size = new Size(302, 0);
      this.tableLayoutPanel2.TabIndex = 14;
      this.textBoxZoomCurrent.Dock = DockStyle.Fill;
      this.textBoxZoomCurrent.Font = new Font("Microsoft Sans Serif", 13.8f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.textBoxZoomCurrent.Location = new Point(24, 179);
      this.textBoxZoomCurrent.Margin = new Padding(4);
      this.textBoxZoomCurrent.Name = "textBoxZoomCurrent";
      this.textBoxZoomCurrent.ReadOnly = true;
      this.textBoxZoomCurrent.Size = new Size(254, 34);
      this.textBoxZoomCurrent.TabIndex = 8;
      this.textBoxZoomCurrent.Text = "...";
      this.textBoxrouteCount.Dock = DockStyle.Fill;
      this.textBoxrouteCount.Font = new Font("Microsoft Sans Serif", 13.8f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.textBoxrouteCount.Location = new Point(24, 297);
      this.textBoxrouteCount.Margin = new Padding(4);
      this.textBoxrouteCount.Name = "textBoxrouteCount";
      this.textBoxrouteCount.ReadOnly = true;
      this.textBoxrouteCount.Size = new Size(254, 34);
      this.textBoxrouteCount.TabIndex = 12;
      this.textBoxrouteCount.Text = "...";
      this.label12.AutoSize = true;
      this.label12.Location = new Point(24, 276);
      this.label12.Margin = new Padding(4, 0, 4, 0);
      this.label12.Name = "label12";
      this.label12.Size = new Size(52, 17);
      this.label12.TabIndex = 13;
      this.label12.Text = "routes:";
      this.label9.AutoSize = true;
      this.label9.Location = new Point(24, 158);
      this.label9.Margin = new Padding(4, 0, 4, 0);
      this.label9.Name = "label9";
      this.label9.Size = new Size(46, 17);
      this.label9.TabIndex = 9;
      this.label9.Text = "zoom:";
      this.textBoxLngCurrent.Dock = DockStyle.Fill;
      this.textBoxLngCurrent.Font = new Font("Microsoft Sans Serif", 13.8f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.textBoxLngCurrent.Location = new Point(24, 100);
      this.textBoxLngCurrent.Margin = new Padding(4);
      this.textBoxLngCurrent.Name = "textBoxLngCurrent";
      this.textBoxLngCurrent.ReadOnly = true;
      this.textBoxLngCurrent.Size = new Size(254, 34);
      this.textBoxLngCurrent.TabIndex = 5;
      this.textBoxLngCurrent.Text = "...";
      this.textBoxMarkerCount.Dock = DockStyle.Fill;
      this.textBoxMarkerCount.Font = new Font("Microsoft Sans Serif", 13.8f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.textBoxMarkerCount.Location = new Point(24, 238);
      this.textBoxMarkerCount.Margin = new Padding(4);
      this.textBoxMarkerCount.Name = "textBoxMarkerCount";
      this.textBoxMarkerCount.ReadOnly = true;
      this.textBoxMarkerCount.Size = new Size(254, 34);
      this.textBoxMarkerCount.TabIndex = 10;
      this.textBoxMarkerCount.Text = "...";
      this.label11.AutoSize = true;
      this.label11.Location = new Point(24, 217);
      this.label11.Margin = new Padding(4, 0, 4, 0);
      this.label11.Name = "label11";
      this.label11.Size = new Size(63, 17);
      this.label11.TabIndex = 11;
      this.label11.Text = "markers:";
      this.label4.AutoSize = true;
      this.label4.Location = new Point(24, 79);
      this.label4.Margin = new Padding(4, 0, 4, 0);
      this.label4.Name = "label4";
      this.label4.Size = new Size(31, 17);
      this.label4.TabIndex = 7;
      this.label4.Text = "lng:";
      this.textBoxLatCurrent.Dock = DockStyle.Fill;
      this.textBoxLatCurrent.Font = new Font("Microsoft Sans Serif", 13.8f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.textBoxLatCurrent.Location = new Point(24, 41);
      this.textBoxLatCurrent.Margin = new Padding(4);
      this.textBoxLatCurrent.Name = "textBoxLatCurrent";
      this.textBoxLatCurrent.ReadOnly = true;
      this.textBoxLatCurrent.Size = new Size(254, 34);
      this.textBoxLatCurrent.TabIndex = 4;
      this.textBoxLatCurrent.Text = "...";
      this.label5.AutoSize = true;
      this.label5.Location = new Point(24, 20);
      this.label5.Margin = new Padding(4, 0, 4, 0);
      this.label5.Name = "label5";
      this.label5.Size = new Size(27, 17);
      this.label5.TabIndex = 6;
      this.label5.Text = "lat:";
      this.xPanderPanelLive.CaptionFont = new Font("Segoe UI", 8f, FontStyle.Bold);
      this.xPanderPanelLive.Controls.Add((Control) this.tableLayoutPanel3);
      this.xPanderPanelLive.CustomColors.BackColor = SystemColors.Control;
      this.xPanderPanelLive.CustomColors.BorderColor = Color.FromArgb(184, 184, 184);
      this.xPanderPanelLive.CustomColors.CaptionCheckedGradientBegin = Color.Empty;
      this.xPanderPanelLive.CustomColors.CaptionCheckedGradientEnd = Color.Empty;
      this.xPanderPanelLive.CustomColors.CaptionCheckedGradientMiddle = Color.Empty;
      this.xPanderPanelLive.CustomColors.CaptionCloseIcon = SystemColors.ControlText;
      this.xPanderPanelLive.CustomColors.CaptionExpandIcon = SystemColors.ControlText;
      this.xPanderPanelLive.CustomColors.CaptionGradientBegin = Color.FromArgb(252, 252, 252);
      this.xPanderPanelLive.CustomColors.CaptionGradientEnd = SystemColors.ButtonFace;
      this.xPanderPanelLive.CustomColors.CaptionGradientMiddle = Color.FromArgb(248, 248, 248);
      this.xPanderPanelLive.CustomColors.CaptionPressedGradientBegin = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelLive.CustomColors.CaptionPressedGradientEnd = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelLive.CustomColors.CaptionPressedGradientMiddle = Color.FromArgb(128, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelLive.CustomColors.CaptionSelectedGradientBegin = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelLive.CustomColors.CaptionSelectedGradientEnd = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelLive.CustomColors.CaptionSelectedGradientMiddle = Color.FromArgb(179, (int) byte.MaxValue, (int) byte.MaxValue);
      this.xPanderPanelLive.CustomColors.CaptionSelectedText = SystemColors.ControlText;
      this.xPanderPanelLive.CustomColors.CaptionText = SystemColors.ControlText;
      this.xPanderPanelLive.CustomColors.FlatCaptionGradientBegin = Color.FromArgb(248, 248, 248);
      this.xPanderPanelLive.CustomColors.FlatCaptionGradientEnd = Color.FromArgb(252, 252, 252);
      this.xPanderPanelLive.CustomColors.InnerBorderColor = SystemColors.Window;
      this.xPanderPanelLive.ForeColor = SystemColors.ControlText;
      this.xPanderPanelLive.Image = (Image) null;
      this.xPanderPanelLive.IsClosable = false;
      this.xPanderPanelLive.Margin = new Padding(3, 2, 3, 2);
      this.xPanderPanelLive.Name = "xPanderPanelLive";
      this.xPanderPanelLive.Padding = new Padding(0, 0, 0, 30);
      this.xPanderPanelLive.PanelStyle = PanelStyle.Office2007;
      this.xPanderPanelLive.Size = new Size(304, 25);
      this.xPanderPanelLive.TabIndex = 2;
      this.xPanderPanelLive.Text = "live";
      this.xPanderPanelLive.ToolTipTextCloseIcon = (string) null;
      this.xPanderPanelLive.ToolTipTextExpandIconPanelCollapsed = (string) null;
      this.xPanderPanelLive.ToolTipTextExpandIconPanelExpanded = (string) null;
      this.xPanderPanelLive.Click += new EventHandler(this.xPanderPanelLive_Click);
      this.tableLayoutPanel3.ColumnCount = 3;
      this.tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 29.06977f));
      this.tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel3.Controls.Add((Control) this.radioButtonNone, 1, 1);
      this.tableLayoutPanel3.Controls.Add((Control) this.checkBoxTcpIpSnap, 1, 7);
      this.tableLayoutPanel3.Controls.Add((Control) this.GridConnections, 1, 6);
      this.tableLayoutPanel3.Controls.Add((Control) this.radioButtonFlight, 1, 3);
      this.tableLayoutPanel3.Controls.Add((Control) this.radioButtonPerf, 1, 4);
      this.tableLayoutPanel3.Controls.Add((Control) this.radioButtonTcpIp, 1, 5);
      this.tableLayoutPanel3.Controls.Add((Control) this.checkBoxTraceRoute, 1, 8);
      this.tableLayoutPanel3.Controls.Add((Control) this.radioButtonVehicle, 1, 2);
      this.tableLayoutPanel3.Dock = DockStyle.Fill;
      this.tableLayoutPanel3.Location = new Point(1, 25);
      this.tableLayoutPanel3.Margin = new Padding(3, 2, 3, 2);
      this.tableLayoutPanel3.Name = "tableLayoutPanel3";
      this.tableLayoutPanel3.RowCount = 9;
      this.tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel3.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel3.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel3.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel3.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel3.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel3.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel3.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel3.Size = new Size(302, 0);
      this.tableLayoutPanel3.TabIndex = 6;
      this.radioButtonNone.AutoSize = true;
      this.radioButtonNone.Checked = true;
      this.radioButtonNone.Location = new Point(24, 24);
      this.radioButtonNone.Margin = new Padding(4);
      this.radioButtonNone.Name = "radioButtonNone";
      this.radioButtonNone.Size = new Size(63, 21);
      this.radioButtonNone.TabIndex = 2;
      this.radioButtonNone.TabStop = true;
      this.radioButtonNone.Text = "None";
      this.radioButtonNone.UseVisualStyleBackColor = true;
      this.radioButtonNone.CheckedChanged += new EventHandler(this.RealTimeChanged);
      this.checkBoxTcpIpSnap.AutoSize = true;
      this.checkBoxTcpIpSnap.Checked = true;
      this.checkBoxTcpIpSnap.CheckState = CheckState.Checked;
      this.checkBoxTcpIpSnap.Location = new Point(23, -47);
      this.checkBoxTcpIpSnap.Margin = new Padding(3, 2, 3, 2);
      this.checkBoxTcpIpSnap.Name = "checkBoxTcpIpSnap";
      this.checkBoxTcpIpSnap.Size = new Size(238, 21);
      this.checkBoxTcpIpSnap.TabIndex = 4;
      this.checkBoxTcpIpSnap.Text = "Snap position on new connection";
      this.checkBoxTcpIpSnap.UseVisualStyleBackColor = true;
      this.checkBoxTcpIpSnap.Visible = false;
      this.GridConnections.AllowUserToAddRows = false;
      this.GridConnections.AllowUserToDeleteRows = false;
      this.GridConnections.AllowUserToResizeRows = false;
      this.GridConnections.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.GridConnections.Columns.AddRange((DataGridViewColumn) this.CountryName, (DataGridViewColumn) this.ConnectionsCount);
      gridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      gridViewCellStyle.BackColor = Color.AliceBlue;
      gridViewCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      gridViewCellStyle.ForeColor = SystemColors.ControlText;
      gridViewCellStyle.SelectionBackColor = Color.LightSkyBlue;
      gridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
      gridViewCellStyle.WrapMode = DataGridViewTriState.False;
      this.GridConnections.DefaultCellStyle = gridViewCellStyle;
      this.GridConnections.Dock = DockStyle.Fill;
      this.GridConnections.EditMode = DataGridViewEditMode.EditProgrammatically;
      this.GridConnections.Location = new Point(23, 167);
      this.GridConnections.Margin = new Padding(3, 2, 3, 2);
      this.GridConnections.Name = "GridConnections";
      this.GridConnections.ReadOnly = true;
      this.GridConnections.RowHeadersVisible = false;
      this.GridConnections.RowTemplate.Height = 24;
      this.GridConnections.ScrollBars = ScrollBars.Vertical;
      this.GridConnections.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      this.GridConnections.Size = new Size(256, 1);
      this.GridConnections.TabIndex = 5;
      this.GridConnections.VirtualMode = true;
      this.GridConnections.Visible = false;
      this.GridConnections.CellValueNeeded += new DataGridViewCellValueEventHandler(this.GridConnections_CellValueNeeded);
      this.GridConnections.DoubleClick += new EventHandler(this.GridConnections_DoubleClick);
      this.CountryName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.CountryName.DataPropertyName = "CountryName";
      this.CountryName.FillWeight = 200f;
      this.CountryName.HeaderText = "Country";
      this.CountryName.Name = "CountryName";
      this.CountryName.ReadOnly = true;
      this.ConnectionsCount.DataPropertyName = "ConnectionsCount";
      this.ConnectionsCount.HeaderText = "Connections";
      this.ConnectionsCount.Name = "ConnectionsCount";
      this.ConnectionsCount.ReadOnly = true;
      this.radioButtonFlight.AutoSize = true;
      this.radioButtonFlight.Location = new Point(24, 82);
      this.radioButtonFlight.Margin = new Padding(4);
      this.radioButtonFlight.Name = "radioButtonFlight";
      this.radioButtonFlight.Size = new Size(141, 21);
      this.radioButtonFlight.TabIndex = 0;
      this.radioButtonFlight.Text = "FlightRadar demo";
      this.radioButtonFlight.UseVisualStyleBackColor = true;
      this.radioButtonFlight.CheckedChanged += new EventHandler(this.RealTimeChanged);
      this.radioButtonPerf.AutoSize = true;
      this.radioButtonPerf.Location = new Point(24, 111);
      this.radioButtonPerf.Margin = new Padding(4);
      this.radioButtonPerf.Name = "radioButtonPerf";
      this.radioButtonPerf.Size = new Size(137, 21);
      this.radioButtonPerf.TabIndex = 1;
      this.radioButtonPerf.Text = "Performance test";
      this.radioButtonPerf.UseVisualStyleBackColor = true;
      this.radioButtonPerf.CheckedChanged += new EventHandler(this.RealTimeChanged);
      this.radioButtonTcpIp.AutoSize = true;
      this.radioButtonTcpIp.Location = new Point(24, 140);
      this.radioButtonTcpIp.Margin = new Padding(4);
      this.radioButtonTcpIp.Name = "radioButtonTcpIp";
      this.radioButtonTcpIp.Size = new Size(152, 21);
      this.radioButtonTcpIp.TabIndex = 3;
      this.radioButtonTcpIp.Text = "TCP/IP connections";
      this.radioButtonTcpIp.UseVisualStyleBackColor = true;
      this.radioButtonTcpIp.CheckedChanged += new EventHandler(this.RealTimeChanged);
      this.checkBoxTraceRoute.AutoSize = true;
      this.checkBoxTraceRoute.Location = new Point(23, -22);
      this.checkBoxTraceRoute.Margin = new Padding(3, 2, 3, 2);
      this.checkBoxTraceRoute.Name = "checkBoxTraceRoute";
      this.checkBoxTraceRoute.Size = new Size(233, 21);
      this.checkBoxTraceRoute.TabIndex = 6;
      this.checkBoxTraceRoute.Text = "Trace route for each connection";
      this.checkBoxTraceRoute.UseVisualStyleBackColor = true;
      this.checkBoxTraceRoute.Visible = false;
      this.checkBoxTraceRoute.CheckedChanged += new EventHandler(this.checkBoxTraceRoute_CheckedChanged);
      this.radioButtonVehicle.AutoSize = true;
      this.radioButtonVehicle.Location = new Point(24, 53);
      this.radioButtonVehicle.Margin = new Padding(4);
      this.radioButtonVehicle.Name = "radioButtonVehicle";
      this.radioButtonVehicle.Size = new Size(114, 21);
      this.radioButtonVehicle.TabIndex = 7;
      this.radioButtonVehicle.Text = "Vehicle demo";
      this.radioButtonVehicle.UseVisualStyleBackColor = true;
      this.radioButtonVehicle.CheckedChanged += new EventHandler(this.RealTimeChanged);
      this.panel2.Controls.Add((Control) this.MainMap);
      this.panel2.Dock = DockStyle.Fill;
      this.panel2.Location = new Point(0, 0);
      this.panel2.Margin = new Padding(3, 2, 3, 2);
      this.panel2.Name = "panel2";
      this.panel2.Size = new Size(881, 818);
      this.panel2.TabIndex = 41;
      this.MainMap.Bearing = 0.0f;
      this.MainMap.CanDragMap = true;
      this.MainMap.Dock = DockStyle.Fill;
      this.MainMap.GrayScaleMode = false;
      this.MainMap.LevelsKeepInMemmory = 5;
      this.MainMap.Location = new Point(0, 0);
      this.MainMap.Margin = new Padding(4);
      this.MainMap.MarkersEnabled = true;
      this.MainMap.MaxZoom = 17;
      this.MainMap.MinZoom = 2;
      this.MainMap.MouseWheelZoomType = MouseWheelZoomType.MousePositionAndCenter;
      this.MainMap.Name = "MainMap";
      this.MainMap.NegativeMode = false;
      this.MainMap.PolygonsEnabled = true;
      this.MainMap.RetryLoadTile = 0;
      this.MainMap.RoutesEnabled = true;
      this.MainMap.ShowTileGridLines = false;
      this.MainMap.Size = new Size(881, 818);
      this.MainMap.TabIndex = 0;
      this.MainMap.Zoom = 0.0;
      this.panel4.Controls.Add((Control) this.panel2);
      this.panel4.Controls.Add((Control) this.splitter1);
      this.panel4.Controls.Add((Control) this.panelMenu);
      this.panel4.Dock = DockStyle.Fill;
      this.panel4.Location = new Point(0, 0);
      this.panel4.Margin = new Padding(3, 2, 3, 2);
      this.panel4.Name = "panel4";
      this.panel4.Size = new Size(1188, 818);
      this.panel4.TabIndex = 44;
      this.AutoScaleDimensions = new SizeF(8f, 16f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = Color.AliceBlue;
      this.ClientSize = new Size(1188, 818);
      this.Controls.Add((Control) this.panel4);
      this.KeyPreview = true;
      this.Margin = new Padding(4);
      this.MinimumSize = new Size(733, 121);
      this.Name = "MainForm";
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "GMap.NET - Great Maps for Windows Forms";
      this.WindowState = FormWindowState.Maximized;
      this.Load += new EventHandler(this.MainForm_Load);
      this.KeyPress += new KeyPressEventHandler(this.MainForm_KeyPress);
      this.KeyUp += new KeyEventHandler(this.MainForm_KeyUp);
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.trackBar1.EndInit();
      this.groupBox5.ResumeLayout(false);
      this.groupBox5.PerformLayout();
      this.groupBox7.ResumeLayout(false);
      this.groupBox7.PerformLayout();
      this.groupBox8.ResumeLayout(false);
      this.groupBox8.PerformLayout();
      this.panelMenu.ResumeLayout(false);
      this.xPanderPanelList1.ResumeLayout(false);
      this.xPanderPanelMain.ResumeLayout(false);
      this.tableLayoutPanel4.ResumeLayout(false);
      this.tableLayoutPanel4.PerformLayout();
      this.tableLayoutPanel5.ResumeLayout(false);
      this.xPanderPanelCache.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.xPanderPanelInfo.ResumeLayout(false);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel2.PerformLayout();
      this.xPanderPanelLive.ResumeLayout(false);
      this.tableLayoutPanel3.ResumeLayout(false);
      this.tableLayoutPanel3.PerformLayout();
      ((ISupportInitialize) this.GridConnections).EndInit();
      this.panel2.ResumeLayout(false);
      this.panel4.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
