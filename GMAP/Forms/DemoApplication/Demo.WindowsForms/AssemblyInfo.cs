﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Universe")]
[assembly: AssemblyCopyright("Copyright © Universe 2011")]
[assembly: AssemblyTitle("Demo.WindowsForms")]
[assembly: Guid("3a7e8f7c-d56d-45f2-8226-2640f7049ea8")]
[assembly: AssemblyTrademark("email@radioman.lt")]
[assembly: AssemblyFileVersion("1.6.0.0")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyDescription("Demo for GMap.NET.WindowsForms")]
[assembly: AssemblyProduct("Demo.WindowsForms")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("1.6.0.0")]
