﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.MapInfo
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using GMap.NET;
using GMap.NET.MapProviders;

namespace Demo.WindowsForms
{
  public struct MapInfo
  {
    public RectLatLng Area;
    public int Zoom;
    public GMapProvider Type;
    public bool MakeWorldFile;

    public MapInfo(RectLatLng Area, int Zoom, GMapProvider Type, bool makeWorldFile)
    {
      this.Area = Area;
      this.Zoom = Zoom;
      this.Type = Type;
      this.MakeWorldFile = makeWorldFile;
    }
  }
}
