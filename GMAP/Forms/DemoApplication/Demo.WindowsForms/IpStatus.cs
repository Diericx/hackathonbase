﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.IpStatus
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

namespace Demo.WindowsForms
{
  internal struct IpStatus
  {
    private string countryName;
    private int connectionsCount;

    public string CountryName
    {
      get
      {
        return this.countryName;
      }
      set
      {
        this.countryName = value;
      }
    }

    public int ConnectionsCount
    {
      get
      {
        return this.connectionsCount;
      }
      set
      {
        this.connectionsCount = value;
      }
    }
  }
}
