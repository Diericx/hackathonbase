﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.SQLiteIpCache
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using Demo.WindowsForms.Properties;
using System;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;

namespace Demo.WindowsForms
{
  internal class SQLiteIpCache
  {
    private string cache;
    private string ipCache;
    private string db;

    public string IpCache
    {
      get
      {
        return this.ipCache;
      }
    }

    public string CacheLocation
    {
      get
      {
        return this.cache;
      }
      set
      {
        this.cache = value;
        this.ipCache = this.cache + (object) "IpGeoCacheDB" + (string) (object) Path.DirectorySeparatorChar;
        this.db = this.ipCache + (object) Path.DirectorySeparatorChar + "Data.ipdb";
        if (File.Exists(this.db))
          return;
        SQLiteIpCache.CreateEmptyDB(this.db);
      }
    }

    public static bool CreateEmptyDB(string file)
    {
      bool flag = true;
      try
      {
        string directoryName = Path.GetDirectoryName(file);
        if (!Directory.Exists(directoryName))
          Directory.CreateDirectory(directoryName);
        using (SQLiteConnection sqLiteConnection = new SQLiteConnection())
        {
          ((DbConnection) sqLiteConnection).ConnectionString = string.Format("Data Source=\"{0}\";FailIfMissing=False;", (object) file);
          ((DbConnection) sqLiteConnection).Open();
          using (DbTransaction dbTransaction = (DbTransaction) sqLiteConnection.BeginTransaction())
          {
            try
            {
              using (DbCommand dbCommand = (DbCommand) sqLiteConnection.CreateCommand())
              {
                dbCommand.Transaction = dbTransaction;
                dbCommand.CommandText = Resources.IpCacheCreateDb;
                dbCommand.ExecuteNonQuery();
              }
              dbTransaction.Commit();
            }
            catch (Exception ex)
            {
              Console.WriteLine("CreateEmptyDB: " + ex.ToString());
              dbTransaction.Rollback();
              flag = false;
            }
          }
          ((DbConnection) sqLiteConnection).Close();
        }
      }
      catch (Exception ex)
      {
        flag = false;
      }
      return flag;
    }

    public bool PutDataToCache(string ip, IpInfo data)
    {
      bool flag = true;
      try
      {
        using (SQLiteConnection sqLiteConnection = new SQLiteConnection())
        {
          ((DbConnection) sqLiteConnection).ConnectionString = string.Format("Data Source=\"{0}\";", (object) this.db);
          ((DbConnection) sqLiteConnection).Open();
          using (DbTransaction dbTransaction = (DbTransaction) sqLiteConnection.BeginTransaction())
          {
            try
            {
              using (DbCommand dbCommand = (DbCommand) sqLiteConnection.CreateCommand())
              {
                dbCommand.Transaction = dbTransaction;
                dbCommand.CommandText = "INSERT INTO Cache(Ip, CountryName, RegionName, City, Latitude, Longitude, Time) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7)";
                dbCommand.Parameters.Add((object) new SQLiteParameter("@p1", (object) ip));
                dbCommand.Parameters.Add((object) new SQLiteParameter("@p2", (object) data.CountryName));
                dbCommand.Parameters.Add((object) new SQLiteParameter("@p3", (object) data.RegionName));
                dbCommand.Parameters.Add((object) new SQLiteParameter("@p4", (object) data.City));
                dbCommand.Parameters.Add((object) new SQLiteParameter("@p5", (object) data.Latitude));
                dbCommand.Parameters.Add((object) new SQLiteParameter("@p6", (object) data.Longitude));
                dbCommand.Parameters.Add((object) new SQLiteParameter("@p7", (object) data.CacheTime));
                dbCommand.ExecuteNonQuery();
              }
              dbTransaction.Commit();
            }
            catch (Exception ex)
            {
              Console.WriteLine("PutDataToCache: " + ex.ToString());
              dbTransaction.Rollback();
              flag = false;
            }
          }
          ((DbConnection) sqLiteConnection).Close();
        }
      }
      catch (Exception ex)
      {
        flag = false;
      }
      return flag;
    }

    public IpInfo GetDataFromCache(string ip)
    {
      IpInfo ipInfo = (IpInfo) null;
      try
      {
        using (SQLiteConnection sqLiteConnection = new SQLiteConnection())
        {
          ((DbConnection) sqLiteConnection).ConnectionString = string.Format("Data Source=\"{0}\";", (object) this.db);
          ((DbConnection) sqLiteConnection).Open();
          using (DbCommand dbCommand = (DbCommand) sqLiteConnection.CreateCommand())
          {
            dbCommand.CommandText = "SELECT * FROM Cache WHERE Ip = '" + ip + "'";
            using (DbDataReader dbDataReader = dbCommand.ExecuteReader())
            {
              if (dbDataReader.Read())
                ipInfo = new IpInfo()
                {
                  Ip = ip,
                  CountryName = dbDataReader["CountryName"] as string,
                  RegionName = dbDataReader["RegionName"] as string,
                  City = dbDataReader["City"] as string,
                  Latitude = (double) dbDataReader["Latitude"],
                  Longitude = (double) dbDataReader["Longitude"],
                  CacheTime = (DateTime) dbDataReader["Time"]
                };
              dbDataReader.Close();
            }
          }
          ((DbConnection) sqLiteConnection).Close();
        }
      }
      catch (Exception ex)
      {
        ipInfo = (IpInfo) null;
      }
      return ipInfo;
    }
  }
}
