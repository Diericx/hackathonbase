﻿// Decompiled with JetBrains decompiler
// Type: Demo.WindowsForms.IpHelper
// Assembly: Demo.WindowsForms, Version=1.6.0.0, Culture=neutral, PublicKeyToken=b85b9027b614afef
// MVID: CA0F1F71-0BB8-4343-A7AA-C902E716DB91
// Assembly location: C:\Users\zjhre_000\Documents\Visual Studio 2013\PLUGINS\GMAP\Forms\Demo.WindowsForms.exe

using System;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;

namespace Demo.WindowsForms
{
  public static class IpHelper
  {
    public const string DllName = "iphlpapi.dll";
    public const int AfInet = 2;

    [DllImport("iphlpapi.dll", SetLastError = true)]
    public static extern uint GetExtendedTcpTable(IntPtr tcpTable, ref int tcpTableLength, bool sort, int ipVersion, IpHelper.TcpTableType tcpTableType, int reserved);

    public enum TcpTableType
    {
      BasicListener,
      BasicConnections,
      BasicAll,
      OwnerPidListener,
      OwnerPidConnections,
      OwnerPidAll,
      OwnerModuleListener,
      OwnerModuleConnections,
      OwnerModuleAll,
    }

    public struct TcpTable
    {
      public uint Length;
      public IpHelper.TcpRow row;
    }

    public struct TcpRow
    {
      public TcpState state;
      public uint localAddr;
      public byte localPort1;
      public byte localPort2;
      public byte localPort3;
      public byte localPort4;
      public uint remoteAddr;
      public byte remotePort1;
      public byte remotePort2;
      public byte remotePort3;
      public byte remotePort4;
      public int owningPid;
    }
  }
}
