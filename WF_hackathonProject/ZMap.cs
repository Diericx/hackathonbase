﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Data;
using System.Drawing;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

namespace WF_hackathonProject
{
    class ZMap
    {
        public GMapControl map;
        public string id;
        GMapMarkerGoogleGreen targetMarker;
        GMapCustomImageMarker copterMarker;

        GMapOverlay targetMarkerOverlay;
        GMapOverlay pathMarkersOverlay;
        GMapOverlay pathOverlay;

        List<GMapMarkerGoogleRed> markers = new List<GMapMarkerGoogleRed>();

        public ZMap(GMapControl map, string id)
        {
            this.map = map;
            this.id = id;

            //----------------------------
            //---------initialization----------
            //----------------------------
            //init map
            map.MapProvider = GMap.NET.MapProviders.BingHybridMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            //gmap_manual.SetCurrentPositionByKeywords("Maputo, Mozambique");
            map.Position = new PointLatLng(37.4976, -122.267);

            //create overlays
            pathMarkersOverlay = new GMapOverlay(map, "markers");
            targetMarkerOverlay = new GMapOverlay(map, "markers");

            //add marker
            object o = Properties.Resources.ResourceManager.GetObject("ufo-small");
            copterMarker = new GMapCustomImageMarker((Image)o, new PointLatLng(37.4976, -122.267));
            copterMarker.Tag = "target";
            targetMarkerOverlay.Markers.Add(copterMarker);

            //add marker
            targetMarker = new GMapMarkerGoogleGreen(new PointLatLng(37.4976, -122.267));
            targetMarker.Tag = "target";
            targetMarkerOverlay.Markers.Add(targetMarker);

            //add overlays
            map.Overlays.Add(pathMarkersOverlay);
            map.Overlays.Add(targetMarkerOverlay);

        }

        public void addPositionToDB(PointLatLng newP)
        {
            Form1.addToMarkersDataBase((double)newP.Lat, (double)newP.Lng);

            reLoadMarkersFromDB();

            reDrawRoute();
        }

        public void addMarker(PointLatLng pos)
        {
            GMapMarkerGoogleRed newMarker = new GMapMarkerGoogleRed(pos);
            newMarker.Tag = "path";
            markers.Add(newMarker);
            pathMarkersOverlay.Markers.Add(newMarker);
            map.Overlays.Add(pathMarkersOverlay);
        }

        public GMapCustomImageMarker getCopterMarker()
        {
            return copterMarker;
        }

        public void setCopterMarkerLocation(double lat, double lng)
        {
            copterMarker.Position = new PointLatLng(lat, lng);
        }

        public void moveMapPositionToCopter()
        {
            map.Position = copterMarker.Position;
        }

        public void reDrawRoute()
        {
            //reset everything
            //markers = new List<GMapMarkerGoogleRed>();
            map.Overlays.Remove(pathOverlay);
            pathOverlay = new GMapOverlay(map, "path");
            //gets points from database
            List<PointLatLng> points = getPointsFromDatabase();
            //draws path
            GMapRoute path = new GMapRoute(points, "testPath");
            pathOverlay.Routes.Add(path);
            map.Overlays.Add(pathOverlay);
        }

        public void reLoadMarkersFromDB()
        {
            markers = new List<GMapMarkerGoogleRed>();
            map.Overlays.Remove(pathMarkersOverlay);
            pathMarkersOverlay = new GMapOverlay(map, "markers");
            List<PointLatLng> points = getPointsFromDatabase();

            foreach (PointLatLng p in points)
            {
                //add marker
                var newMarker = new GMapMarkerGoogleRed(p);
                newMarker.Tag = "path";
                pathMarkersOverlay.Markers.Add(newMarker);
            }

            map.Overlays.Add(pathMarkersOverlay);
            
        }

        public PointLatLng getTargetMarkerPosition()
        {
            return targetMarker.Position;
        }

        public double getPathDistance()
        {
            List<PointLatLng> points = getPointsFromDatabase();
            double totalDist = 0;
            if (points.Count > 1)
            {
                for (int i = 1; i < points.Count; i++)
                {
                    PointLatLng p1 = points[i - 1];
                    PointLatLng p2 = points[i];
                    totalDist += distanceTo(p1.Lat, p1.Lng, p2.Lat, p2.Lng, 'M');
                    Console.WriteLine(totalDist + "; " + p1.Lat + " ; " + p1.Lng);
                }
            }
            return totalDist;
        }


        static public double distanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double rlon1 = Math.PI * lon1 / 180;
            double rlon2 = Math.PI * lon2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }

            return dist;
        }

        // convert degrees to radians
	    double deg2rad(double deg) {
		    double rad = deg * Math.PI/180; // radians = degrees * pi/180
		    return rad;
	    }

        // round to the nearest 1/1000
	    double round(double x) {
		    return Math.Round( x * 1000) / 1000;
	    }

        public List<PointLatLng> getPointsFromDatabase()
        {
            var table = Form1.getTable();
            List<PointLatLng> points = new List<PointLatLng>();

            foreach (DataRow row in table.Rows)
            {
                if (row.RowState != DataRowState.Deleted)
                {
                    var newPoint = new PointLatLng((double)row["Latitude"], (double)row["Longitude"]);
                    points.Add(newPoint);
                }
            }

            return points;
        }

        public void setTargetMarkerPos(PointLatLng newP)
        {
            targetMarker.Position = newP;
        }

        public PointLatLng getTargetMarkerPos()
        {
            return targetMarker.Position;
        }
    }
}
