﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WF_hackathonProject
{
    class ReceivePacket
    {
        public string packetString = "";
        public string lat;
        public string lng;
        public string sensor;
        public int time;
        public int sat; //satalites
        public double m1;
        public double m2;
        public double m3;
        public double m4;
        public double hmd;
        public double vlt;
        public double snd;
        public double ult;
        public double tmp;
        public double cps;
        public double x;
        public double y;
        public double z;
        public double crs; //course
        public double alt; //altitude
        public double speed;
    }
}
