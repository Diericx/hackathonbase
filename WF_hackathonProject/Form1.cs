﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Timers;
using System.Data;
using Newtonsoft.Json;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

namespace WF_hackathonProject
{
    public partial class Form1 : Form
    {
        ZMap zmap_manual;
        static ZMap zmap_auto;
        String messages = "";

        ZMap targetMap;

        static SerialCommunication sc;

        Dictionary<string, double> occulusData = new Dictionary<string, double>();

        private static Form1 form = null;

        delegate void SetTextCallback(string text);

        public Form1()
        {
            InitializeComponent();
            form = this;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'soundDataBaseDataSet.Sound' table. You can move, or remove it, as needed.
            this.soundTableAdapter.Fill(this.soundDataBaseDataSet.Sound);
            // TODO: This line of code loads data into the 'tempuratureDataBaseDataSet.Tempurature' table. You can move, or remove it, as needed.
            this.tempuratureTableAdapter.Fill(this.tempuratureDataBaseDataSet.Tempurature);
            // TODO: This line of code loads data into the 'humidityDataBaseDataSet.Humidity' table. You can move, or remove it, as needed.
            this.humidityTableAdapter.Fill(this.humidityDataBaseDataSet.Humidity);
            // TODO: This line of code loads data into the 'motor4DataBaseDataSet.Motor4' table. You can move, or remove it, as needed.
            this.motor4TableAdapter.Fill(this.motor4DataBaseDataSet.Motor4);
            // TODO: This line of code loads data into the 'motor3DataBaseDataSet.Motor3' table. You can move, or remove it, as needed.
            this.motor3TableAdapter.Fill(this.motor3DataBaseDataSet.Motor3);
            // TODO: This line of code loads data into the 'motor2DataBaseDataSet.Motor2' table. You can move, or remove it, as needed.
            this.motor2TableAdapter.Fill(this.motor2DataBaseDataSet.Motor2);
            // TODO: This line of code loads data into the 'motor1DataBaseDataSet.Motor1' table. You can move, or remove it, as needed.
            this.motor1TableAdapter.Fill(this.motor1DataBaseDataSet.Motor1);
            // TODO: This line of code loads data into the 'lightDataBaseDataSet.Light' table. You can move, or remove it, as needed.
            //this.lightTableAdapter.Fill(this.lightDataBaseDataSet.Light);
            // TODO: This line of code loads data into the 'closestObjDataBaseDataSet.ClosestObjFromBot' table. You can move, or remove it, as needed.
            this.closestObjFromBotTableAdapter.Fill(this.closestObjDataBaseDataSet.ClosestObjFromBot);
            // TODO: This line of code loads data into the 'voltageDataBaseDataSet.Voltage' table. You can move, or remove it, as needed.
            this.voltageTableAdapter.Fill(this.voltageDataBaseDataSet.Voltage);
            // TODO: This line of code loads data into the 'altitudeDataBaseDataSet.Altitude' table. You can move, or remove it, as needed.
            this.altitudeTableAdapter.Fill(this.altitudeDataBaseDataSet.Altitude);
            // TODO: This line of code loads data into the 'markersDataBaseDataSet.Markers' table. You can move, or remove it, as needed.
            this.markersTableAdapter.Fill(this.markersDataBaseDataSet.Markers);


            //motor4Chart.DataBind();

            sc = new SerialCommunication();
            sc.start();

            //setup occulus data dict
            occulusData["x"] = 0;
            occulusData["y"] = 0;
            occulusData["z"] = 0;

            zmap_manual = new ZMap(gmap_manual, "manual");
            //zmap_manual.addMarker(zmap_manual.map.Position);
            zmap_auto = new ZMap(gmap_auto, "auto");

            targetMap = zmap_manual;

            // Create a timer with a two second interval.
            System.Timers.Timer aTimer = new System.Timers.Timer(20);
            aTimer.SynchronizingObject = this;
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.Enabled = true;

        }

        
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            //sc.packets_synch.Count
            var packetCount = sc.packets.Count;
            if (packetCount > 0) {
                ReceivePacket latestPacket = (ReceivePacket)SerialCommunication.packets_synch.Dequeue();
                //string newMessage = "sensor: " + latestPacket.sensor + "; Time: " + latestPacket.time + "; Lng: " + latestPacket.lng + "\n";
                setPacketViewerText(latestPacket.packetString);

                if (shouldSendOcu.Checked == true)
                {
                    sc.send(formatOculusDataToJson());
                    Console.WriteLine("SEND COU DATA");
                }

                if (sendMarkerData.Checked == true)
                {
                    string markersMessage = formatMarkersToJSON();
                    //ssetPacketViewerText("SENT: " + markersMessage);
                    sc.send(markersMessage);
                }

                if (latestPacket.sensor == "gps" && targetMap.id == "manual")
                {
                    moveCopterMarker(latestPacket);
                    targetMap.moveMapPositionToCopter();
                    
                }
                else if (latestPacket.sensor == "thr")
                {
                    //MOTOR 1
                    Motor1DataBaseDataSet.Motor1Row newRow;
                    newRow = form.motor1DataBaseDataSet.Motor1.NewMotor1Row();
                    newRow.Throttle = latestPacket.m1;
                    //MOTOR 2
                    Motor2DataBaseDataSet.Motor2Row newRow2;
                    newRow2 = form.motor2DataBaseDataSet.Motor2.NewMotor2Row();
                    newRow2.Throttle = latestPacket.m2;
                    //MOTOR 3
                    Motor3DataBaseDataSet.Motor3Row newRow3;
                    newRow3 = form.motor3DataBaseDataSet.Motor3.NewMotor3Row();
                    newRow3.Throttle = latestPacket.m3;
                    //MOTOR 4
                    Motor4DataBaseDataSet.Motor4Row newRow4;
                    newRow4 = form.motor4DataBaseDataSet.Motor4.NewMotor4Row();
                    newRow4.Throttle = latestPacket.m4;

                    // Add the row to the Region table 
                    form.motor1DataBaseDataSet.Motor1.Rows.Add(newRow);
                    // Add the row to the Region table 
                    form.motor2DataBaseDataSet.Motor2.Rows.Add(newRow2);
                    // Add the row to the Region table 
                    form.motor3DataBaseDataSet.Motor3.Rows.Add(newRow3);
                    // Add the row to the Region table 
                    form.motor4DataBaseDataSet.Motor4.Rows.Add(newRow4);

                    //refresh charts
                    //DATABIND CHARTS
                    voltChart.DataBind();
                    closestObjChart.DataBind();
                    humidChart.DataBind();
                    tempChart.DataBind();
                    soundChart.DataBind();
                    motor1Chart.DataBind();
                    motor2Chart.DataBind();
                    motor3Chart.DataBind();
                    motor4Chart.DataBind();
                    //Update chart data
                    voltChart.Update();
                    closestObjChart.Update();
                    humidChart.Update();
                    tempChart.Update();
                    soundChart.Update();
                    motor1Chart.Update();
                    motor2Chart.Update();
                    motor3Chart.Update();
                    motor4Chart.Update();
                }
                else if (latestPacket.sensor == "msc") 
                {
                    //distance from bot
                    closestObjDataBaseDataSet.ClosestObjFromBotRow newClosestRow;
                    newClosestRow = form.closestObjDataBaseDataSet.ClosestObjFromBot.NewClosestObjFromBotRow();
                    newClosestRow.ClosestObjFromBot = latestPacket.ult;

                    //snd / mic
                    SoundDataBaseDataSet.SoundRow newSoundRow;
                    newSoundRow = form.soundDataBaseDataSet.Sound.NewSoundRow();
                    newSoundRow.Decibels = latestPacket.snd;

                    //voltage
                    VoltageDataBaseDataSet.VoltageRow newVoltRow;
                    newVoltRow = form.voltageDataBaseDataSet.Voltage.NewVoltageRow();
                    newVoltRow.Volts = latestPacket.vlt;

                    //humidity
                    HumidityDataBaseDataSet.HumidityRow newHumidityRow;
                    newHumidityRow = form.humidityDataBaseDataSet.Humidity.NewHumidityRow();
                    newHumidityRow.Percent = latestPacket.hmd;

                    //Tempurature
                    TempuratureDataBaseDataSet.TempuratureRow tempRow;
                    tempRow = form.tempuratureDataBaseDataSet.Tempurature.NewTempuratureRow();
                    tempRow.Degrees = latestPacket.tmp;

                    // Add the row to the sou d table 
                    form.soundDataBaseDataSet.Sound.Rows.Add(newSoundRow);
                    // Add the row to the closest object 
                    form.closestObjDataBaseDataSet.ClosestObjFromBot.Rows.Add(newClosestRow);
                    // Add the row to the voltage table 
                    form.voltageDataBaseDataSet.Voltage.Rows.Add(newVoltRow);
                    // Add the row to the humidity table 
                    form.humidityDataBaseDataSet.Humidity.Rows.Add(newHumidityRow);
                    // Add the row to the tempurature table 
                    form.tempuratureDataBaseDataSet.Tempurature.Rows.Add(tempRow);
                }
                else if (latestPacket.sensor == "unity")
                {
                    occulusData["x"] = latestPacket.x;
                    occulusData["y"] = latestPacket.y;
                    occulusData["z"] = latestPacket.z;
                    //setPacketViewerText("x: " + occulusData["x"] + ", y: " + occulusData["y"]);
                }
            }
            
        }


        private void setPacketViewerText(string text)
        {
            if (packetViewer != null)
            {
                // InvokeRequired required compares the thread ID of the 
                // calling thread to the thread ID of the creating thread. 
                // If these threads are different, it returns true. 
                if (this.packetViewer.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(setPacketViewerText);
                    this.Invoke(d, new object[] { text });
                }
                else
                {
                    this.packetViewer.AppendText(text);
                    this.packetViewer.SelectionStart = this.packetViewer.TextLength;
                    this.packetViewer.ScrollToCaret();
                }
            }
        }

        public static void addToMarkersDataBase(double latitude, double longitude) {
            // Create a new row.
            MarkersDataBaseDataSet.MarkersRow newRegionRow;
            newRegionRow = form.markersDataBaseDataSet.Markers.NewMarkersRow();
            newRegionRow.Latitude = latitude;
            newRegionRow.Longitude = longitude;

            // Add the row to the Region table 
            form.markersDataBaseDataSet.Markers.Rows.Add(newRegionRow);
            //form.markersDataBaseDataSet.Tables["Markers"].NewRow();

            // Save the new row to the database 
            form.Validate();
            form.markersBindingSource.EndEdit();
            form.markersTableAdapter.Update(form.markersDataBaseDataSet.Markers);
            //form.markersTableAdapter.Update(form.ProjectDataSet.Markers);

            //form.markersTableAdapter.Adapter.Update(form.markersDataBaseDataSet, "Markers");
        }

        private void moveCopterMarker(ReceivePacket p)
        {
            double lat = 0;
            double lng = 0;
            try
            {
                lat = Convert.ToDouble(p.lat);
                lng = Convert.ToDouble(p.lng);
            }
            catch (Exception e) { }
            targetMap.setCopterMarkerLocation(lat, lng );
            //targetMap.moveMapPositionToCopter();
        }

        public static DataTable getTable()
        {
            return form.markersDataBaseDataSet.Markers;
        }

        //Turn marker data into JSON to send to Quadcopter
        public string formatMarkersToJSON()
        {
            List<PointLatLng> points = targetMap.getPointsFromDatabase();

            WaypointsPacket newPacket = new WaypointsPacket();
            newPacket.packetString = "HI DANNY";
            newPacket.lats = new string[points.Count];
            newPacket.lngs = new string[points.Count];

            for (int i = 0; i < points.Count; i++ )
            {
                PointLatLng p = points[i];
                newPacket.lats[i] = (p.Lat.ToString());
                newPacket.lngs[i] = (p.Lng.ToString());
            }

            string json = JsonConvert.SerializeObject(newPacket);
            return json;
        }

        //Format oculus rift data from Unity into JSON
        public string formatOculusDataToJson()
        {
            OculusDataPacket newPacket = new OculusDataPacket();
            newPacket.x = occulusData["x"];
            newPacket.y = occulusData["y"];

            string json = JsonConvert.SerializeObject(newPacket);
            return json;
        }

        private void gmap_mouseUp(object sender, MouseEventArgs e)
        {
            double lat = 0;
            double lng = 0;
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                lat = targetMap.map.FromLocalToLatLng(e.X, e.Y).Lat;
                lng = targetMap.map.FromLocalToLatLng(e.X, e.Y).Lng;
                //targetMap.addMarker(new PointLatLng(lat, lng));
                targetMap.setTargetMarkerPos(new PointLatLng(lat, lng));
                //edit text to show target marker position
                PointLatLng targetMarkerPos = targetMap.getTargetMarkerPosition();
                auto_targetLat.Text = "Lat    (" + targetMarkerPos.Lat.ToString() + ")";
                auto_targetLong.Text = "Long (" + targetMarkerPos.Lng.ToString() + ")";
            }

        }

        private void manualPage_Click(object sender, EventArgs e)
        {

        }

        private void gmap_markerClick(GMapMarker item, MouseEventArgs e)
        {
            if ((string)item.Tag != "target")
            {
                targetMap.map.Position = item.Position;
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 1)
            {
                targetMap = zmap_auto;
            }
            else if (tabControl1.SelectedIndex == 0)
            {
                targetMap = zmap_manual;
            }
        }

        private void addMarkerBtn_Click(object sender, EventArgs e)
        {
            targetMap.addPositionToDB(targetMap.getTargetMarkerPos());
        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (targetMap != null)
            {
                targetMap.reDrawRoute();
                targetMap.reLoadMarkersFromDB();
            }
        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (targetMap != null)
            {
                targetMap.reDrawRoute();
                auto_distLabel.Text = "Path Distance: " + targetMap.getPathDistance().ToString() + " Miles";
            }
        }

        private void reDrawRouteBtn_Click(object sender, EventArgs e)
        {
            if (targetMap != null)
            {
                targetMap.reDrawRoute();
                targetMap.reLoadMarkersFromDB();
            }
        }

        private void auto_controlGroup_Enter(object sender, EventArgs e)
        {

        }

        private void gmap_manual_OnMapZoomChanged()
        {
            if (targetMap.id == "manual")
            {
                targetMap.map.Position = targetMap.getCopterMarker().Position;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            sc.stop();
        }

        private void mapZoomBar_Scroll(object sender, EventArgs e)
        {
            targetMap.map.Zoom = mapZoomBar.Value;
        }

        private void sendShitBtn_Click(object sender, EventArgs e)
        {
            string markersMessage = formatMarkersToJSON();
            setPacketViewerText("SENT: " + markersMessage);
            sc.send(markersMessage);

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void markersBindingSource_CurrentChanged(object sender, EventArgs e)
        {
        }

    }
}
