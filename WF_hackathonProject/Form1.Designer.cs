﻿namespace WF_hackathonProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.manualPage = new System.Windows.Forms.TabPage();
            this.controlsGroup = new System.Windows.Forms.GroupBox();
            this.mapGroup = new System.Windows.Forms.GroupBox();
            this.mapZoomBar = new System.Windows.Forms.TrackBar();
            this.gmap_manual = new GMap.NET.WindowsForms.GMapControl();
            this.autoPage = new System.Windows.Forms.TabPage();
            this.auto_controlGroup = new System.Windows.Forms.GroupBox();
            this.pathDataGroup = new System.Windows.Forms.GroupBox();
            this.auto_targetLong = new System.Windows.Forms.Label();
            this.auto_targetLat = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.auto_distLabel = new System.Windows.Forms.Label();
            this.addMarkerBtn = new System.Windows.Forms.Button();
            this.reDrawRouteBtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.latitudeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.longitudeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.auto_mapGroup = new System.Windows.Forms.GroupBox();
            this.mapZoomBar2 = new System.Windows.Forms.TrackBar();
            this.gmap_auto = new GMap.NET.WindowsForms.GMapControl();
            this.dataPage = new System.Windows.Forms.TabPage();
            this.motor4Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.motor4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.motor4DataBaseDataSet = new WF_hackathonProject.Motor4DataBaseDataSet();
            this.motor3Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.motor3BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.motor3DataBaseDataSet = new WF_hackathonProject.Motor3DataBaseDataSet();
            this.motor2Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.motor2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.motor2DataBaseDataSet = new WF_hackathonProject.Motor2DataBaseDataSet();
            this.motor1Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.motor1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.motor1DataBaseDataSet = new WF_hackathonProject.Motor1DataBaseDataSet();
            this.humidChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.humidityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.humidityDataBaseDataSet = new WF_hackathonProject.HumidityDataBaseDataSet();
            this.tempChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tempBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tempuratureDataBaseDataSet = new WF_hackathonProject.TempuratureDataBaseDataSet();
            this.soundChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.soundBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.soundDataBaseDataSet = new WF_hackathonProject.SoundDataBaseDataSet();
            this.closestObjChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.closestObjBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.closestObjDataBaseDataSet = new WF_hackathonProject.closestObjDataBaseDataSet();
            this.voltChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.voltageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.voltageDataBaseDataSet = new WF_hackathonProject.VoltageDataBaseDataSet();
            this.altChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.altitudeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.altitudeDataBaseDataSet = new WF_hackathonProject.AltitudeDataBaseDataSet();
            this.lightBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lightDataBaseDataSet = new WF_hackathonProject.LightDataBaseDataSet();
            this.sendShitBtn = new System.Windows.Forms.Button();
            this.packetViewer = new System.Windows.Forms.RichTextBox();
            this.altitudeTableAdapter = new WF_hackathonProject.AltitudeDataBaseDataSetTableAdapters.AltitudeTableAdapter();
            this.markersTableAdapter = new WF_hackathonProject.MarkersDataBaseDataSetTableAdapters.MarkersTableAdapter();
            this.markersDataBaseDataSet = new WF_hackathonProject.MarkersDataBaseDataSet();
            this.markersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lightTableAdapter = new WF_hackathonProject.LightDataBaseDataSetTableAdapters.LightTableAdapter();
            this.motor1TableAdapter = new WF_hackathonProject.Motor1DataBaseDataSetTableAdapters.Motor1TableAdapter();
            this.motor2TableAdapter = new WF_hackathonProject.Motor2DataBaseDataSetTableAdapters.Motor2TableAdapter();
            this.motor3TableAdapter = new WF_hackathonProject.Motor3DataBaseDataSetTableAdapters.Motor3TableAdapter();
            this.motor4TableAdapter = new WF_hackathonProject.Motor4DataBaseDataSetTableAdapters.Motor4TableAdapter();
            this.closestObjFromBotTableAdapter = new WF_hackathonProject.closestObjDataBaseDataSetTableAdapters.ClosestObjFromBotTableAdapter();
            this.voltageTableAdapter = new WF_hackathonProject.VoltageDataBaseDataSetTableAdapters.VoltageTableAdapter();
            this.soundTableAdapter = new WF_hackathonProject.SoundDataBaseDataSetTableAdapters.SoundTableAdapter();
            this.humidityTableAdapter = new WF_hackathonProject.HumidityDataBaseDataSetTableAdapters.HumidityTableAdapter();
            this.tempuratureTableAdapter = new WF_hackathonProject.TempuratureDataBaseDataSetTableAdapters.TempuratureTableAdapter();
            this.shouldSendOcu = new System.Windows.Forms.RadioButton();
            this.sendMarkerData = new System.Windows.Forms.RadioButton();
            this.tabControl1.SuspendLayout();
            this.manualPage.SuspendLayout();
            this.mapGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapZoomBar)).BeginInit();
            this.autoPage.SuspendLayout();
            this.auto_controlGroup.SuspendLayout();
            this.pathDataGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.auto_mapGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapZoomBar2)).BeginInit();
            this.dataPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motor4Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor4BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor4DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor3Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor3BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor3DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor2Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor2DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor1Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor1DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityDataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempuratureDataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundDataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closestObjChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closestObjBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closestObjDataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageDataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.altChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.altitudeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.altitudeDataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightDataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.markersDataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.markersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.manualPage);
            this.tabControl1.Controls.Add(this.autoPage);
            this.tabControl1.Controls.Add(this.dataPage);
            this.tabControl1.Location = new System.Drawing.Point(13, 14);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1745, 1287);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // manualPage
            // 
            this.manualPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.manualPage.Controls.Add(this.controlsGroup);
            this.manualPage.Controls.Add(this.mapGroup);
            this.manualPage.Location = new System.Drawing.Point(4, 25);
            this.manualPage.Margin = new System.Windows.Forms.Padding(4);
            this.manualPage.Name = "manualPage";
            this.manualPage.Padding = new System.Windows.Forms.Padding(4);
            this.manualPage.Size = new System.Drawing.Size(1737, 1258);
            this.manualPage.TabIndex = 0;
            this.manualPage.Text = "Manual";
            this.manualPage.UseVisualStyleBackColor = true;
            this.manualPage.Click += new System.EventHandler(this.manualPage_Click);
            // 
            // controlsGroup
            // 
            this.controlsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controlsGroup.Location = new System.Drawing.Point(1231, 7);
            this.controlsGroup.Margin = new System.Windows.Forms.Padding(4);
            this.controlsGroup.Name = "controlsGroup";
            this.controlsGroup.Padding = new System.Windows.Forms.Padding(4);
            this.controlsGroup.Size = new System.Drawing.Size(496, 1240);
            this.controlsGroup.TabIndex = 2;
            this.controlsGroup.TabStop = false;
            this.controlsGroup.Text = "Controls";
            // 
            // mapGroup
            // 
            this.mapGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mapGroup.Controls.Add(this.mapZoomBar);
            this.mapGroup.Controls.Add(this.gmap_manual);
            this.mapGroup.Location = new System.Drawing.Point(4, 7);
            this.mapGroup.Margin = new System.Windows.Forms.Padding(4);
            this.mapGroup.Name = "mapGroup";
            this.mapGroup.Padding = new System.Windows.Forms.Padding(4);
            this.mapGroup.Size = new System.Drawing.Size(1219, 1240);
            this.mapGroup.TabIndex = 1;
            this.mapGroup.TabStop = false;
            this.mapGroup.Text = "Map";
            // 
            // mapZoomBar
            // 
            this.mapZoomBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.mapZoomBar.LargeChange = 19;
            this.mapZoomBar.Location = new System.Drawing.Point(1156, 23);
            this.mapZoomBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mapZoomBar.Maximum = 19;
            this.mapZoomBar.Name = "mapZoomBar";
            this.mapZoomBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.mapZoomBar.Size = new System.Drawing.Size(56, 1208);
            this.mapZoomBar.TabIndex = 1;
            this.mapZoomBar.Scroll += new System.EventHandler(this.mapZoomBar_Scroll);
            // 
            // gmap_manual
            // 
            this.gmap_manual.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gmap_manual.Bearing = 0F;
            this.gmap_manual.CanDragMap = false;
            this.gmap_manual.GrayScaleMode = false;
            this.gmap_manual.LevelsKeepInMemmory = 5;
            this.gmap_manual.Location = new System.Drawing.Point(8, 23);
            this.gmap_manual.Margin = new System.Windows.Forms.Padding(4);
            this.gmap_manual.MarkersEnabled = true;
            this.gmap_manual.MaxZoom = 19;
            this.gmap_manual.MinZoom = 0;
            this.gmap_manual.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionWithoutCenter;
            this.gmap_manual.Name = "gmap_manual";
            this.gmap_manual.NegativeMode = false;
            this.gmap_manual.PolygonsEnabled = true;
            this.gmap_manual.RetryLoadTile = 0;
            this.gmap_manual.RoutesEnabled = true;
            this.gmap_manual.ShowTileGridLines = false;
            this.gmap_manual.Size = new System.Drawing.Size(1149, 1208);
            this.gmap_manual.TabIndex = 0;
            this.gmap_manual.Zoom = 5D;
            this.gmap_manual.OnMarkerClick += new GMap.NET.WindowsForms.MarkerClick(this.gmap_markerClick);
            this.gmap_manual.OnMapZoomChanged += new GMap.NET.MapZoomChanged(this.gmap_manual_OnMapZoomChanged);
            this.gmap_manual.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gmap_mouseUp);
            // 
            // autoPage
            // 
            this.autoPage.Controls.Add(this.auto_controlGroup);
            this.autoPage.Controls.Add(this.auto_mapGroup);
            this.autoPage.Location = new System.Drawing.Point(4, 25);
            this.autoPage.Margin = new System.Windows.Forms.Padding(4);
            this.autoPage.Name = "autoPage";
            this.autoPage.Padding = new System.Windows.Forms.Padding(4);
            this.autoPage.Size = new System.Drawing.Size(1737, 1258);
            this.autoPage.TabIndex = 1;
            this.autoPage.Text = "Auto";
            this.autoPage.UseVisualStyleBackColor = true;
            // 
            // auto_controlGroup
            // 
            this.auto_controlGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.auto_controlGroup.Controls.Add(this.pathDataGroup);
            this.auto_controlGroup.Controls.Add(this.addMarkerBtn);
            this.auto_controlGroup.Controls.Add(this.reDrawRouteBtn);
            this.auto_controlGroup.Controls.Add(this.dataGridView1);
            this.auto_controlGroup.Location = new System.Drawing.Point(1231, 7);
            this.auto_controlGroup.Margin = new System.Windows.Forms.Padding(4);
            this.auto_controlGroup.Name = "auto_controlGroup";
            this.auto_controlGroup.Padding = new System.Windows.Forms.Padding(4);
            this.auto_controlGroup.Size = new System.Drawing.Size(496, 2058);
            this.auto_controlGroup.TabIndex = 6;
            this.auto_controlGroup.TabStop = false;
            this.auto_controlGroup.Text = "Controls";
            this.auto_controlGroup.Enter += new System.EventHandler(this.auto_controlGroup_Enter);
            // 
            // pathDataGroup
            // 
            this.pathDataGroup.Controls.Add(this.auto_targetLong);
            this.pathDataGroup.Controls.Add(this.auto_targetLat);
            this.pathDataGroup.Controls.Add(this.label1);
            this.pathDataGroup.Controls.Add(this.auto_distLabel);
            this.pathDataGroup.Location = new System.Drawing.Point(8, 432);
            this.pathDataGroup.Margin = new System.Windows.Forms.Padding(4);
            this.pathDataGroup.Name = "pathDataGroup";
            this.pathDataGroup.Padding = new System.Windows.Forms.Padding(4);
            this.pathDataGroup.Size = new System.Drawing.Size(480, 202);
            this.pathDataGroup.TabIndex = 5;
            this.pathDataGroup.TabStop = false;
            this.pathDataGroup.Text = "Map Data";
            // 
            // auto_targetLong
            // 
            this.auto_targetLong.AutoSize = true;
            this.auto_targetLong.Location = new System.Drawing.Point(177, 50);
            this.auto_targetLong.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.auto_targetLong.Name = "auto_targetLong";
            this.auto_targetLong.Size = new System.Drawing.Size(54, 17);
            this.auto_targetLong.TabIndex = 3;
            this.auto_targetLong.Text = "Long ()";
            // 
            // auto_targetLat
            // 
            this.auto_targetLat.AutoSize = true;
            this.auto_targetLat.Location = new System.Drawing.Point(177, 23);
            this.auto_targetLat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.auto_targetLat.Name = "auto_targetLat";
            this.auto_targetLat.Size = new System.Drawing.Size(54, 17);
            this.auto_targetLat.TabIndex = 2;
            this.auto_targetLat.Text = "Lat    ()";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Target Marker Location:";
            // 
            // auto_distLabel
            // 
            this.auto_distLabel.AutoSize = true;
            this.auto_distLabel.Location = new System.Drawing.Point(8, 69);
            this.auto_distLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.auto_distLabel.Name = "auto_distLabel";
            this.auto_distLabel.Size = new System.Drawing.Size(103, 17);
            this.auto_distLabel.TabIndex = 0;
            this.auto_distLabel.Text = "Total Distance:";
            // 
            // addMarkerBtn
            // 
            this.addMarkerBtn.Location = new System.Drawing.Point(122, 23);
            this.addMarkerBtn.Margin = new System.Windows.Forms.Padding(4);
            this.addMarkerBtn.Name = "addMarkerBtn";
            this.addMarkerBtn.Size = new System.Drawing.Size(100, 28);
            this.addMarkerBtn.TabIndex = 2;
            this.addMarkerBtn.Text = "Add Marker";
            this.addMarkerBtn.UseVisualStyleBackColor = true;
            this.addMarkerBtn.Click += new System.EventHandler(this.addMarkerBtn_Click);
            // 
            // reDrawRouteBtn
            // 
            this.reDrawRouteBtn.Location = new System.Drawing.Point(239, 23);
            this.reDrawRouteBtn.Margin = new System.Windows.Forms.Padding(4);
            this.reDrawRouteBtn.Name = "reDrawRouteBtn";
            this.reDrawRouteBtn.Size = new System.Drawing.Size(121, 28);
            this.reDrawRouteBtn.TabIndex = 4;
            this.reDrawRouteBtn.Text = "ReDraw Route";
            this.reDrawRouteBtn.UseVisualStyleBackColor = true;
            this.reDrawRouteBtn.Click += new System.EventHandler(this.reDrawRouteBtn_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.latitudeDataGridViewTextBoxColumn,
            this.longitudeDataGridViewTextBoxColumn});
            this.dataGridView1.Location = new System.Drawing.Point(8, 59);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(480, 366);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView1_RowsAdded);
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // latitudeDataGridViewTextBoxColumn
            // 
            this.latitudeDataGridViewTextBoxColumn.DataPropertyName = "Latitude";
            this.latitudeDataGridViewTextBoxColumn.HeaderText = "Latitude";
            this.latitudeDataGridViewTextBoxColumn.Name = "latitudeDataGridViewTextBoxColumn";
            // 
            // longitudeDataGridViewTextBoxColumn
            // 
            this.longitudeDataGridViewTextBoxColumn.DataPropertyName = "Longitude";
            this.longitudeDataGridViewTextBoxColumn.HeaderText = "Longitude";
            this.longitudeDataGridViewTextBoxColumn.Name = "longitudeDataGridViewTextBoxColumn";
            // 
            // auto_mapGroup
            // 
            this.auto_mapGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.auto_mapGroup.Controls.Add(this.mapZoomBar2);
            this.auto_mapGroup.Controls.Add(this.gmap_auto);
            this.auto_mapGroup.Location = new System.Drawing.Point(4, 7);
            this.auto_mapGroup.Margin = new System.Windows.Forms.Padding(4);
            this.auto_mapGroup.Name = "auto_mapGroup";
            this.auto_mapGroup.Padding = new System.Windows.Forms.Padding(4);
            this.auto_mapGroup.Size = new System.Drawing.Size(1219, 2058);
            this.auto_mapGroup.TabIndex = 5;
            this.auto_mapGroup.TabStop = false;
            this.auto_mapGroup.Text = "Map";
            // 
            // mapZoomBar2
            // 
            this.mapZoomBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.mapZoomBar2.LargeChange = 18;
            this.mapZoomBar2.Location = new System.Drawing.Point(1156, 23);
            this.mapZoomBar2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mapZoomBar2.Maximum = 18;
            this.mapZoomBar2.Name = "mapZoomBar2";
            this.mapZoomBar2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.mapZoomBar2.Size = new System.Drawing.Size(56, 1976);
            this.mapZoomBar2.TabIndex = 2;
            this.mapZoomBar2.Scroll += new System.EventHandler(this.mapZoomBar_Scroll);
            // 
            // gmap_auto
            // 
            this.gmap_auto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gmap_auto.Bearing = 0F;
            this.gmap_auto.CanDragMap = true;
            this.gmap_auto.GrayScaleMode = false;
            this.gmap_auto.LevelsKeepInMemmory = 5;
            this.gmap_auto.Location = new System.Drawing.Point(8, 23);
            this.gmap_auto.Margin = new System.Windows.Forms.Padding(4);
            this.gmap_auto.MarkersEnabled = true;
            this.gmap_auto.MaxZoom = 18;
            this.gmap_auto.MinZoom = 2;
            this.gmap_auto.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionWithoutCenter;
            this.gmap_auto.Name = "gmap_auto";
            this.gmap_auto.NegativeMode = false;
            this.gmap_auto.PolygonsEnabled = true;
            this.gmap_auto.RetryLoadTile = 0;
            this.gmap_auto.RoutesEnabled = true;
            this.gmap_auto.ShowTileGridLines = false;
            this.gmap_auto.Size = new System.Drawing.Size(1149, 2027);
            this.gmap_auto.TabIndex = 0;
            this.gmap_auto.Zoom = 10D;
            this.gmap_auto.OnMarkerClick += new GMap.NET.WindowsForms.MarkerClick(this.gmap_markerClick);
            this.gmap_auto.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gmap_mouseUp);
            // 
            // dataPage
            // 
            this.dataPage.Controls.Add(this.motor4Chart);
            this.dataPage.Controls.Add(this.motor3Chart);
            this.dataPage.Controls.Add(this.motor2Chart);
            this.dataPage.Controls.Add(this.motor1Chart);
            this.dataPage.Controls.Add(this.humidChart);
            this.dataPage.Controls.Add(this.tempChart);
            this.dataPage.Controls.Add(this.soundChart);
            this.dataPage.Controls.Add(this.closestObjChart);
            this.dataPage.Controls.Add(this.voltChart);
            this.dataPage.Controls.Add(this.altChart);
            this.dataPage.Location = new System.Drawing.Point(4, 25);
            this.dataPage.Margin = new System.Windows.Forms.Padding(4);
            this.dataPage.Name = "dataPage";
            this.dataPage.Padding = new System.Windows.Forms.Padding(4);
            this.dataPage.Size = new System.Drawing.Size(1737, 1258);
            this.dataPage.TabIndex = 2;
            this.dataPage.Text = "Data";
            this.dataPage.UseVisualStyleBackColor = true;
            // 
            // motor4Chart
            // 
            chartArea1.Name = "ChartArea1";
            this.motor4Chart.ChartAreas.Add(chartArea1);
            this.motor4Chart.DataSource = this.motor4BindingSource;
            legend1.Name = "Legend1";
            this.motor4Chart.Legends.Add(legend1);
            this.motor4Chart.Location = new System.Drawing.Point(1304, 851);
            this.motor4Chart.Name = "motor4Chart";
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            series1.Legend = "Legend1";
            series1.Name = "Throttle\\n(m4)";
            series1.XValueMember = "Id";
            series1.YValueMembers = "Throttle";
            this.motor4Chart.Series.Add(series1);
            this.motor4Chart.Size = new System.Drawing.Size(430, 400);
            this.motor4Chart.TabIndex = 8;
            this.motor4Chart.Text = "chart1";
            // 
            // motor4BindingSource
            // 
            this.motor4BindingSource.DataMember = "Motor4";
            this.motor4BindingSource.DataSource = this.motor4DataBaseDataSet;
            // 
            // motor4DataBaseDataSet
            // 
            this.motor4DataBaseDataSet.DataSetName = "Motor4DataBaseDataSet";
            this.motor4DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // motor3Chart
            // 
            chartArea2.Name = "ChartArea1";
            this.motor3Chart.ChartAreas.Add(chartArea2);
            this.motor3Chart.DataSource = this.motor3BindingSource;
            legend2.Name = "Legend1";
            this.motor3Chart.Legends.Add(legend2);
            this.motor3Chart.Location = new System.Drawing.Point(877, 851);
            this.motor3Chart.Name = "motor3Chart";
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            series2.Legend = "Legend1";
            series2.Name = "Throttle\\n(m3)";
            series2.XValueMember = "Id";
            series2.YValueMembers = "Throttle";
            this.motor3Chart.Series.Add(series2);
            this.motor3Chart.Size = new System.Drawing.Size(430, 400);
            this.motor3Chart.TabIndex = 7;
            this.motor3Chart.Text = "chart1";
            // 
            // motor3BindingSource
            // 
            this.motor3BindingSource.DataMember = "Motor3";
            this.motor3BindingSource.DataSource = this.motor3DataBaseDataSet;
            // 
            // motor3DataBaseDataSet
            // 
            this.motor3DataBaseDataSet.DataSetName = "Motor3DataBaseDataSet";
            this.motor3DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // motor2Chart
            // 
            chartArea3.Name = "ChartArea1";
            this.motor2Chart.ChartAreas.Add(chartArea3);
            this.motor2Chart.DataSource = this.motor2BindingSource;
            legend3.Name = "Legend1";
            this.motor2Chart.Legends.Add(legend3);
            this.motor2Chart.Location = new System.Drawing.Point(441, 851);
            this.motor2Chart.Name = "motor2Chart";
            series3.BorderWidth = 3;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            series3.Legend = "Legend1";
            series3.Name = "Throttle \\n(m2)";
            series3.XValueMember = "Id";
            series3.YValueMembers = "Throttle";
            this.motor2Chart.Series.Add(series3);
            this.motor2Chart.Size = new System.Drawing.Size(430, 400);
            this.motor2Chart.TabIndex = 6;
            this.motor2Chart.Text = "chart7";
            // 
            // motor2BindingSource
            // 
            this.motor2BindingSource.DataMember = "Motor2";
            this.motor2BindingSource.DataSource = this.motor2DataBaseDataSet;
            // 
            // motor2DataBaseDataSet
            // 
            this.motor2DataBaseDataSet.DataSetName = "Motor2DataBaseDataSet";
            this.motor2DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // motor1Chart
            // 
            chartArea4.Name = "ChartArea1";
            this.motor1Chart.ChartAreas.Add(chartArea4);
            this.motor1Chart.DataSource = this.motor1BindingSource;
            legend4.Name = "Legend1";
            this.motor1Chart.Legends.Add(legend4);
            this.motor1Chart.Location = new System.Drawing.Point(4, 850);
            this.motor1Chart.Margin = new System.Windows.Forms.Padding(4);
            this.motor1Chart.Name = "motor1Chart";
            series4.BorderWidth = 3;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            series4.Legend = "Legend1";
            series4.Name = "Throttle \\n(m1)";
            series4.XValueMember = "Id";
            series4.YValueMembers = "Throttle";
            this.motor1Chart.Series.Add(series4);
            this.motor1Chart.Size = new System.Drawing.Size(430, 400);
            this.motor1Chart.TabIndex = 5;
            this.motor1Chart.Text = "chart8";
            // 
            // motor1BindingSource
            // 
            this.motor1BindingSource.DataMember = "Motor1";
            this.motor1BindingSource.DataSource = this.motor1DataBaseDataSet;
            // 
            // motor1DataBaseDataSet
            // 
            this.motor1DataBaseDataSet.DataSetName = "Motor1DataBaseDataSet";
            this.motor1DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // humidChart
            // 
            chartArea5.Name = "ChartArea1";
            this.humidChart.ChartAreas.Add(chartArea5);
            this.humidChart.DataSource = this.humidityBindingSource;
            legend5.Name = "Legend1";
            this.humidChart.Legends.Add(legend5);
            this.humidChart.Location = new System.Drawing.Point(1149, 409);
            this.humidChart.Name = "humidChart";
            series5.BorderWidth = 3;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Color = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(156)))), ((int)(((byte)(18)))));
            series5.Legend = "Legend1";
            series5.Name = "% (Humidity)";
            series5.XValueMember = "Id";
            series5.YValueMembers = "Percent";
            this.humidChart.Series.Add(series5);
            this.humidChart.Size = new System.Drawing.Size(580, 400);
            this.humidChart.TabIndex = 4;
            this.humidChart.Text = "chart1";
            // 
            // humidityBindingSource
            // 
            this.humidityBindingSource.DataMember = "Humidity";
            this.humidityBindingSource.DataSource = this.humidityDataBaseDataSet;
            // 
            // humidityDataBaseDataSet
            // 
            this.humidityDataBaseDataSet.DataSetName = "HumidityDataBaseDataSet";
            this.humidityDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tempChart
            // 
            chartArea6.Name = "ChartArea1";
            this.tempChart.ChartAreas.Add(chartArea6);
            this.tempChart.DataSource = this.tempBindingSource;
            legend6.Name = "Legend1";
            this.tempChart.Legends.Add(legend6);
            this.tempChart.Location = new System.Drawing.Point(564, 409);
            this.tempChart.Name = "tempChart";
            series6.BorderWidth = 3;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Color = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            series6.Legend = "Legend1";
            series6.Name = "Degrees";
            series6.XValueMember = "Id";
            series6.YValueMembers = "Degrees";
            this.tempChart.Series.Add(series6);
            this.tempChart.Size = new System.Drawing.Size(580, 400);
            this.tempChart.TabIndex = 3;
            this.tempChart.Text = "chart4";
            // 
            // tempBindingSource
            // 
            this.tempBindingSource.DataMember = "Tempurature";
            this.tempBindingSource.DataSource = this.tempuratureDataBaseDataSet;
            // 
            // tempuratureDataBaseDataSet
            // 
            this.tempuratureDataBaseDataSet.DataSetName = "TempuratureDataBaseDataSet";
            this.tempuratureDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soundChart
            // 
            chartArea7.Name = "ChartArea1";
            this.soundChart.ChartAreas.Add(chartArea7);
            this.soundChart.DataSource = this.soundBindingSource;
            legend7.Name = "Legend1";
            this.soundChart.Legends.Add(legend7);
            this.soundChart.Location = new System.Drawing.Point(4, 410);
            this.soundChart.Margin = new System.Windows.Forms.Padding(4);
            this.soundChart.Name = "soundChart";
            series7.BorderWidth = 3;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Color = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(126)))), ((int)(((byte)(34)))));
            series7.Legend = "Legend1";
            series7.Name = "Analog";
            series7.XValueMember = "Id";
            series7.YValueMembers = "Decibels";
            this.soundChart.Series.Add(series7);
            this.soundChart.Size = new System.Drawing.Size(580, 400);
            this.soundChart.TabIndex = 2;
            this.soundChart.Text = "chart5";
            // 
            // soundBindingSource
            // 
            this.soundBindingSource.DataMember = "Sound";
            this.soundBindingSource.DataSource = this.soundDataBaseDataSet;
            // 
            // soundDataBaseDataSet
            // 
            this.soundDataBaseDataSet.DataSetName = "SoundDataBaseDataSet";
            this.soundDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // closestObjChart
            // 
            chartArea8.Name = "ChartArea1";
            this.closestObjChart.ChartAreas.Add(chartArea8);
            this.closestObjChart.DataSource = this.closestObjBindingSource;
            legend8.Name = "Legend1";
            this.closestObjChart.Legends.Add(legend8);
            this.closestObjChart.Location = new System.Drawing.Point(1154, 3);
            this.closestObjChart.Name = "closestObjChart";
            series8.BorderWidth = 3;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Color = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(174)))), ((int)(((byte)(96)))));
            series8.Legend = "Legend1";
            series8.Name = "Feet";
            series8.XValueMember = "Id";
            series8.YValueMembers = "ClosestObjFromBot";
            this.closestObjChart.Series.Add(series8);
            this.closestObjChart.Size = new System.Drawing.Size(580, 400);
            this.closestObjChart.TabIndex = 1;
            this.closestObjChart.Text = "chart1";
            // 
            // closestObjBindingSource
            // 
            this.closestObjBindingSource.DataMember = "ClosestObjFromBot";
            this.closestObjBindingSource.DataSource = this.closestObjDataBaseDataSet;
            // 
            // closestObjDataBaseDataSet
            // 
            this.closestObjDataBaseDataSet.DataSetName = "closestObjDataBaseDataSet";
            this.closestObjDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // voltChart
            // 
            chartArea9.Name = "ChartArea1";
            this.voltChart.ChartAreas.Add(chartArea9);
            this.voltChart.DataSource = this.voltageBindingSource;
            legend9.Name = "Legend1";
            this.voltChart.Legends.Add(legend9);
            this.voltChart.Location = new System.Drawing.Point(564, 3);
            this.voltChart.Name = "voltChart";
            series9.BorderWidth = 3;
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Color = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(196)))), ((int)(((byte)(15)))));
            series9.Legend = "Legend1";
            series9.Name = "Volts";
            series9.XValueMember = "Id";
            series9.YValueMembers = "Volts";
            this.voltChart.Series.Add(series9);
            this.voltChart.Size = new System.Drawing.Size(580, 400);
            this.voltChart.TabIndex = 1;
            this.voltChart.Text = "chart1";
            this.voltChart.Click += new System.EventHandler(this.chart1_Click);
            // 
            // voltageBindingSource
            // 
            this.voltageBindingSource.DataMember = "Voltage";
            this.voltageBindingSource.DataSource = this.voltageDataBaseDataSet;
            // 
            // voltageDataBaseDataSet
            // 
            this.voltageDataBaseDataSet.DataSetName = "VoltageDataBaseDataSet";
            this.voltageDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // altChart
            // 
            chartArea10.Name = "ChartArea1";
            this.altChart.ChartAreas.Add(chartArea10);
            this.altChart.DataSource = this.altitudeBindingSource;
            legend10.Name = "Legend1";
            this.altChart.Legends.Add(legend10);
            this.altChart.Location = new System.Drawing.Point(4, 4);
            this.altChart.Margin = new System.Windows.Forms.Padding(4);
            this.altChart.Name = "altChart";
            series10.BorderWidth = 3;
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Color = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            series10.Legend = "Legend1";
            series10.Name = "Altitude";
            series10.XValueMember = "Time";
            series10.YValueMembers = "Alt";
            this.altChart.Series.Add(series10);
            this.altChart.Size = new System.Drawing.Size(580, 400);
            this.altChart.TabIndex = 0;
            this.altChart.Text = "altChart";
            // 
            // altitudeBindingSource
            // 
            this.altitudeBindingSource.DataMember = "Altitude";
            this.altitudeBindingSource.DataSource = this.altitudeDataBaseDataSet;
            // 
            // altitudeDataBaseDataSet
            // 
            this.altitudeDataBaseDataSet.DataSetName = "AltitudeDataBaseDataSet";
            this.altitudeDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lightBindingSource
            // 
            this.lightBindingSource.DataMember = "Light";
            this.lightBindingSource.DataSource = this.lightDataBaseDataSet;
            // 
            // lightDataBaseDataSet
            // 
            this.lightDataBaseDataSet.DataSetName = "LightDataBaseDataSet";
            this.lightDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sendShitBtn
            // 
            this.sendShitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.sendShitBtn.Location = new System.Drawing.Point(509, 1342);
            this.sendShitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sendShitBtn.Name = "sendShitBtn";
            this.sendShitBtn.Size = new System.Drawing.Size(75, 23);
            this.sendShitBtn.TabIndex = 1;
            this.sendShitBtn.Text = "send shit";
            this.sendShitBtn.UseVisualStyleBackColor = true;
            this.sendShitBtn.Click += new System.EventHandler(this.sendShitBtn_Click);
            // 
            // packetViewer
            // 
            this.packetViewer.Location = new System.Drawing.Point(13, 1342);
            this.packetViewer.Margin = new System.Windows.Forms.Padding(4);
            this.packetViewer.Name = "packetViewer";
            this.packetViewer.Size = new System.Drawing.Size(479, 312);
            this.packetViewer.TabIndex = 0;
            this.packetViewer.Text = "";
            // 
            // altitudeTableAdapter
            // 
            this.altitudeTableAdapter.ClearBeforeFill = true;
            // 
            // markersTableAdapter
            // 
            this.markersTableAdapter.ClearBeforeFill = true;
            // 
            // markersDataBaseDataSet
            // 
            this.markersDataBaseDataSet.DataSetName = "MarkersDataBaseDataSet";
            this.markersDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // markersBindingSource
            // 
            this.markersBindingSource.DataMember = "Markers";
            this.markersBindingSource.DataSource = this.markersDataBaseDataSet;
            this.markersBindingSource.CurrentChanged += new System.EventHandler(this.markersBindingSource_CurrentChanged);
            // 
            // lightTableAdapter
            // 
            this.lightTableAdapter.ClearBeforeFill = true;
            // 
            // motor1TableAdapter
            // 
            this.motor1TableAdapter.ClearBeforeFill = true;
            // 
            // motor2TableAdapter
            // 
            this.motor2TableAdapter.ClearBeforeFill = true;
            // 
            // motor3TableAdapter
            // 
            this.motor3TableAdapter.ClearBeforeFill = true;
            // 
            // motor4TableAdapter
            // 
            this.motor4TableAdapter.ClearBeforeFill = true;
            // 
            // closestObjFromBotTableAdapter
            // 
            this.closestObjFromBotTableAdapter.ClearBeforeFill = true;
            // 
            // voltageTableAdapter
            // 
            this.voltageTableAdapter.ClearBeforeFill = true;
            // 
            // soundTableAdapter
            // 
            this.soundTableAdapter.ClearBeforeFill = true;
            // 
            // humidityTableAdapter
            // 
            this.humidityTableAdapter.ClearBeforeFill = true;
            // 
            // tempuratureTableAdapter
            // 
            this.tempuratureTableAdapter.ClearBeforeFill = true;
            // 
            // shouldSendOcu
            // 
            this.shouldSendOcu.AutoSize = true;
            this.shouldSendOcu.Location = new System.Drawing.Point(630, 1343);
            this.shouldSendOcu.Name = "shouldSendOcu";
            this.shouldSendOcu.Size = new System.Drawing.Size(190, 21);
            this.shouldSendOcu.TabIndex = 2;
            this.shouldSendOcu.TabStop = true;
            this.shouldSendOcu.Text = "Should send Oculus Data";
            this.shouldSendOcu.UseVisualStyleBackColor = true;
            // 
            // sendMarkerData
            // 
            this.sendMarkerData.AutoSize = true;
            this.sendMarkerData.Location = new System.Drawing.Point(630, 1396);
            this.sendMarkerData.Name = "sendMarkerData";
            this.sendMarkerData.Size = new System.Drawing.Size(188, 21);
            this.sendMarkerData.TabIndex = 2;
            this.sendMarkerData.TabStop = true;
            this.sendMarkerData.Text = "Should send marker data";
            this.sendMarkerData.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1777, 1767);
            this.Controls.Add(this.sendMarkerData);
            this.Controls.Add(this.shouldSendOcu);
            this.Controls.Add(this.sendShitBtn);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.packetViewer);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.manualPage.ResumeLayout(false);
            this.mapGroup.ResumeLayout(false);
            this.mapGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapZoomBar)).EndInit();
            this.autoPage.ResumeLayout(false);
            this.auto_controlGroup.ResumeLayout(false);
            this.pathDataGroup.ResumeLayout(false);
            this.pathDataGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.auto_mapGroup.ResumeLayout(false);
            this.auto_mapGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapZoomBar2)).EndInit();
            this.dataPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.motor4Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor4BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor4DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor3Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor3BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor3DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor2Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor2DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor1Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motor1DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityDataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempuratureDataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundDataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closestObjChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closestObjBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closestObjDataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageDataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.altChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.altitudeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.altitudeDataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightDataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.markersDataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.markersBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage manualPage;
        private System.Windows.Forms.TabPage autoPage;
        private System.Windows.Forms.TabPage dataPage;
        private GMap.NET.WindowsForms.GMapControl gmap_manual;
        private GMap.NET.WindowsForms.GMapControl gmap_auto;
        private System.Windows.Forms.Button addMarkerBtn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button reDrawRouteBtn;
        private System.Windows.Forms.GroupBox controlsGroup;
        private System.Windows.Forms.GroupBox mapGroup;
        private System.Windows.Forms.GroupBox auto_controlGroup;
        private System.Windows.Forms.GroupBox auto_mapGroup;
        private System.Windows.Forms.GroupBox pathDataGroup;
        private System.Windows.Forms.Label auto_distLabel;
        private System.Windows.Forms.Label auto_targetLong;
        private System.Windows.Forms.Label auto_targetLat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox packetViewer;
        private System.Windows.Forms.TrackBar mapZoomBar;
        private System.Windows.Forms.TrackBar mapZoomBar2;
        private System.Windows.Forms.Button sendShitBtn;
        private System.Windows.Forms.DataVisualization.Charting.Chart altChart;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn latitudeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn longitudeDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource altitudeBindingSource;
        private AltitudeDataBaseDataSet altitudeDataBaseDataSet;
        private AltitudeDataBaseDataSetTableAdapters.AltitudeTableAdapter altitudeTableAdapter;
        private MarkersDataBaseDataSetTableAdapters.MarkersTableAdapter markersTableAdapter;
        private MarkersDataBaseDataSet markersDataBaseDataSet;
        private System.Windows.Forms.BindingSource markersBindingSource;
        private System.Windows.Forms.BindingSource voltageBindingSource;
        private VoltageDataBaseDataSet voltageDataBaseDataSet;
        private System.Windows.Forms.BindingSource closestObjBindingSource;
        private closestObjDataBaseDataSet closestObjDataBaseDataSet;
        private System.Windows.Forms.BindingSource lightBindingSource;
        private LightDataBaseDataSet lightDataBaseDataSet;
        private System.Windows.Forms.BindingSource motor1BindingSource;
        private Motor1DataBaseDataSet motor1DataBaseDataSet;
        private System.Windows.Forms.BindingSource motor2BindingSource;
        private Motor2DataBaseDataSet motor2DataBaseDataSet;
        private System.Windows.Forms.BindingSource motor3BindingSource;
        private Motor3DataBaseDataSet motor3DataBaseDataSet;
        private System.Windows.Forms.BindingSource motor4BindingSource;
        private Motor4DataBaseDataSet motor4DataBaseDataSet;
        private System.Windows.Forms.DataVisualization.Charting.Chart voltChart;
        private VoltageDataBaseDataSetTableAdapters.VoltageTableAdapter voltageTableAdapter;
        private closestObjDataBaseDataSetTableAdapters.ClosestObjFromBotTableAdapter closestObjFromBotTableAdapter;
        private LightDataBaseDataSetTableAdapters.LightTableAdapter lightTableAdapter;
        private Motor1DataBaseDataSetTableAdapters.Motor1TableAdapter motor1TableAdapter;
        private Motor2DataBaseDataSetTableAdapters.Motor2TableAdapter motor2TableAdapter;
        private Motor3DataBaseDataSetTableAdapters.Motor3TableAdapter motor3TableAdapter;
        private Motor4DataBaseDataSetTableAdapters.Motor4TableAdapter motor4TableAdapter;
        private System.Windows.Forms.DataVisualization.Charting.Chart closestObjChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart humidChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart tempChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart soundChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart motor3Chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart motor2Chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart motor1Chart;
        private System.Windows.Forms.BindingSource humidityBindingSource;
        private HumidityDataBaseDataSet humidityDataBaseDataSet;
        private HumidityDataBaseDataSetTableAdapters.HumidityTableAdapter humidityTableAdapter;
        private System.Windows.Forms.BindingSource tempBindingSource;
        private TempuratureDataBaseDataSet tempuratureDataBaseDataSet;
        private TempuratureDataBaseDataSetTableAdapters.TempuratureTableAdapter tempuratureTableAdapter;
        private System.Windows.Forms.BindingSource soundBindingSource;
        private SoundDataBaseDataSet soundDataBaseDataSet;
        private SoundDataBaseDataSetTableAdapters.SoundTableAdapter soundTableAdapter;
        private System.Windows.Forms.DataVisualization.Charting.Chart motor4Chart;
        private System.Windows.Forms.RadioButton shouldSendOcu;
        private System.Windows.Forms.RadioButton sendMarkerData;

    }
}

