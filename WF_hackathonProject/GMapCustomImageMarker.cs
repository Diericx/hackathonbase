﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using GMap.NET.WindowsForms;
namespace GMap.NET.WindowsForms.Markers
{

    public class GMapCustomImageMarker : GMapMarker
    {
        private Image _image;

        public GMapCustomImageMarker(Image Image, PointLatLng p)
            : base(p)
        {
            _image = Image;
        }

        public override void OnRender(Graphics g)
        {
            g.DrawImage(_image, new Point(LocalPosition.X - _image.Width / 2, LocalPosition.Y - _image.Height / 2));
        }
    }
}