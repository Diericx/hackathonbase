﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections;

namespace WF_hackathonProject
{
    class SerialCommunication
    {

        public Queue packets = new Queue();
        public static Queue packets_synch;

        static bool _continue = true;
        static SerialPort _serialPort;

        static string message = "";

        Thread readThread = new Thread(Read);

        public SerialCommunication()
        {
            // Create a new SerialPort object with default settings.
            _serialPort = new SerialPort();
            _serialPort.PortName = "COM1";
            _serialPort.BaudRate = 115200;
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;

            // Creates a synchronized wrapper around the Queue.
            packets_synch = Queue.Synchronized(packets);

            //_serialPort.Open();
            _serialPort.Open();
        }

        public void start()
        {
            readThread.Start();
        }

        public void stop()
        {
            _continue = false;
            readThread.Join();
            _serialPort.Close();
            
        }

        public static void Read()
        {
            while (_continue)
            {
                try
                {
                    string newMessage = _serialPort.ReadLine();
                    Console.WriteLine(newMessage);
                    message += newMessage + " \n";
                    ReceivePacket m = new ReceivePacket();
                    try
                    {
                        m = JsonConvert.DeserializeObject<ReceivePacket>(newMessage);
                        Console.WriteLine("SERIALIZED PACKET ***************");
                        Console.WriteLine(newMessage);
                    }
                    catch(Exception e) 
                    {
                        m = new ReceivePacket();
                        m.sensor = "null";
                        Console.WriteLine("FAILED TO SERIALIZE PACKET ***************");
                        Console.WriteLine(newMessage);
                    }
                    m.packetString = newMessage;
                    packets_synch.Enqueue(m);

                }
                catch (TimeoutException) { }
            }
        }

        public void send(string input)
        {
            _serialPort.WriteLine(input);
        }
    }
}
